package org.fejoa.storage

import org.fejoa.support.AsyncInStream
import org.fejoa.support.AsyncOutStream


typealias Mode = Int

infix fun Mode.join(otherMode: Mode): Mode {
    return this or otherMode
}

infix fun Mode.has(otherMode: Mode): Boolean {
    return this and otherMode == otherMode
}

interface RandomDataAccess : AsyncInStream, AsyncOutStream {
    companion object {
        const val READ: Mode = 0x01
        const val WRITE: Mode = (0x02)
        const val TRUNCATE: Mode = WRITE or 0x04
        const val APPEND: Mode = WRITE or 0x08
        const val INSERT: Mode = WRITE or 0x10
    }

    val mode: Mode
    fun isOpen(): Boolean
    suspend fun length(): Long
    fun position(): Long
    suspend fun seek(position: Long)

    suspend fun truncate(size: Long)
    suspend fun delete(position: Long, length: Long)

    suspend fun readAt(position: Long, buffer: ByteArray): Int {
        return readAt(position, buffer, 0, buffer.size)
    }
    suspend fun readAt(position: Long, buffer: ByteArray, offset: Int, length: Int): Int

    suspend fun writeAt(position: Long, buffer: ByteArray): Int {
        return writeAt(position, buffer, 0, buffer.size)
    }

    suspend fun writeAt(position: Long, buffer: ByteArray, offset: Int, length: Int): Int
}
