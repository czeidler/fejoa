package org.fejoa.storage

import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor
import org.fejoa.support.*


@Serializable
class HashValue(val bytes: ByteArray) : Comparable<HashValue> {

    constructor(hashSize: Int) : this(ByteArray(hashSize))

    constructor(hash: HashValue) : this(hash.bytes.copyOf(hash.size()))

    fun clone(): HashValue = HashValue(bytes.copyOf())

    override fun hashCode(): Int {
        val byte0 = (if (bytes.size > 0) bytes[0] else 0).toInt()
        val byte1 = (if (bytes.size > 1) bytes[1] else 0).toInt()
        val byte2 = (if (bytes.size > 2) bytes[2] else 0).toInt()
        val byte3 = (if (bytes.size > 3) bytes[3] else 0).toInt()

        return (byte0 shl 24) + (byte1 shl 16) + (byte2 shl 8) + byte3
    }

    override fun equals(other: Any?): Boolean {
        return if (other !is HashValue) false else bytes.contentEquals(other.bytes)
    }

    val isZero: Boolean
        get() {
            for (i in bytes.indices) {
                if (bytes[i].toInt() != 0)
                    return false
            }
            return true
        }

    fun size(): Int {
        return bytes.size
    }

    fun toHex(): String {
        return bytes.toHex()
    }

    override fun toString(): String {
        return toHex()
    }

    override fun compareTo(other: HashValue): Int {
        val theirHash = other.bytes
        assert(theirHash.size == bytes.size)

        for (i in bytes.indices) {
            val ours = bytes[i].toInt() and 0xFF
            val theirs = theirHash[i].toInt() and 0xFF
            if (ours != theirs)
                return ours - theirs
        }
        return 0
    }

    @Serializer(forClass = HashValue::class)
    companion object : KSerializer<HashValue> {
        override val descriptor: SerialDescriptor = StringDescriptor.withName("org.fejoa.storage.HashValue")

        override fun serialize(encoder: Encoder, obj: HashValue) {
            encoder.encodeString(obj.toHex())
        }

        override fun deserialize(decoder: Decoder): HashValue {
            return HashValue.fromHex(decoder.decodeString())
        }

        fun fromHex(hash: String): HashValue {
            return HashValue(org.fejoa.support.fromHex(hash))
        }
    }
}
