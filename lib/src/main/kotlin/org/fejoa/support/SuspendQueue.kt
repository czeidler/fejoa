package org.fejoa.support

import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

interface Locker {
    suspend fun<T> runSync(block: suspend () -> T): T
}

/**
 * Cross platform lock. Should be replaced by Mutex once Kotlin supports it for JS.
 */
class SuspendQueue(val context: CoroutineContext): Locker {
    private val queue: MutableList<suspend () -> Unit> = ArrayList()

    override suspend fun <T> runSync(block: suspend () -> T): T = suspendCoroutine {
        scheduleUnchecked {
            try {
                val result = block.invoke()
                it.resume(result)
            } catch (e: Throwable) {
                it.resumeWithException(e)
            }
        }
    }

    fun schedule(block: suspend () -> Unit) {
        scheduleUnchecked {
            try {
                block.invoke()
            } catch (e: Throwable) {
                // just catch the exception
            }
        }
    }

    private fun scheduleUnchecked(block: suspend () -> Unit) {
        queue.add(block)
        triggerWork()
    }

    private var working = false

    private fun triggerWork() {
        if (working)
            return
        working = true

        async(context) {
            try {
                while (queue.isNotEmpty())
                    queue.removeAt(0).invoke()
            } finally {
                working = false
            }
        }
    }
}
