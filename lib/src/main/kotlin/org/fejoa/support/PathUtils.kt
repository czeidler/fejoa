package org.fejoa.support

object PathUtils {
    fun appendDir(baseDir: String, dir: String): String {
        var newDir = baseDir
        if (dir == "")
            return baseDir
        if (newDir != "")
            newDir += "/"
        newDir += dir
        return newDir
    }

    fun fileName(path: String): String {
        val lastSlash = path.lastIndexOf("/")
        return if (lastSlash < 0) path else path.substring(lastSlash + 1)
    }

    fun dirName(path: String): String {
        val lastSlash = path.lastIndexOf("/")
        return if (lastSlash < 0) "" else path.substring(0, lastSlash)
    }

    const val PERCENT_VALUE = "25"
    const val SLASH_VALUE = "2F"

    fun toPath(value: String): String {
        // "%" -> %25, "/" -> %2F
        var out = ""
        for (char in value) {
            out += when (char) {
                '%' -> "%$PERCENT_VALUE"
                '/' -> "%$SLASH_VALUE"
                else -> "$char"
            }
        }
        return out
    }

    fun fromPath(path: String): String {
        var out = ""
        var i = 0
        while (i < path.length) {
            val char = path[i]
            i++
            out += when (char) {
                '%' -> {
                    if (i + 2 > path.length)
                        throw Exception("Invalid path encoding")
                    val encodingValue = path.substring(i, i + 2)
                    i += 2
                    when (encodingValue) {
                        PERCENT_VALUE -> "%"
                        SLASH_VALUE -> "/"
                        else -> throw Exception("Unknown encoding value: $encodingValue")
                    }
                }
                else -> "$char"
            }
        }
        return out
    }
}