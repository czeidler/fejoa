package org.fejoa.support

import kotlin.coroutines.CoroutineContext

expect fun getWorkerCoroutineContext(): CoroutineContext