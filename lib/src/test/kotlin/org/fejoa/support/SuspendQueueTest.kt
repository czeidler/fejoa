package org.fejoa.support

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers.Unconfined
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import org.fejoa.test.testAsync
import kotlin.test.Test
import kotlin.test.assertFails


class SuspendQueueTest {
    suspend fun worker(count: Int, out: MutableList<Int>) {
        (0 .. 9).forEach {
            out.add(count * 10 + it)
            delay(1)
        }
    }
    suspend fun problem(queue: SuspendQueue?): List<Int> {
        val out: MutableList<Int> = ArrayList()
        val jobs: MutableList<Deferred<Unit>> = ArrayList()
        (0 .. 9).forEach { loop1 ->
            jobs += GlobalScope.async(Unconfined) {
                if (queue != null) {
                    queue.runSync {
                        worker(loop1, out)
                    }
                } else
                    worker(loop1, out)
            }
        }

        jobs.forEach { it.await() }
        return out
    }

    fun validateOrder(list: List<Int>) {
        list.forEachIndexed { index, i ->
            if (i != index)
                throw Exception("Not sorted")
        }
    }

    @Test
    fun testProblem() = testAsync {
        val result = problem(null)
        assertFails { validateOrder(result) }

        val queue = SuspendQueue(this.coroutineContext)
        val result2 = problem(queue)
        validateOrder(result2)
    }
}