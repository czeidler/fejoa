package org.fejoa.cli

import org.fejoa.Branch
import org.fejoa.Client
import org.fejoa.UserData
import org.fejoa.fs.fusejnr.FejoaFuse
import org.fejoa.fs.fusejnr.Utils
import org.fejoa.storage.StorageDir
import org.fejoa.support.toUTF
import org.fejoa.withLooper
import ru.serce.jnrfuse.FuseFS
import java.io.File
import java.io.IOException
import kotlin.coroutines.experimental.CoroutineContext


class MountManager(private val client: Client, private val path: String) {
    private class Mount(private val fuseFs: FuseFS, val storageDir: StorageDir, val description: String) {
        fun umount() {
            fuseFs.umount()
        }
    }

    // path -> Mount
    private val mountMap: MutableMap<String, Mount> = HashMap()

    private fun key(path: String, branch: String): String {
        return "$path:$branch"
    }

    /**
     * @return the mount point relative to the given path
     */
    suspend fun mount(branch: Branch, coroutineContext: CoroutineContext): String {
        val branchInfo = branch.branchInfo.get()
        val dirName = "${branchInfo.branch} (${branchInfo.description.replace(" ", "_")})"
        val mountPoint = File(path, dirName)
        mountPoint.mkdirs()
        if (mountPoint.list().isNotEmpty())
            throw IOException("Mount point ${mountPoint.path} is not empty")
        val mountDir = mountPoint.absoluteFile.toPath()

        client.withLooper {
            val storageDir = userData.getStorageDir(branch)
            val fileSystem = FejoaFuse(storageDir, coroutineContext)

            Utils.blockingMount(fileSystem, mountDir)

            mountMap[key(path, branch.branch.toHex())] = Mount(fileSystem, storageDir, branchInfo.description)
        }
        return dirName
    }

    fun umount() {
        mountMap.forEach { it.value.umount() }
        mountMap.clear()
    }

    suspend fun umount(branch: String) {
        client.withLooper {
            mountMap.remove(key(path, branch))?.let {
                it.storageDir.commit("Umount".toUTF())
                it.umount()
            }
        }
    }
}
