package org.fejoa.cli

import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.asCoroutineDispatcher
import kotlinx.coroutines.experimental.future.future
import org.fejoa.Client
import org.fejoa.SinglePasswordGetter
import org.fejoa.library.support.LooperExecutor
import java.io.File
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors


/**
 * Manages all open clients
 */
class ClientManager {
    class ClientEntry(val client: Client, val path: String) {
        val mountManager: MountManager = MountManager(client, path)

        fun close() {
            mountManager.umount()
            client.stop()
        }
    }

    /**
     * Path -> Client
     */
    private val openClients: MutableMap<String, ClientEntry> = ConcurrentHashMap()
    private val executor = Executors.newCachedThreadPool()

    private fun getClientCoroutineContext(): CoroutineDispatcher {
        return LooperExecutor(executor).asCoroutineDispatcher()
    }

    fun open(homeDir: File, password: String): Client = future {
        val homeDirPath = homeDir.absolutePath
        if (openClients.containsKey(homeDirPath))
            throw Exception("Abort open client, already exist for $homeDir")
        val client = Client.open(homeDir.parent, homeDir.name, password, getClientCoroutineContext())
        client.connectionAuthManager.passwordGetter = SinglePasswordGetter(password)
        start(client)
        openClients[homeDirPath] = ClientEntry(client, homeDir.path)
        return@future client
    }.join()

    fun init(homeDir: File, password: String): Client = future {
        val client = Client.create(homeDir.parent, homeDir.name, password,
                coroutineContext = getClientCoroutineContext())
        client.connectionAuthManager.passwordGetter = SinglePasswordGetter(password)
        client.userData.commit()
        start(client)
        openClients[homeDir.absolutePath] = ClientEntry(client, homeDir.path)
        return@future client
    }.join()

    fun getOpenClients(): Map<String, ClientEntry> = openClients

    fun getOpenClient(path: String): ClientEntry? = openClients[path]

    private fun close(entry: ClientEntry) = future(entry.client.context.coroutineContext) {
        entry.close()
    }.join()

    /**
     * @return false if no client for the given homeDir is open
     */
    fun close(homeDir: String): Boolean {
        val entry = openClients.remove(homeDir) ?: return false
        close(entry)
        return true
    }

    fun close() {
        openClients.forEach {_, entry -> close(entry) }
        while (openClients.isNotEmpty()) {
            val toBeClosed = openClients.map {
                it.key
            }
            toBeClosed.forEach {
                close(it)
            }
        }

        executor.shutdown()
    }

    private suspend fun start(client: Client) {
        client.start()
    }
}
