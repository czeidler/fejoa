/*
 * Copyright 2015.
 * Distributed under the terms of the GPLv3 License.
 *
 * Authors:
 *      Clemens Zeidler <czei002@aucklanduni.ac.nz>
 */
package org.fejoa.library.support

import java.util.ArrayList
import java.util.concurrent.Executor
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock


/**
 * Executor for running jobs in a given order
 *
 * In contrast to the normal executor services the LooperExecutor can use a shared worker executor. For example,
 * multiple LooperExecutors can work their queue using a common thread pool while ensuring the local job order.
 *
 * @param executor the executor who does the real work
 */
class LooperExecutor(capacity: Int, private val executor: Executor) : Executor {
    private var quit = false
    private val jobs: MutableList<Runnable>
    private val isWorking = AtomicBoolean(false)
    private var currentThread: Thread? = null

    val currentThreadId: Long
        get() = synchronized(this) {
            return if (currentThread == null) -1 else currentThread!!.id
        }

    init {
        // TODO use a blocking queue?
        jobs = ArrayList(capacity)
    }

    constructor(executor: Executor) : this(20, executor)

    private fun work() {
        synchronized(this) {
            if (isWorking.get())
                return
            if (jobs.size == 0)
                return
            isWorking.set(true)
            val job = jobs.removeAt(0)
            executor.execute {
                synchronized(this@LooperExecutor) {
                    currentThread = Thread.currentThread()
                }
                job.run()
                synchronized(this@LooperExecutor) {
                    currentThread = null
                    isWorking.set(false)
                    work()
                }
            }
        }
    }

    fun quit(waitTillFinished: Boolean) {
        synchronized(this) {
            if (quit)
                return
            quit = true
        }
        val lock = ReentrantLock()
        val finished = lock.newCondition()

        executeForced(Runnable {
            if (waitTillFinished) {
                lock.lock()
                try {
                    finished.signal()
                } finally {
                    lock.unlock()
                }
            }
        }, false)
        if (waitTillFinished) {
            lock.lock()
            try {
                finished.await()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            } finally {
                lock.unlock()
            }
        }
    }

    override fun execute(runnable: Runnable) {
        execute(runnable, false)
    }

    fun execute(runnable: Runnable, insertAtFront: Boolean): Boolean {
        if (quit)
            return false
        executeForced(runnable, insertAtFront)
        return true
    }

    /**
     * Queues a job even after quit has been called (needed to send the final job)
     */
    private fun executeForced(runnable: Runnable, insertAtFront: Boolean) {
        synchronized(this) {
            if (insertAtFront)
                jobs.add(0, runnable)
            else
                jobs.add(runnable)
        }
        // start work
        work()
    }
}
