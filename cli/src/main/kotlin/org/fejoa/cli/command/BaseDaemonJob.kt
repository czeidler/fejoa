package org.fejoa.cli.command

import kotlinx.coroutines.experimental.async
import org.fejoa.Client
import org.fejoa.cli.ClientManager
import org.fejoa.cli.Daemon
import org.fejoa.cli.DaemonJob

/**
 * Base job for jobs that requires an open client
 */
abstract class BaseDaemonJob(val daemon: Daemon) : DaemonJob() {
    override suspend fun runInternal(path: String, args: String?): Any? {
        val entry = daemon.clientManager.getOpenClient(path)
                ?: return failJob("Client need to be open")
        return async(entry.client.context.coroutineContext) {
            runInternal(entry, path, args)
        }.await()
    }

    /**
     * Is run in the client executor context
     */
    protected abstract suspend fun runInternal(entry: ClientManager.ClientEntry, path: String, jsonArgs: String?): Any?
}
