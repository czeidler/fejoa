package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.ClientManager
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import picocli.CommandLine


const val MOUNT_COMMAND_NAME = "mount"

class MountClientCommand : IClientCommand {
    @CommandLine.Command(name = MOUNT_COMMAND_NAME, description = ["Mount data branches"])
    class MountArgs : ArgsBase() {
        @CommandLine.Parameters(index = "0", description = ["branch"])
        var branch: String = ""
    }

    override fun createArgs(): Any = MountArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class MountDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    override suspend fun runInternal(entry: ClientManager.ClientEntry, path: String, jsonArgs: String?): Any? {
        val client = entry.client
        val args = ObjectMapper().readValue(jsonArgs, MountClientCommand.MountArgs::class.java)
        val branch = client.userData.getBranches().firstOrNull {
            it.branch.toHex().startsWith(args.branch)
        } ?: return failJob("Branch not found")

        val dirName = entry.mountManager.mount(branch, client.context.coroutineContext)
        return reportProgress("Branch ${branch.branch} mounted at $dirName")
    }
}
