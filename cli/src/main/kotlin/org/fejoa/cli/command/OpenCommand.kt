package org.fejoa.cli.command

import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.experimental.future.future
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.Daemon
import org.fejoa.cli.DaemonJob
import org.fejoa.cli.IClientCommand
import picocli.CommandLine
import java.io.Console
import java.io.File


const val OPEN_COMMAND_NAME = "open"

class OpenClientCommand : IClientCommand {
    @CommandLine.Command(name = OPEN_COMMAND_NAME, description = ["Open a local account"])
    class OpenArgs : ArgsBase() {
        @CommandLine.Option(names = ["-p", "--password"], description = ["password"])
        var password: String = ""
    }

    override fun createArgs(): Any = OpenArgs()

    override fun start(argObject: Any): String? {
        assert(argObject is OpenArgs)
        val args = argObject as OpenArgs
        if (args.password == "") {
            val console: Console = System.console() ?: return null
            args.password = console.readPassword("Enter password").contentToString()
        }
        val mapper = ObjectMapper()
        return mapper.writeValueAsString(args)
    }
}

class OpenDaemonJob(private val daemon: Daemon) : DaemonJob() {
    override suspend fun runInternal(path: String, args: String?): Any? {
        val mapper = ObjectMapper()
        val argsObject = mapper.readValue(args.toString(), OpenClientCommand.OpenArgs::class.java)

        daemon.clientManager.getOpenClients()[path]?.let {
            return future(it.client.context.coroutineContext) {
                failJob("Client already open: " + it.client.userData.id.get())
            }.join()
        }

        val homeDir = File(path)
        if (!homeDir.isDirectory)
            return failJob("Internal error: path \"$path\" must be a directory")
        if (homeDir.list().isEmpty())
            return failJob("Directory is empty")

        try {
            val client = daemon.clientManager.open(homeDir, argsObject.password)
            return reportProgress("Open client succeeded: ${client.userData.id.get()}")
        } catch (e: Exception) {
            return failJob("Open client FAILED, wrong password?")
        }
    }
}


