package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.ClientManager
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import picocli.CommandLine


const val UNMOUNT_COMMAND_NAME = "umount"

class UmountClientCommand : IClientCommand {
    @CommandLine.Command(name = UNMOUNT_COMMAND_NAME, description = ["Un-mount a data branches"])
    class UmountArgs : ArgsBase() {
        @CommandLine.Parameters(index = "0", description = ["branch"])
        var branch: String = ""
    }

    override fun createArgs(): Any = UmountArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class UmountDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    override suspend fun runInternal(entry: ClientManager.ClientEntry, path: String, jsonArgs: String?): Any? {
        val args = ObjectMapper().readValue(jsonArgs,
                UmountClientCommand.UmountArgs::class.java)
        entry.mountManager.umount(args.branch)
        return null
    }
}
