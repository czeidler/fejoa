package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.LoginAuthInfo
import org.fejoa.UserData
import org.fejoa.cli.*
import org.fejoa.network.ReturnType
import picocli.CommandLine


const val REGISTER_COMMAND_NAME = "register"

@CommandLine.Command(name = PING_COMMAND_NAME, description = ["Register an account at a CSP"])
class RegisterArgs : ArgsBase() {
    @CommandLine.Option(names = ["-p", "--password"], description = ["password"], required = false)
    var password: String? = null

    @CommandLine.Parameters(index = "0", description = ["user name"])
    var userName: String? = null

    @CommandLine.Parameters(index = "1", description = ["server"])
    var server: String? = null
}

class RegisterCommand : IClientCommand {
    override fun createArgs(): Any {
        return RegisterArgs()
    }

    override fun start(argObject: Any): String? {
        assert(argObject is RegisterArgs)
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class RegisterDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    override suspend fun runInternal(entry: ClientManager.ClientEntry, path: String, jsonArgs: String?): Any? {
        val mapper = ObjectMapper()
        val args : RegisterArgs = mapper.readValue(jsonArgs.toString(), RegisterArgs::class.java)

        val password = args.password ?: run {
            val password1 = sendRequest(InputRequest("Enter registration password", true))
            if (!validatePassword(password1))
                return failJob("Password too simple")
            val password2 = sendRequest(InputRequest("Re-enter registration password", true))
            if (password1 != password2)
                return failJob("Password mismatch")
            password1
        }

        val client = entry.client
        val result = client.registerAccount(args.userName!!, args.server!!, password)
        if (result.first.code != ReturnType.OK)
            return failJob("Registering failed: ${result.first.message}")

        // add the remote to all user data branches
        val remote = result.second!!
        client.setBranchLocation(remote, LoginAuthInfo(),  UserData.USER_DATA_CONTEXT)

        client.userData.commit()

        // restart to sync to the new remote
        client.restart()

        reportProgress("Registered to ${args.userName}@${args.server}")
        return null
    }
}