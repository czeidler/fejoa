package org.fejoa.cli.command

import org.fejoa.cli.*


class CommandFactory {
    abstract class Factory {
        var name: String = ""
        var rpcName: String = ""
        abstract fun createClientCommand(): IClientCommand
        abstract fun createDaemonJob(daemon: Daemon): DaemonJob
    }

    class SubCommand(val baseName: String, val name: String) {
        private var factory: Factory? = null
        val subCommands: MutableList<SubCommand> = ArrayList()

        fun subCommand(subCommandName: String, block: SubCommand.() -> Unit): SubCommand {
            val subCommand = SubCommand(rpcName(), subCommandName)
            subCommand.apply(block)
            subCommands.add(subCommand)
            return subCommand
        }

        fun factory(factory: Factory) {
            factory.rpcName = rpcName()
            factory.name = name
            this.factory = factory
        }

        fun rpcName(): String {
            if (baseName == "")
                return name
            return baseName + name.capitalize()
        }

        fun getFactory(): Factory {
            return factory ?: throw Exception("No factory set")
        }

        fun getFactories(): List<Factory> {
            val factories: MutableList<Factory> = ArrayList()
            factory?.let { factories.add(it) }
            subCommands.forEach { factories.addAll(it.getFactories()) }
            return factories
        }
    }

    fun root(block: SubCommand.() -> Unit): SubCommand {
        return SubCommand("", "").apply(block)
    }

    val root: SubCommand

    init {
        root = root {
            subCommand(PING_COMMAND_NAME) {
                factory(object : Factory() {
                    override fun createClientCommand(): IClientCommand = PingClientCommand()
                    override fun createDaemonJob(daemon: Daemon): DaemonJob = PingDaemonJob(daemon)
                })
            }
            subCommand(CHAT_BOT_COMMAND_NAME) {
                factory(object : Factory() {
                    override fun createClientCommand(): IClientCommand = ChatBotCommand()
                    override fun createDaemonJob(daemon: Daemon): DaemonJob = ChatBotJob()
                })
            }
            subCommand(STATUS_COMMAND_NAME) {
                factory(object : Factory() {
                    override fun createClientCommand(): IClientCommand = StatusClientCommand()
                    override fun createDaemonJob(daemon: Daemon): DaemonJob = StatusDaemonJob(daemon)
                })
            }
            subCommand(CREATE_COMMAND_NAME) {
                factory(object : Factory() {
                    override fun createClientCommand(): IClientCommand = CreateClientCommand()
                    override fun createDaemonJob(daemon: Daemon): DaemonJob = CreateDaemonJob(daemon)
                })
            }
            subCommand(OPEN_COMMAND_NAME) {
                factory(object : Factory() {
                    override fun createClientCommand(): IClientCommand = OpenClientCommand()
                    override fun createDaemonJob(daemon: Daemon): DaemonJob = OpenDaemonJob(daemon)
                })
            }
            subCommand(CLOSE_COMMAND_NAME) {
                factory(object : Factory() {
                    override fun createClientCommand(): IClientCommand = CloseClientCommand()
                    override fun createDaemonJob(daemon: Daemon): DaemonJob = CloseDaemonJob(daemon)
                })
            }
            subCommand(REGISTER_COMMAND_NAME) {
                factory(object : Factory() {
                    override fun createClientCommand(): IClientCommand = RegisterCommand()
                    override fun createDaemonJob(daemon: Daemon): DaemonJob = RegisterDaemonJob(daemon)
                })
            }
            subCommand(REMOTE_COMMAND_NAME) {
                factory(object : Factory() {
                    override fun createClientCommand(): IClientCommand = RemoteCommand()
                    override fun createDaemonJob(daemon: Daemon): DaemonJob = RemoteDaemonJob(daemon)
                })
                subCommand(REMOTE_ADD_COMMAND_NAME) {
                    factory(object : Factory() {
                        override fun createClientCommand(): IClientCommand = RemoteAddCommand()
                        override fun createDaemonJob(daemon: Daemon): DaemonJob = RemoteAddDaemonJob(daemon)
                    })
                }
            }
            subCommand(CONTACT_COMMAND_NAME) {
                factory(object : Factory() {
                    override fun createClientCommand(): IClientCommand = ContactClientCommand()
                    override fun createDaemonJob(daemon: Daemon): DaemonJob = ContactDaemonJob(daemon)
                })
                subCommand(CONTACT_REQUEST_COMMAND_NAME) {
                    factory(object : Factory() {
                        override fun createClientCommand(): IClientCommand = ContactRequestClientCommand()
                        override fun createDaemonJob(daemon: Daemon): DaemonJob = ContactRequestDaemonJob(daemon)
                    })
                }
                subCommand(CONTACT_ACCEPT_COMMAND_NAME) {
                    factory(object : Factory() {
                        override fun createClientCommand(): IClientCommand = ContactAcceptClientCommand()
                        override fun createDaemonJob(daemon: Daemon): DaemonJob = ContactAcceptDaemonJob(daemon)
                    })
                }
            }
            subCommand(USER_DATA_COMMAND_NAME) {
                factory(object : Factory() {
                    override fun createClientCommand(): IClientCommand = UserDataClientCommand()
                    override fun createDaemonJob(daemon: Daemon): DaemonJob = UserDataDaemonJob(daemon)
                })
            }
            subCommand(MOUNT_COMMAND_NAME) {
                factory(object : Factory() {
                    override fun createClientCommand(): IClientCommand = MountClientCommand()
                    override fun createDaemonJob(daemon: Daemon): DaemonJob = MountDaemonJob(daemon)
                })
            }
            subCommand(UNMOUNT_COMMAND_NAME) {
                factory(object : Factory() {
                    override fun createClientCommand(): IClientCommand = UmountClientCommand()
                    override fun createDaemonJob(daemon: Daemon): DaemonJob = UmountDaemonJob(daemon)
                })
            }
            subCommand(STORAGE_COMMAND_NAME) {
                factory(object : Factory() {
                    override fun createClientCommand(): IClientCommand = StorageClientCommand()
                    override fun createDaemonJob(daemon: Daemon): DaemonJob = StorageDaemonJob(daemon)
                })
                subCommand(ADD_STORAGE_NAME) {
                    factory(object : Factory() {
                        override fun createClientCommand(): IClientCommand = AddStorageCommand()
                        override fun createDaemonJob(daemon: Daemon): DaemonJob = AddStorageDaemonJob(daemon)
                    })
                }
            }
        }
    }

    fun getRpcFactories(): Map<String, Factory> {
        return root.getFactories().associateBy({it.rpcName}, {it})
    }
}
