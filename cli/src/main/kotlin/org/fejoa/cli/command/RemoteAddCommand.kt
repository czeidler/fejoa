package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.async
import org.fejoa.Client
import org.fejoa.Remote
import org.fejoa.UserData
import org.fejoa.cli.*
import org.fejoa.storage.HashValue
import picocli.CommandLine

const val REMOTE_ADD_COMMAND_NAME = "add"

@CommandLine.Command(name = REMOTE_ADD_COMMAND_NAME, description = ["Add a new remote CSP"])
class RemoteAddArgs : ArgsBase() {
    @CommandLine.Parameters(index = "0", description = ["user name"])
    var userName: String? = null

    @CommandLine.Parameters(index = "1", description = ["server"])
    var server: String? = null
}

class RemoteAddCommand : IClientCommand {

    override fun createArgs(): Any = RemoteAddArgs()

    override fun start(argObject: Any): String? {
        assert(argObject is RemoteAddArgs)
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class RemoteAddDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    override suspend fun runInternal(entry: ClientManager.ClientEntry, path: String, jsonArgs: String?): Any? {
        val mapper = ObjectMapper()
        val args : RemoteAddArgs = mapper.readValue(jsonArgs.toString(), RemoteAddArgs::class.java)
        val remote = Remote.create(args.userName!!, args.server!!)
        val client = entry.client
        val addRemoteResult = client.userData.remotes.get(HashValue.fromHex(remote.id)).write(remote)
        client.userData.commit()
        return addRemoteResult
    }
}