package org.fejoa.cli.command

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.fejoa.Client
import org.fejoa.cli.ArgsBase
import org.fejoa.cli.ClientManager
import org.fejoa.cli.Daemon
import org.fejoa.cli.IClientCommand
import picocli.CommandLine


const val USER_DATA_COMMAND_NAME = "userdata"

class UserDataClientCommand : IClientCommand {
    @CommandLine.Command(name = USER_DATA_COMMAND_NAME, description = arrayOf("List user data info"))
    class UserDataArgs : ArgsBase()

    override fun createArgs(): Any = UserDataArgs()

    override fun start(argObject: Any): String? {
        return ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(argObject)
    }
}

class UserDataDaemonJob(daemon: Daemon) : BaseDaemonJob(daemon) {
    override suspend fun runInternal(entry: ClientManager.ClientEntry, path: String, jsonArgs: String?): Any? {
        entry.client.userData.getBranches().forEach {
            val branchInfo = it.branchInfo.get()
            reportProgress("${branchInfo.branch}: ${branchInfo.description}")
        }
        return null
    }
}
