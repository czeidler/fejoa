package org.fejoa.cli

import java.io.File
import java.nio.file.Files
import java.nio.file.attribute.PosixFilePermission
import java.util.*

interface IPlatform {
    fun getHomeDir(): File
    fun makeNamedPipe(name: File)
}

val platform: IPlatform by lazy { detectPlatform() }

private fun detectPlatform(): IPlatform {
    val osName = System.getProperty("os.name")
    if (osName.indexOf("nux") == 0)
        throw RuntimeException("Unsupported OS: $osName")
    return UnixPlatform()
}

private class UnixPlatform : IPlatform {
    override fun getHomeDir(): File {
        return File(System.getProperty("user.home"))
    }

    override fun makeNamedPipe(pipe: File) {
        pipe.parentFile.mkdirs()

        val command = arrayOf("mkfifo", pipe.absolutePath)
        val process = ProcessBuilder(*command).inheritIO().start()
        val result = process.waitFor()
        if (result != 0)
            throw Exception("Can't create named pipe: $result")

        // only read/writable for the owner
        Files.setPosixFilePermissions(pipe.toPath(), EnumSet.of<PosixFilePermission>(PosixFilePermission.OWNER_READ, PosixFilePermission.OWNER_WRITE))
    }
}