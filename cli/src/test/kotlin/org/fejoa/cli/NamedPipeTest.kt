package org.fejoa.cli

import kotlinx.coroutines.experimental.*
import org.fejoa.support.StorageLib
import kotlin.test.*
import org.junit.After
import org.junit.Assert.*
import org.junit.Test
import java.io.File
import java.util.ArrayList
import kotlin.system.measureTimeMillis


class NamedPipeTest {
    private val cleanUpDirs = ArrayList<String>()

    @After
    fun cleanUp() {
        for (dir in cleanUpDirs)
            StorageLib.recursiveDeleteFile(File(dir))
    }

    @Test
    fun testPipeCreation() {
        val pipeName = "test.pipe"
        val pipeFile = getPipeFile(pipeName)
        cleanUpDirs.add(pipeFile.path)
        cleanUp()

        NamedPipe.create(pipeFile)
        try {
            // pipe exist so this should fail
            NamedPipe.create(pipeFile)
            assertFalse(true)
        } catch (e: Exception) {
        }

        NamedPipe.delete(pipeFile)
    }

    @Test
    fun testBasics() {
        runBlocking {
            val pipeName = "test.pipe"
            val pipeFile = getPipeFile(pipeName)
            cleanUpDirs.add(pipeFile.path)
            cleanUp()

            NamedPipe.create(pipeFile)
            launch(CommonPool) {
                val outPipe = NamedPipe.openForWrite(pipeFile)
                outPipe.writeDataPackage("Hello".toByteArray())

                outPipe.writeDataPackage("World".toByteArray())
                outPipe.close()
            }

            val inPipe = NamedPipe.openForRead(pipeFile)
            var stringRead = String(inPipe.readDataPackage())
            assert(stringRead == "Hello")
            stringRead = String(inPipe.readDataPackage())
            assert(stringRead == "World")
            inPipe.close()

            NamedPipe.delete(pipeFile)
        }
    }

    @Test
    fun testBasicsOpenReadFirst() {
        runBlocking {
            val pipeName = "test.pipe"
            val pipeFile = getPipeFile(pipeName)
            cleanUpDirs.add(pipeFile.path)
            cleanUp()

            NamedPipe.create(pipeFile)
            var readJob = async(CommonPool) {
                delay(100)
                val inPipe = NamedPipe.openForRead(pipeFile)
                var stringRead = String(inPipe.readDataPackage())
                assert(stringRead == "Hello")
                stringRead = String(inPipe.readDataPackage())
                assert(stringRead == "World")
                inPipe.close()
            }


            val outPipe = NamedPipe.openForWrite(pipeFile)
            outPipe.writeDataPackage("Hello".toByteArray())

            outPipe.writeDataPackage("World".toByteArray())
            outPipe.close()

            readJob.await()

            NamedPipe.delete(pipeFile)
        }
    }

    @Test
    fun testReadTimeout() {
        runBlocking {
            val pipeName = "test.pipe"
            val pipeFile = getPipeFile(pipeName)
            cleanUpDirs.add(pipeFile.path)
            cleanUp()

            NamedPipe.create(pipeFile)
            launch(CommonPool) {
                val outPipe = NamedPipe.openForWrite(pipeFile)
                delay(100)
                outPipe.writeDataPackage("Hello".toByteArray())
                delay(1000)
                outPipe.writeDataPackage("World".toByteArray())
                outPipe.close()
            }

            val inPipe = NamedPipe.openForRead(pipeFile)
            assertTrue(200 > measureTimeMillis {
                var stringRead = String(inPipe.readDataPackage(200) ?: "".toByteArray())
                assertEquals("Hello", stringRead)
            })
            assertFails {
                var stringRead = String(inPipe.readDataPackage(200) ?: "".toByteArray())
                assertEquals("", stringRead)
            }
            inPipe.close()

            NamedPipe.delete(pipeFile)
        }
    }

    @Test
    @Throws(Exception::class)
    fun testWriteTimeout() {
        val pipeName = "test.pipe"
        val pipeFile = getPipeFile(pipeName)
        cleanUpDirs.add(pipeFile.path)
        cleanUp()

        NamedPipe.create(pipeFile)
        try {
            val outPipe = NamedPipe.openForWrite(pipeFile, 100)
            outPipe.writeDataPackage("Hello".toByteArray())
            // should not be reached because we time out
            assertFalse(true)
        } catch (e: Exception) {
        }

        runBlocking {
            NamedPipe.create(pipeFile)

            val writeJob = async(CommonPool) {
                val outPipe = NamedPipe.openForWrite(pipeFile, 100)
                outPipe.writeDataPackage("Hello".toByteArray())
            }

            async(CommonPool) {
                delay(50)
                val inPipe = NamedPipe.openForRead(pipeFile)
                val stringRead = String(inPipe.readDataPackage())
                assertEquals("Hello", stringRead)
            }.await()

            writeJob.await()
        }
    }

    @Test
    @Throws(Exception::class)
    fun testMultipleWrites() {
        val pipeName = "test.pipe"
        val pipeFile = getPipeFile(pipeName)
        cleanUpDirs.add(pipeFile.path)
        cleanUp()

        var longString = ""
        for (i in 1 .. 100)
            longString += "long string"

        NamedPipe.create(pipeFile)
        val inPipe = NamedPipe.openForRead(pipeFile)

        val jobs: MutableList<List<String>> = ArrayList()
        val all: MutableList<String> = ArrayList()
        val nJobs = 4
        val nStrings = 50
        for (jobId in 1..nJobs) {
            val job: MutableList<String> = (1..nStrings).mapTo(ArrayList()) { "Job $jobId: $it" }
            jobs.add(job)
            all.addAll(job)
        }

        runBlocking {
            val runningJobs = jobs.map { job ->
                async(CommonPool) {
                    job.forEach { string ->
                        var outPipe = NamedPipe.openForWrite(pipeFile)
                        try {
                            outPipe.writeDataPackage(string.toByteArray())
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        outPipe.close()
                        delay(10)
                    }
                }
            }


            for (i in 1..(nJobs * nStrings)) {
                val read = String(inPipe.readDataPackage())
                assertTrue(all.contains(read))
                all.remove(read)
            }
            assertEquals(0, all.size)

            runningJobs.forEach { it.await() }
            inPipe.close()
        }
    }
}
