package org.fejoa.fs.fusejnr

import com.kenai.jffi.MemoryIO
import jnr.constants.platform.Errno
import jnr.ffi.Pointer
import jnr.ffi.types.off_t
import org.fejoa.repository.AttrType
import org.fejoa.repository.FSAttribute
import org.fejoa.storage.*
import ru.serce.jnrfuse.FuseFillDir
import ru.serce.jnrfuse.FuseStubFS
import ru.serce.jnrfuse.struct.FileStat
import ru.serce.jnrfuse.struct.FuseFileInfo
import ru.serce.jnrfuse.struct.Statvfs
import ru.serce.jnrfuse.struct.Timespec

import java.io.FileNotFoundException
import java.io.IOException
import java.nio.ByteBuffer
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicLong
import kotlin.coroutines.Continuation
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.startCoroutine


fun <T> future(context: CoroutineContext, block: suspend () -> T): CompletableFuture<T> =
        CompletableFutureCoroutine<T>(context).also { block.startCoroutine(completion = it) }

class CompletableFutureCoroutine<T>(override val context: CoroutineContext) : CompletableFuture<T>(), Continuation<T> {
    override fun resumeWith(result: Result<T>) {
        result.onSuccess { complete(it) }
                .onFailure { completeExceptionally(it) }
    }
}

class FejoaFuse(private val storageDir: StorageDir, private val coroutineContext: CoroutineContext) : FuseStubFS() {
    private val openFilesManager = OpenFilesManager()

    internal class OpenFilesManager {
        private val handleCounter = AtomicLong(0)
        private val openFiles = ConcurrentHashMap<Long, RandomDataAccess>()

        private val newHandle: Long
            get() = handleCounter.incrementAndGet()

        fun getFile(handle: Long): RandomDataAccess? {
            return openFiles[handle]
        }

        fun putFile(channel: RandomDataAccess): Long {
            val handle = newHandle
            openFiles[handle] = channel
            return handle
        }

        @Throws(IOException::class)
        suspend fun release(handle: Long?) {
            val channel = openFiles.remove(handle) ?: return
            channel.close()
        }
    }

    private fun error(errno: Errno): Int {
        return -errno.intValue()
    }

    override fun getattr(path: String, stat: FileStat): Int = future(coroutineContext) {
        var p = path
        p = parsePath(p)
        try {
            val type = storageDir.probe(p)
            val dummy = when (type) {
                IODatabase.FileType.FILE -> {
                    val dataAccess = storageDir.open(p, RandomDataAccess.READ)
                    stat.st_size.set(dataAccess.length())
                    dataAccess.close()
                    stat.st_mode.set((FileStat.S_IFREG or 432).toLong())
                }
                IODatabase.FileType.DIRECTORY -> stat.st_mode.set((FileStat.S_IFDIR or 493).toLong())
                IODatabase.FileType.NOT_EXISTING -> {
                    return@future error(Errno.ENOENT)
                }
            }
            (storageDir.getBackingDatabase().getAttribute(p, AttrType.FS_ATTR) as? FSAttribute)?.let {
                stat.st_mtim.tv_nsec.set(it.mTime)
                stat.st_atim.tv_nsec.set(it.mTime)
                stat.st_mode.set((it.mode shl 6) or (it.mode shl 3))
            }
        } catch (e: Exception) {
            return@future exceptionToError(e)
        }

        val fuseContext = context
        stat.st_uid.set(fuseContext.uid.get())
        stat.st_gid.set(fuseContext.gid.get())
        return@future 0
    }.join()

    override fun chmod(path: String, mode: Long): Int = future(coroutineContext) {
        // TODO do some mode checks
        val cloudEFSMode = mode.toInt() shr 6 and 0x7
        try {
            val attr = (storageDir.getBackingDatabase().getAttribute(path, AttrType.FS_ATTR) as? FSAttribute)?.let {
                it.mode = cloudEFSMode
                it
            } ?: FSAttribute(cloudEFSMode, 0)
            storageDir.getBackingDatabase().setAttribute(path, attr)
            return@future 0
        } catch (e: Exception) {
            return@future exceptionToError(e)
        }
    }.join()

    override fun chown(path: String?, uid: Long, gid: Long): Int {
        val fuseContext = context
        if (uid != fuseContext.uid.get() || gid != fuseContext.gid.get())
            return errnoToError(Errno.EACCES)
        return 0
    }

    override fun utimens(path: String, timespec: Array<out Timespec>): Int = future(coroutineContext) {
        try {
            val attr = (storageDir.getBackingDatabase().getAttribute(path, AttrType.FS_ATTR) as? FSAttribute)
                    ?: FSAttribute(0, 0)
            attr.mTime = timespec[1].tv_nsec.longValue()
            storageDir.getBackingDatabase().setAttribute(path, attr)
            return@future 0
        } catch (e: Exception) {
            return@future exceptionToError(e)
        }
    }.join()

    override fun mkdir(path: String, mode: Long): Int = future(coroutineContext) {
        try {
            storageDir.mkDir(path)
            0
        } catch (e: Exception) {
            exceptionToError(e)
        }
    }.join()

    override fun readdir(path: String, buf: Pointer?, filter: FuseFillDir, @off_t offset: Long, fi: FuseFileInfo?)
            : Int = future(coroutineContext) {
        val p = parsePath(path)

        filter.apply(buf, ".", null, 0)
        filter.apply(buf, "..", null, 0)
        try {
            val dirs = storageDir.listDirectories(p)
            for (dir in dirs)
                filter.apply(buf, dir, null, 0)
            val files = storageDir.listFiles(p)
            for (file in files)
                filter.apply(buf, file, null, 0)
        } catch (e: Exception) {
            return@future error(Errno.EIO)
        }

        return@future 0
    }.join()

    private fun parsePath(path: String): String {
        return path
    }

    internal object FuseFileInfoUtil {
        val OPEN_MODE_MASK = 0x003
        val O_RDONLY = 0x0000
        val O_WRONLY = 0x0001
        val O_RDWR = 0x0002

        val O_CREAT = 0x0100
        val O_TRUNC = 0x01000
        val O_APPEND = 0x02000

        fun getOpenMode(fi: FuseFileInfo): Mode {
            val flags = fi.flags.intValue()
            val openMode = flags and OPEN_MODE_MASK
            var mode = when (openMode) {
                O_RDONLY -> RandomDataAccess.READ
                O_WRONLY -> RandomDataAccess.WRITE
                O_RDWR -> RandomDataAccess.READ join RandomDataAccess.WRITE
                else -> throw Exception("Unsupported open mode: $openMode")
            }
            if (flags and O_CREAT != 0)
                mode = mode join RandomDataAccess.WRITE
            if (flags and O_TRUNC != 0)
                mode = mode join RandomDataAccess.TRUNCATE
            if (flags and O_APPEND != 0)
                mode = mode join RandomDataAccess.APPEND
            return mode
        }
    }

    private fun exceptionToError(e: Exception): Int {
        return errnoToError(exceptionToErrno(e))
    }

    private fun exceptionToErrno(e: Exception): Errno {
        return if (e is FileNotFoundException) Errno.ENOENT else Errno.EFAULT
    }

    private fun errnoToError(error: Errno): Int {
        return -error.intValue()
    }

    override fun open(path: String, fi: FuseFileInfo): Int {
        return openInternal(path, FuseFileInfoUtil.getOpenMode(fi), fi)
    }

    private fun openInternal(path: String, mode: Mode, fi: FuseFileInfo): Int {
        val p = parsePath(path)
        val randomAccess: RandomDataAccess
        try {
            randomAccess = future(coroutineContext) { storageDir.open(p, mode) }.join()
        } catch (e: Exception) {
            return exceptionToError(e)
        }

        val handle = openFilesManager.putFile(randomAccess)
        fi.fh.set(handle)
        return 0
    }

    override fun fsync(path: String?, isdatasync: Int, fi: FuseFileInfo?): Int = future(coroutineContext) {
        val channel = openFilesManager.getFile(fi!!.fh.get())
                ?: return@future errnoToError(Errno.EINVAL)
        try {
            channel.flush()
        } catch (e: Exception) {
            return@future exceptionToError(e)
        }
        return@future 0
    }.join()

    override fun release(path: String?, fi: FuseFileInfo?): Int = future(coroutineContext) {
        try {
            openFilesManager.release(fi!!.fh.get())
        } catch (e: Exception) {
            return@future exceptionToError(e)
        }

        return@future 0
    }.join()

    private fun toByteBuffer(pointer: Pointer?, size: Long): ByteBuffer {
        if (size >= Integer.MAX_VALUE)
            throw IllegalArgumentException("Invalid buffer size: $size")
        return MemoryIO.getInstance().newDirectByteBuffer(pointer!!.address(), size.toInt())
    }

    override fun read(path: String?, buf: Pointer?, size: Long, offset: Long, fi: FuseFileInfo?): Int
            = future(coroutineContext) {
        val randomAccess = openFilesManager.getFile(fi!!.fh.get())
                ?: return@future errnoToError(Errno.EINVAL)
        try {
            val byteBuffer = toByteBuffer(buf, size)
            val buffer = ByteArray(size.toInt())
            val read = randomAccess.readAt(offset, buffer)
            if (read > 0)
                byteBuffer.put(buffer, 0, read)
            return@future read
        } catch (e: Exception) {
            println("Read $path: $e")
            e.printStackTrace()
            return@future exceptionToError(e)
        }
    }.join()

    override fun write(path: String?, buf: Pointer?, size: Long, offset: Long, fi: FuseFileInfo): Int
            = future(coroutineContext) {
        val randomAccess = openFilesManager.getFile(fi.fh.get()) ?: return@future errnoToError(Errno.EINVAL)
        val byteBuffer = toByteBuffer(buf, size)
        val buffer = ByteArray(size.toInt())
        byteBuffer.get(buffer)
        try {
            return@future randomAccess.writeAt(offset, buffer)
        } catch (e: Exception) {
            println("Write $path: $e")
            e.printStackTrace()
            return@future exceptionToError(e)
        }
    }.join()

    override fun truncate(path: String, size: Long): Int = future(coroutineContext) {
        var dataAccess: RandomDataAccess? = null
        try {
            dataAccess = storageDir.open(path, RandomDataAccess.WRITE)
            dataAccess.truncate(size)
            return@future 0
        } catch (e: Exception) {
            return@future exceptionToError(e)
        } finally {
            if (dataAccess != null) {
                try {
                    dataAccess.close()
                } catch (e: Exception) {
                    return@future exceptionToError(e)
                }

            }
        }
    }.join()

    override fun create(path: String, mode: Long, fi: FuseFileInfo): Int {
        val mode1 = FuseFileInfoUtil.getOpenMode(fi) join RandomDataAccess.WRITE  join RandomDataAccess.TRUNCATE
        return openInternal(path, mode1, fi)
    }

    override fun rmdir(path: String): Int = future(coroutineContext) {
        val p = parsePath(path)
        try {
            // TODO: fix race condition: a file could be created while deleting the directory...
            if (storageDir.listFiles(p).isNotEmpty())
                return@future errnoToError(Errno.ENOTEMPTY)
            if (storageDir.listDirectories(p).isNotEmpty())
                return@future errnoToError(Errno.ENOTEMPTY)
            storageDir.remove(p)
            return@future 0
        } catch (e: Exception) {
            return@future exceptionToError(e)
        }
    }.join()

    override fun unlink(path: String): Int = future(coroutineContext) {
        var p = path
        // delete a file
        p = parsePath(p)
        try {
            storageDir.remove(p)
        } catch (e: Exception) {
            return@future exceptionToError(e)
        }

        return@future 0
    }.join()

    override fun statfs(path: String, stbuf: Statvfs): Int {
        super.statfs(path, stbuf)

        stbuf.f_bsize.set(8 * 1024) /* file system block size */
        stbuf.f_frsize.set(16 * 8 * 1024)/* fragment size */
        stbuf.f_blocks.set(1000_1000) /* size of fs in f_frsize units */
        stbuf.f_bfree.set(1000_1000)  /* # free blocks */
        stbuf.f_bavail.set(1000_1000) /* # free blocks for non-root */
        stbuf.f_files.set(1000_1000)  /* # inodes */
        stbuf.f_ffree.set(1000_1000)  /* # free inodes */
        stbuf.f_favail.set(1000_1000) /* # free inodes for non-root */
        //stbuf.f_fsid.set(1000_1000)   /* file system ID */
        return 0
    }
}
