package org.fejoa.fs.fusejnr

import ru.serce.jnrfuse.FuseFS

import java.nio.file.Path
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

object Utils {
    @Throws(InterruptedException::class)
    fun blockingMount(stub: FuseFS, tmpDir: Path) {
        val latch = CountDownLatch(1)
        Thread(Runnable {
            stub.mount(tmpDir, false, true)
            latch.countDown()
        }).start()
        latch.await(1L, TimeUnit.MINUTES)
    }
}
