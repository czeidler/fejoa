package org.fejoa.fs

import junit.framework.TestCase
import kotlinx.coroutines.experimental.newSingleThreadContext
import kotlinx.coroutines.experimental.runBlocking
import org.fejoa.AccountIO
import org.fejoa.FejoaContext
import org.fejoa.UserData
import org.fejoa.crypto.CryptoSettings
import org.fejoa.crypto.generateSecretKeyData
import org.fejoa.fs.fusejnr.FejoaFuse
import org.fejoa.fs.fusejnr.Utils
import org.fejoa.storage.StorageDir
import org.fejoa.support.StorageLib
import org.fejoa.support.StreamHelper
import org.fejoa.support.toInStream
import org.junit.After
import org.junit.Before
import org.junit.Test

import java.io.*
import java.util.ArrayList


class FejoaFuseTest {
    private val cleanUpDirs: MutableList<String> = ArrayList()
    private val baseDir = "fejoafs"
    private var userData: UserData? = null

    @Before
    fun setUp() = runBlocking {
        cleanUpDirs.add(baseDir)

        val context = FejoaContext(AccountIO.Type.CLIENT, baseDir, "userData",
                newSingleThreadContext("fuse test context"))
        userData = UserData.create(context)
    }

    @After
    fun tearDown() {
        for (dir in cleanUpDirs)
            StorageLib.recursiveDeleteFile(File(dir))
    }

    private suspend fun createStorageDir(userData: UserData, storageContext: String): StorageDir {
        val keyData = CryptoSettings.default.symmetric.generateSecretKeyData()
        val branch = userData.createStorageDir(storageContext, UserData.generateBranchName().toHex(),
                "fuse test branch", keyData)
        val storageDir = userData.getStorageDir(branch)
        userData.commit()
        return storageDir
    }

    @Throws(IOException::class)
    private fun assertContent(file: File, content: String) {
        val read = String(StreamHelper.readAll(FileInputStream(file).toInStream()))
        TestCase.assertEquals(content, read)
    }

    @Test
    fun testBasics() = runBlocking {
        val storageDir = createStorageDir(userData!!, "testContext")
        storageDir.writeString("test", "test")
        storageDir.writeString("dir/test2", "test2")
        storageDir.commit("Commit".toByteArray())

        val mountPoint = File(baseDir, "fejoaMountPoint")
        mountPoint.mkdirs()
        val mountDir = mountPoint.absoluteFile.toPath()

        val fileSystenJNR = FejoaFuse(storageDir, userData!!.context.coroutineContext)
        //fileSystenJNR.umount()

        Utils.blockingMount(fileSystenJNR, mountDir)
        println("fejoa fs mounted")

        // open a file from the mounted fs and try to edit it
        val mountedTestFile = File(mountPoint, "dir/test2")
        assertContent(mountedTestFile, "test2")
        var outputStream = FileOutputStream(mountedTestFile, false)
        outputStream.write("edited".toByteArray())
        outputStream.close()
        assertContent(mountedTestFile, "edited")

        // append
        outputStream = FileOutputStream(mountedTestFile, true)
        outputStream.write("2".toByteArray())
        outputStream.close()
        assertContent(mountedTestFile, "edited2")

        // test dir creation
        TestCase.assertTrue(File(mountPoint, "emptyDir").mkdirs())
        TestCase.assertTrue(mountPoint.list().contains("emptyDir"))
        TestCase.assertTrue(File(mountPoint, "emptyDir").list().isEmpty())

        // sleep for further manual testing
        Thread.sleep((1 * 1000).toLong())

        fileSystenJNR.umount()
    }
}

