package org.fejoa.server.ui

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.support.await
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.HTMLSpanElement
import kotlin.browser.document


class PendingRegistrationViewModel(val model: MainModel) {
    val confirmUser = document.getElementById(confirmRegistrationUserId).unsafeCast<HTMLSpanElement>()
    val requestEmailInput = document.getElementById(requestEmailInputId).unsafeCast<HTMLInputElement>()
    val requestTokenInput = document.getElementById(requestTokenInputId).unsafeCast<HTMLInputElement>()
    val requestTokenButton = document.getElementById(requestTokenButtonId).unsafeCast<HTMLButtonElement>()
    val finishRegistrationButton = document.getElementById(finishRegistrationButtonId).unsafeCast<HTMLButtonElement>()

    init {
        updateView()

        model.listeners += {
            updateView()
        }

        requestTokenButton.onclick = { sendTokenRequest() }
        finishRegistrationButton.onclick = { confirmRegistration()}
    }

    private fun getUser(): String? {
        return model.authStatus?.response?.pendingRegistrations?.firstOrNull()?.user
    }

    private fun updateView() {
        val user = getUser() ?: return
        confirmUser.innerHTML = user
    }

    private fun sendTokenRequest() {
        model.logModel.clear()

        val user = getUser() ?: return
        val email = requestEmailInput.value

        requestTokenButton.disabled = true
        requestEmailInput.disabled = true
        GlobalScope.launch {
            try {
                model.sendTokenRequest(user, email).await()
                model.logModel.info = "Token sent to $email"
            } catch (e: Throwable) {
                model.logModel.error = e.message ?: "Unknown exception"
            } finally {
                requestTokenButton.disabled = false
                requestEmailInput.disabled = false
                requestEmailInput.value = ""
            }
        }
    }

    private fun confirmRegistration() {
        model.logModel.clear()

        val user = getUser() ?: return
        val token = requestTokenInput.value

        GlobalScope.launch {
            try {
                model.confirmRegistration(user, token).await()
            } catch (e: Throwable) {
                model.logModel.error = e.message ?: "Unknown exception"
            }
        }
    }
}