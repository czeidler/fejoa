package org.fejoa.server.ui

import kotlinx.html.*
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLParagraphElement
import kotlin.browser.document


const val errorDivId = "error-container"
const val errorMessageId = "error-message"
const val infoDivId = "info-container"
const val infoMessageId = "info-message"

fun buildErrorDiv(parent: DIV) = parent.apply {
    div("alert alert-danger") {
        attributes["role"] = "alert"

        id = errorDivId
        hidden = true

        p {
            id = errorMessageId
            +"error"
        }
    }

    div("alert alert-info") {
        attributes["role"] = "alert"

        id = infoDivId
        hidden = true

        p {
            id = infoMessageId
            +"info"
        }
    }
}

class LogViewModel(val model: LogModel) {
    val errorContainer = document.getElementById(errorDivId).unsafeCast<HTMLDivElement>()
    val errorMessageP = document.getElementById(errorMessageId).unsafeCast<HTMLParagraphElement>()

    val infoContainer = document.getElementById(infoDivId).unsafeCast<HTMLDivElement>()
    val infoMessageP = document.getElementById(infoMessageId).unsafeCast<HTMLParagraphElement>()

    init {
        model.listeners += {
            update()
        }
    }

    private fun update() {
        val error = model.error
        if (error.isBlank()) {
            errorContainer.hidden = true
        } else {
            errorContainer.hidden = false
            errorMessageP.innerText = error
        }

        val info = model.info
        if (info.isBlank()) {
            infoContainer.hidden = true
        } else {
            infoContainer.hidden = false
            infoMessageP.innerText = info
        }
    }
}
