package org.fejoa.server.ui

import kotlinx.html.*


const val sendEmailDivId = "send-email-container"
const val requestTokenButtonId = "request-token-button"
const val requestTokenInputId = "request-token-input"
const val requestEmailInputId = "request-email-input"
const val confirmRegistrationUserId = "confirm-registration-user"
const val finishRegistrationButtonId = "finish-registration-button"

fun buildSendEmailDiv(parent: DIV) = parent.apply {
    div("container-fluid") {
        id = sendEmailDivId
        hidden = true

        h4 {
            +"Confirm registration for user: "
            span {
                id = confirmRegistrationUserId
            }
        }

        div {
            h5("row") {
                +"1) Request registration token"
            }
            div("container") {
                p("row") {
                    +"After requesting the token you will receive an email with the token."
                }

                div("form-group row") {
                    label {
                        attributes += "for" to requestEmailInputId
                        +"Email"
                    }
                    input(classes = "form-control") {
                        id = requestEmailInputId
                        type = InputType.email
                        placeholder = "email address"
                        value = ""
                    }
                    button(classes = "btn float-left mt-1") {
                        id = requestTokenButtonId
                        +"Request"
                    }
                }
            }
        }


        div {
            h5("row") {
                +"2) Confirm Registration:"
            }
            div("container") {
                p("row") {
                    +"Enter the token from the email."
                }
                div("form-group row") {
                    label {
                        attributes += "for" to requestTokenInputId
                        +"Token"
                    }
                    input(classes = "form-control") {
                        id = requestTokenInputId
                        type = InputType.text
                        placeholder = "enter token from email"
                    }

                    button(classes = "btn float-left mt-1") {
                        id = finishRegistrationButtonId
                        +"Submit"
                    }
                }
            }
        }
    }
}

const val loggedOutDivId = "logged-out-container"
fun buildLoggedOutDiv(parent: DIV) = parent.apply {
    div {
        id = loggedOutDivId
        hidden = true
        h3 {
            +"Not authenticated"
        }
        p {
            +"Use the FejoaAuth browser addon to login or to register."
        }
    }
}
