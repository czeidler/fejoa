package org.fejoa.server.ui


external class FejoaAuth {
    fun addAuthStatusListener(origin: String, listener: (data: dynamic) -> Unit)

    fun removeAuthStatusListener(origin: String)
}