package org.fejoa.server.ui

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import org.fejoa.StorageConfig
import org.w3c.dom.*
import kotlin.browser.document


class LoggedInViewModel(private val model: RemoteStorageModel, private val logModel: LogModel) {
    private val userNameSpan = document.getElementById(LoggedInView.userNameId).unsafeCast<HTMLSpanElement>()
    private val storageList = document.getElementById(LoggedInView.storageListId).unsafeCast<HTMLUListElement>()
    private val storageListEmpty = document.getElementById(LoggedInView.storageListEmptyId)
            .unsafeCast<HTMLParagraphElement>()
    private val fetchingStatus = document.getElementById(LoggedInView.fetchingId).unsafeCast<HTMLSpanElement>()

    private val storageConfigContainer = document.getElementById(LoggedInView.storageConfigContainerId)
            .unsafeCast<HTMLDivElement>()
    private val noStorageConfig = document.getElementById(LoggedInView.noStorageConfigId)
            .unsafeCast<HTMLParagraphElement>()
    private val storageConfigDisplay = document.getElementById(LoggedInView.storageConfigDisplayId)
            .unsafeCast<HTMLDivElement>()
    private val deleteStorageConfigButton = document.getElementById(LoggedInView.deleteStorageConfigButtonId)
            .unsafeCast<HTMLButtonElement>()

    init {
        model.listeners += object : RemoteStorageModel.Listener {
            override fun onAllBranchesUpdated() {
                onUpdate()
            }

            override fun onServerConfigUpdate() {
                onUpdate()
            }
        }
        deleteStorageConfigButton.onclick = {
            model.deleteServerConfig()
        }

        onUpdate()
    }

    private fun clearStorageList() {
        while (storageList.firstChild != null)
            storageList.removeChild(storageList.firstChild!!)
    }

    fun onUpdate() {
        userNameSpan.innerText = model.user ?: ""

        storageListEmpty.hidden = !model.branches.isEmpty()
        storageList.hidden = model.isFetching
        fetchingStatus.hidden = !model.isFetching

        if (model.isFetching)
            return

        if (model.errors.lastOrNull() != null)
            logModel.error = model.errors.removeAt(model.errors.lastIndex)

        model.storageConfig.first?.let {
            storageConfigContainer.hidden = false
            noStorageConfig.hidden = true
            storageConfigDisplay.innerText = Json(indented = true).stringify(StorageConfig.serializer(), it)
        } ?: run {
            storageConfigContainer.hidden = true
            noStorageConfig.hidden = false
        }
        deleteStorageConfigButton.disabled = model.storageConfig.second == RemoteStorageModel.Action.DELETING

        clearStorageList()
        for (branch in model.branches) {
            val button = LoggedInView.appendStorageEntry(storageList, branch)
            button.onclick = {
                GlobalScope.launch {
                    model.deleteBranch(branch.branch)
                }
            }
        }
    }
}
