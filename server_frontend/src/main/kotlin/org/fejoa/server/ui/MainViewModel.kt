package org.fejoa.server.ui

import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLParagraphElement
import kotlin.browser.document


class ErrorViewModel(val model: MainModel) {
    val errorLabel = document.getElementById(errorMessageId).unsafeCast<HTMLParagraphElement>()

    fun onUpdate() {
        val message = model.authStatus?.message ?: "Unknown error"
        errorLabel.innerText = message
    }
}

class MainViewModel(val model: MainModel) {
    val loadingContainer = document.getElementById(loadingDivId).unsafeCast<HTMLDivElement>()
    val sendEmailContainer = document.getElementById(sendEmailDivId).unsafeCast<HTMLDivElement>()
    val loggedOutContainer = document.getElementById(loggedOutDivId).unsafeCast<HTMLDivElement>()
    val loggedInContainer = document.getElementById(LoggedInView.loggedInDivId).unsafeCast<HTMLDivElement>()

    val errorViewModel = ErrorViewModel(model)
    val loggedInViewModel = LoggedInViewModel(model.remoteBranchModel, model.logModel)

    val container = listOf(sendEmailContainer, loggedOutContainer, loggedInContainer)

    init {
        model.listeners += {
            onUpdate()
        }
    }

    private fun select(div: HTMLDivElement) {
        container.forEach {
            it.hidden = it != div
        }
    }

    private fun onUpdate() {
        loadingContainer.hidden = true
        return when (model.state){
            MainModel.State.LOADING -> loadingContainer.hidden = false
            MainModel.State.PENDING_REGISTRATION -> select(sendEmailContainer)
            MainModel.State.LOGGED_OUT -> select(loggedOutContainer)
            MainModel.State.LOGGED_IN -> {
                select(loggedInContainer)
                loggedInViewModel.onUpdate()
            }
            MainModel.State.LOAD_ERROR -> {
                model.logModel.error = model.authStatus?.message ?: "Internal error"
                errorViewModel.onUpdate()
            }
        }
    }
}