package org.fejoa.server.ui

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.ConnectionAuthManager
import org.fejoa.PlainAuthInfo
import org.fejoa.Remote
import org.fejoa.crypto.BaseKeyCache
import org.fejoa.network.*
import org.fejoa.support.Future
import org.fejoa.support.await
import org.w3c.dom.url.URL


interface JobSender {
    fun <T: RemoteJob.Result>sendJob(job: RemoteJob<T>): Future<T>
}

class MainModel(val url: URL, val fejoaPortalPath: String): JobSender {
    enum class State {
        LOADING,
        PENDING_REGISTRATION,
        LOGGED_OUT,
        LOGGED_IN,
        LOAD_ERROR
    }

    val logModel = LogModel()
    val authManager = ConnectionAuthManager(BaseKeyCache())
    val remoteBranchModel = RemoteStorageModel(this)

    var state: State = State.LOADING
        private set
    var authStatus: AuthStatusJob.Result? = null
        private set

    val listeners: MutableList<() -> Unit> = ArrayList()

    init {
        val fejoaAuth = FejoaAuth()
        fejoaAuth.addAuthStatusListener(url.origin) {
            load()
        }

        setState(State.LOADING, null)
        load()
    }

    override fun <T: RemoteJob.Result>sendJob(job: RemoteJob<T>): Future<T> {
        logModel.clear()
        val path = "${url.origin}/$fejoaPortalPath"
        return authManager.send(job, Remote("no_id", "no_user", path), PlainAuthInfo())
    }

    fun sendTokenRequest(user: String, email: String): Future<RemoteJob.Result>  {
        return sendJob(RequestRegistrationTokenJob(user, email))
    }

    fun confirmRegistration(user: String, token: String): Future<RegisterJob.Result> {
        val future = sendJob(ConfirmRegistrationJob(user, token))
        future.whenCompleted { result, _ ->
            if (result != null)
                load()
        }
        return future
    }

    private fun load() = GlobalScope.launch {
        logModel.clear()
        setState(State.LOADING, null)

        try {
            val result = sendJob(AuthStatusJob()).await()
            if (result.code != ReturnType.OK) {
                setState(State.LOAD_ERROR, result)
                return@launch
            }
            val response = result.response ?: run {
                setState(State.LOAD_ERROR, result)
                return@launch
            }

            when {
                response.pendingRegistrations.isNotEmpty() -> setState(State.PENDING_REGISTRATION, result)
                response.accounts.isNotEmpty() -> {
                    setState(State.LOGGED_IN, result)
                    result.response?.accounts?.firstOrNull()?.let {
                        remoteBranchModel.user = it
                    }
                }
                else -> setState(State.LOGGED_OUT, result)
            }
        } catch (e: Throwable) {
            setState(State.LOAD_ERROR, AuthStatusJob.Result(ReturnType.EXCEPTION, e.message
                    ?: "Unknown exception"))
        }
    }

    private fun setState(state: State, authStatus: AuthStatusJob.Result? = null) {
        this.state = state
        this.authStatus = authStatus
        listeners.forEach { it.invoke() }
    }

}