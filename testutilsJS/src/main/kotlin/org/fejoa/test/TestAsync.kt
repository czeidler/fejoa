package org.fejoa.test

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.asPromise
import kotlinx.coroutines.async


actual fun <T> testAsync(body: suspend CoroutineScope.() -> T): dynamic {
    return GlobalScope.async(block = body).asPromise()
}

/**
 * We can't shortcut and call other testAsync method. E.g. testAsync(null, cleanup, body)
 * I don't know why but the test are not working otherwise...
 */
actual fun <T> testAsync(cleanUp: suspend () -> Unit, body: suspend () -> T): dynamic {
    return GlobalScope.async {
        try {
            body.invoke()
        } finally {
            cleanUp.invoke()
        }
    }.asPromise()
}

actual fun <T> testAsync(setUp: (suspend () -> Unit)?, cleanUp: (suspend () -> Unit)?, body: suspend () -> T)
        : dynamic {
    return GlobalScope.async {
        try {
            setUp?.invoke()
            body.invoke()
        } finally {
            cleanUp?.invoke()
        }
    }.asPromise()
}