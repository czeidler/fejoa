package org.fejoa.storage

import org.fejoa.crypto.KeyPairData
import org.fejoa.crypto.SecretKeyIvData
import org.fejoa.support.PathUtils


class SignCredentialsDBValue(parent: DBObject, relativePath: String) : DBValue<KeyPairData>(parent, relativePath) {
    override suspend fun write(obj: KeyPairData) {
        dir.writeString(path, obj.toJson())
    }

    override suspend fun get(): KeyPairData {
        return signCredentialsFromJson(dir.readString(path))
    }
}

class SignCredentialList(dir: IOStorageDir, path: String) : DBMap<HashValue, SignCredentialsDBValue>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    override fun get(key: HashValue): SignCredentialsDBValue {
        return SignCredentialsDBValue(this, key.toHex())
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }
}

class SymCredentialsDBValue(parent: DBObject, relativePath: String) : DBValue<SecretKeyIvData>(parent, relativePath) {
    override suspend fun write(obj: SecretKeyIvData) {
        dir.writeString(path, obj.toJson())
    }

    override suspend fun get(): SecretKeyIvData {
        return symCredentialsFromJson(dir.readString(path))
    }
}


class SymCredentialList(dir: IOStorageDir, path: String) : DBMap<HashValue, SymCredentialsDBValue>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    override fun get(key: HashValue): SymCredentialsDBValue {
        return SymCredentialsDBValue(this, key.toHex())
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }
}
