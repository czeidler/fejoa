package org.fejoa.storage

import org.fejoa.support.PathUtils


class DBString(dir: IOStorageDir, path: String) : DBValue<String>(dir, path) {
    constructor(parent: DBObject, relativePath: String)
            : this(parent.dir, PathUtils.appendDir(parent.path, relativePath))

    override suspend fun write(obj: String) {
        dir.writeString(path, obj)
    }

    override suspend fun get(): String {
        return dir.readString(path)
    }
}
