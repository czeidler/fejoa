package org.fejoa

import org.fejoa.repository.sync.AccessRight
import org.fejoa.support.fromHex


/**
 * Keeps track about auth status at a remote.
 */
open class AccessTracker(val noAccessControl: Boolean = false) {
    // authenticated accounts; list of usernames
    private val authAccounts: MutableSet<String> = HashSet()

    fun getAuthAccounts(): Set<String> {
        return authAccounts
    }

    fun addAccountAccess(user: String) {
        authAccounts.add(user)
    }

    fun removeAccountAccess(user: String) {
        authAccounts.remove(user)
    }

    fun removeAllAccountAccess() {
        authAccounts.clear()
    }

    fun hasAccountAccess(user: String): Boolean {
        return authAccounts.contains(user)
    }
}

class ClientAccessTracker(noAccessControl: Boolean = false) : AccessTracker(noAccessControl) {
    // user -> TokenAccessTracker
    private val userTokenAccess: MutableMap<String, TokenTracker> = HashMap()
    // token -> AccessRight
    class TokenTracker(val tokens: MutableSet<String> = HashSet())

    fun addTokenAccess(user: String, tokenId: String) {
        val branchTracker = userTokenAccess[user] ?: TokenTracker().also { userTokenAccess[user] = it}
        branchTracker.tokens.add(tokenId)
    }

    fun hasTokenAccess(user: String, tokenId: String): Boolean {
        return if (hasAccountAccess(user) || noAccessControl)
            true
        else {
            val branchTracker = userTokenAccess[user] ?: return false
            branchTracker.tokens.contains(tokenId)
        }
    }

    fun removeTokenAccess(user: String, tokenId: String) {
        userTokenAccess[user]?.let { it.tokens.remove(tokenId) }
    }
}

class ServerAccessTracker(noAccessControl: Boolean = false) : AccessTracker(noAccessControl) {
    // user -> BranchAccessTracker
    private val userBranchAccess: MutableMap<String, BranchAccessTracker> = HashMap()
    // branch -> AccessRight
    class BranchAccessTracker(val map: MutableMap<String, AccessRight> = HashMap())

    fun addBranchAccess(user: String, branch: String, accessRight: AccessRight) {
        val branchAccessTracker = userBranchAccess[user] ?:BranchAccessTracker().also {
            userBranchAccess[user] = it
        }
        branchAccessTracker.map[branch] = accessRight
    }

    fun getBranchAccessRight(user: String, branch: String): AccessRight {
        try {
            // check if the branch is valid
            fromHex(branch)
        } catch (e: Exception) {
            return AccessRight.NONE
        }
        if (hasAccountAccess(user) || noAccessControl)
            return AccessRight.ALL

        return userBranchAccess[user]?.map?.get(branch) ?: AccessRight.NONE
    }
}
