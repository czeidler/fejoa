package org.fejoa

import org.fejoa.storage.DBHashValue
import org.fejoa.storage.DBMap
import org.fejoa.storage.HashValue
import org.fejoa.storage.StorageDir
import org.fejoa.support.PathUtils


class BranchIndex(val storageDir: StorageDir) {
    class BranchMap(storageDir: StorageDir, path: String) : DBMap<String, DBHashValue>(storageDir, path) {
        override suspend fun list(): Collection<String> {
            return dir.listFiles(path)
        }

        override fun get(key: String): DBHashValue {
            return DBHashValue(this, key)
        }

        override suspend fun remove(key: String) {
            dir.remove(PathUtils.appendDir(path, key))
        }
    }

    private val branchMap = BranchMap(storageDir, "branches")

    suspend fun list(): Map<String, HashValue> {
        return branchMap.list().associate { it to branchMap.get(it).get() }
    }

    suspend fun update(branch: String, logTip: HashValue) {
        val entry = branchMap.get(branch)
        entry.write(logTip)
    }

    suspend fun getLogTip(branch: String): HashValue? {
        val entry = branchMap.get(branch)
        if (!entry.exists())
            return null
        return entry.get()
    }

    suspend fun commit() {
        storageDir.commit()
    }
}