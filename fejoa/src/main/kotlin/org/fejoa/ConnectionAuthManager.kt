package org.fejoa

import org.fejoa.crypto.BaseKeyCache
import org.fejoa.network.*
import org.fejoa.support.Future
import org.fejoa.support.async
import org.fejoa.support.await


class ConnectionAuthManager(val baseKeyCache: BaseKeyCache, pwGetter: PasswordGetter? = null) {
    constructor(context: FejoaContext) : this(context.baseKeyCache)

    private val remoteAccessTrackerMap: MutableMap<String, ClientAccessTracker> = HashMap()
    var passwordGetter = pwGetter ?: CanceledPasswordGetter()

    private fun getAccessTracker(remote: Remote): ClientAccessTracker {
        val key = remote.server
        return remoteAccessTrackerMap[key] ?: ClientAccessTracker().also { remoteAccessTrackerMap[key] = it }
    }

    private fun getRequest(remote: Remote): RemoteRequest {
        return platformCreateHTTPRequest(remote.server)
    }

    // Only send one auth request at time
    private val onGoingAuth: MutableMap<String, Future<RemoteJob.Result>> = HashMap()
    private fun onGoingAuthId(remote: Remote, authInfo: AuthInfo): String {
        return "${remote.address()}${authInfo.type.name}"
    }

    private suspend fun ensureAccess(remote: Remote, authInfo: AuthInfo, pwGetter: PasswordGetter) {
        // wait for the ongoing auth request if there is one
        val requestId = onGoingAuthId(remote, authInfo)
        onGoingAuth[requestId]?.let {
            it.await()
            return
        }

        val accessTracker = getAccessTracker(remote)
        return when (authInfo.type) {
            AuthType.PLAIN -> {}
            AuthType.LOGIN -> {
                if (accessTracker.hasAccountAccess(remote.user))
                    return

                val future = async {
                    // Get the password from within the future otherwise there is a race condition
                    val password = pwGetter.get(PasswordGetter.Purpose.SERVER_LOGIN, remote.address(),
                            "ConnectionAuthManager") ?: throw Exception("Login canceled by user")
                    LoginJob(remote.user, password, baseKeyCache).run(getRequest(remote))
                }

                onGoingAuth[requestId] = future
                val result = try {
                    future.await()
                } finally {
                    onGoingAuth.remove(requestId)
                }

                if (result.code != ReturnType.OK)
                    throw Exception("Login failed: ${result.message}")
                accessTracker.addAccountAccess(remote.user)
                return
            }
            AuthType.TOKEN -> {
                val authToken = authInfo as AuthToken
                if (accessTracker.hasTokenAccess(remote.user, authToken.id))
                    return
                val future = async {
                    TokenAuthJob(remote.user, authToken).run(getRequest(remote))
                }
                onGoingAuth[requestId] = future
                val result = try {
                    future.await()
                } finally {
                    onGoingAuth.remove(requestId)
                }

                if (result.code != ReturnType.OK)
                    throw Exception("Login failed: ${result.message}")

                accessTracker.addTokenAccess(remote.user, authToken.id)
                return
            }
        }
    }

    suspend fun login(remote: Remote, password: String) {
        ensureAccess(remote, LoginAuthInfo(), SinglePasswordGetter(password))
    }

    fun logout(remote: Remote, users: List<String>): Future<LogoutJob.Result> {
        val accessTracker = getAccessTracker(remote)
        if (users.isEmpty())
            accessTracker.removeAllAccountAccess()
        else
            users.forEach { accessTracker.removeAccountAccess(it) }
        return send(LogoutJob(), remote, PlainAuthInfo())
    }

    fun <T: RemoteJob.Result>send(job: RemoteJob<T>, remote: Remote, authInfo: AuthInfo,
                                          pwGetter: PasswordGetter? = null): Future<T> {
        val request = getRequest(remote)
        val future = async {
            ensureAccess(remote, authInfo, pwGetter ?: passwordGetter)
            val result = job.run(request)
            if (result.code == ReturnType.ACCESS_DENIED) {
                // remove access from tacker
                val accessTracker = getAccessTracker(remote)
                val dummy = when (authInfo.type) {
                    AuthType.PLAIN -> throw Exception("Unexpected")
                    AuthType.LOGIN -> {
                        accessTracker.removeAccountAccess(remote.user)
                    }
                    AuthType.TOKEN -> {
                        accessTracker.removeTokenAccess(remote.user, (authInfo as AuthToken).id)
                    }
                }
            }
            result
        }
        future.whenCompleted { _, error ->
            if (error is Future.CancelationException) {
                request.cancel()
                job.cancel()
            }
        }
        return future
    }
}
