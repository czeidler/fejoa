package org.fejoa.command

import kotlinx.serialization.json.Json
import org.fejoa.BranchIndexCache
import org.fejoa.UserData
import org.fejoa.storage.HashValue


class MigrationHandler(val userData: UserData, val branchIndexCache: BranchIndexCache)
    : InQueueManager.Handler<InQueueManager.EnvelopeHandle> {
    override val command: String
        get() = MigrationCommand.NAME

    override suspend fun onNewEntry(entry: InQueueManager.EnvelopeHandle) {
        val command = Json.parse(MigrationCommand.serializer(), entry.commandJson)
        val newRemote = command.remote

        val contact = userData.contacts.list.get(command.user)
        val oldRemote = contact.remotes.listRemotes().firstOrNull { it.id == newRemote.id }
            ?: TODO("Remote doesn't exist, discard command?")
        // update remote
        contact.remotes.get(HashValue.fromHex(oldRemote.id)).write(newRemote)
        userData.commit()

        // update out commands
        userData.getOutQueues().forEach {
            val outQueue = OutQueue(userData.getStorageDir(it))
            outQueue.updateRemote(newRemote)
        }

        // invalided remote branch index cache; the new remote might have a new branch index
        branchIndexCache.invalidateRemote(oldRemote.id)

        entry.setHandled()
    }
}
