package org.fejoa.command

import kotlinx.serialization.Serializable
import org.fejoa.JsonPublicKeyData
import org.fejoa.Remote
import org.fejoa.storage.HashValue
import org.fejoa.storage.HashValueDataSerializer


@Serializable
class ContactRequestCommand(@Serializable(with = HashValueDataSerializer::class) val user: HashValue,
                            val userRemote: Remote,
                            val asymEncKey: JsonPublicKeyData,
                            val verificationKey: JsonPublicKeyData,
                            override val command: String = NAME) : Command {
    companion object {
        const val NAME = "contactRequest"
    }
}
