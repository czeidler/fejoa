package org.fejoa.command

import kotlinx.serialization.Serializable
import org.fejoa.Remote
import org.fejoa.storage.HashValue
import org.fejoa.storage.HashValueDataSerializer


/**
 * Indicates that a user moved to a new remote
 *
 * @param user the user id of the migrating contact
 * @param remote the updated remote of the contact; the remote id should match with the previous remote
 */
@Serializable
class MigrationCommand(@Serializable(with = HashValueDataSerializer::class) val user: HashValue,
                       val remote: Remote,
                       override val command: String = NAME) : Command {
    companion object {
        const val NAME = "migration"
    }
}
