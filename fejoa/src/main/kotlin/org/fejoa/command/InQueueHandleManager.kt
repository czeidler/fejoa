package org.fejoa.command


class InQueueHandleManager<T> {
    abstract class PendingRequests<T>(val manager: InQueueHandleManager<T>, val command: T,
                                      val handle: InQueueManager.EnvelopeHandle) {
        suspend fun accept() {
            onAccept()

            handle.setHandled()
            manager.pendingRequests.remove(this)
        }

        abstract suspend fun onAccept()
    }

    // contact request that needs to be approved
    private val pendingRequests: MutableList<PendingRequests<T>> = ArrayList()
    private val listeners: MutableList<(request: PendingRequests<T>) -> Unit> = ArrayList()

    fun getPendingRequests(): List<PendingRequests<T>> {
        return pendingRequests
    }

    fun addListener(listener: (request: PendingRequests<T>) -> Unit) {
        listeners.add(listener)
        pendingRequests.forEach { listener.invoke(it) }
    }

    fun addPending(pending: PendingRequests<T>) {
        pendingRequests.add(pending)
        listeners.forEach { it.invoke(pending) }
    }
}