package org.fejoa.command

import kotlinx.serialization.json.Json
import org.fejoa.UserData


class BranchUpdateHandler(val userData: UserData) : InQueueManager.Handler<InQueueManager.EnvelopeHandle> {
    override val command: String
        get() = BranchUpdateCommand.NAME

    val handles = InQueueHandleManager<BranchUpdateCommand>()

    inner class BranchUpdateRequest(handleManager: InQueueHandleManager<BranchUpdateCommand>,
                                    command: BranchUpdateCommand, handle: InQueueManager.EnvelopeHandle)
        : InQueueHandleManager.PendingRequests<BranchUpdateCommand>(handleManager, command, handle) {
        override suspend fun onAccept() {
            // do nothing
        }
    }

    override suspend fun onNewEntry(entry: InQueueManager.EnvelopeHandle) {
        val command = Json.parse(BranchUpdateCommand.serializer(), entry.commandJson)

        handles.addPending(BranchUpdateRequest(handles, command, entry))
    }
}
