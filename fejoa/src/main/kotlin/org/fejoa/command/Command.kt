package org.fejoa.command

import kotlinx.serialization.Optional
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.fejoa.JsonPublicKeyData
import org.fejoa.crypto.*
import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.storage.HashValue


class CommandEnveloper(val signer: HashValue, val signing: KeyPairData,
                       val encCredentials: org.fejoa.crypto.SecretKeyData?,
                       val receiverKey: PublicKeyData?) {
    class SymKeyData(val keyData: ByteArray) {
        companion object {
            const val KEY_DATA_TAG = 0

            fun read(buffer: ProtocolBufferLight): SymKeyData {
                val keyData = buffer.getBytes(KEY_DATA_TAG) ?: throw Exception("Missing key data")
                return SymKeyData(keyData)
            }
        }

        fun toProtoBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            buffer.put(KEY_DATA_TAG, keyData)
            return buffer
        }
    }

    companion object {
        suspend fun create(settings: CryptoSettings.Symmetric, signer: HashValue, signing: KeyPairData,
                           receiverKey: PublicKeyData?): CommandEnveloper {
            // if there is no receive, don't encrypt
            val key = if (receiverKey != null)
                SecretKeyData(CryptoHelper.crypto.generateSymmetricKey(settings), settings.algo)
            else
                null
            return CommandEnveloper(signer, signing, key, receiverKey)
        }

        suspend fun unwrapKey(asymEncEnvelope: AsymEncEnvelope, keyGetter: suspend (keyId: HashValue) -> PrivateKey?)
                : SymKeyData {
            val privateKey = keyGetter.invoke(HashValue(asymEncEnvelope.keyId))
                    ?: throw Exception("Unknown asymmetric key")

            return SymKeyData.read(ProtocolBufferLight(asymEncEnvelope.decrypt(privateKey)))
        }

        suspend fun unwrap(symEncEnvelope: SymEncEnvelope, symKeyData: SymKeyData): ByteArray {
            val key = CryptoHelper.crypto.secretKeyFromRaw(symKeyData.keyData, symEncEnvelope.algo)
            return symEncEnvelope.decrypt(key)
        }
    }

    suspend fun envelope(data: ByteArray): Envelope {
        val plain = PlainEnvelope.pack(data, signer.bytes, signing)
        if (encCredentials == null)
            return plain
        return SymEncEnvelope.pack(plain.toProtoBuffer().toByteArray(), ByteArray(0), encCredentials.key,
                encCredentials.algo)
    }

    suspend fun wrapReceiverKey(): AsymEncEnvelope? {
        return receiverKey?.let {
            wrapKey(it.publicKey, it.algo)
        }
    }

    suspend fun wrapKey(publicKey: PublicKey, algo: CryptoSettings.ASYM_ALGO): AsymEncEnvelope? {
        return encCredentials?.let {
            val keyData = SymKeyData(CryptoHelper.crypto.encode(it.key))
            AsymEncEnvelope.pack(keyData.toProtoBuffer().toByteArray(), publicKey, algo)
        }
    }
}


interface Command {
    val command: String
}

@Serializable
class CommandNameParser(override val command: String, @Optional val verificationKey: JsonPublicKeyData? = null) : Command {
    companion object {
        fun parse(json: String): CommandNameParser {
            return Json(strictMode = false).parse(CommandNameParser.serializer(), json)
        }
    }
}
