package org.fejoa

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.fejoa.crypto.CryptoHelper
import org.fejoa.crypto.CryptoSettings
import org.fejoa.crypto.KeyPairData
import org.fejoa.repository.sync.AccessRight
import org.fejoa.storage.HashValue
import org.fejoa.support.decodeBase64
import org.fejoa.support.encodeBase64
import org.fejoa.support.toHex
import org.fejoa.support.toUTF



/**
 * @param salt value to make AccessTokens indistinguishable, i.e. two tokens with the same access rights can have
 * different hashes. This prevent the server from reasoning about unknown access tokens
 */
@Serializable
class AccessToken(val salt: String, val branches: MutableMap<String, AccessRight>) {
    companion object {
        fun create(branches: MutableMap<String, AccessRight>): AccessToken {
            val salt = CryptoHelper.crypto.generateSalt16()
            return AccessToken(salt.encodeBase64(), branches)
        }
    }

    fun toJson(): String {
        return Json.stringify(AccessToken.serializer(), this)
    }

    suspend fun hash(hashAlgo: CryptoSettings.HASH_TYPE): ByteArray {
        val token = toJson()

        val stream = CryptoHelper.getHashStream(hashAlgo)
        stream.write(token.toUTF())
        return stream.hash()
    }
}

/**
 * In general a token contains access information.
 *
 * To grant access to a contact, the data owner send a AuthToken to the contact and deploys the ServerToken at the
 * server.
 *
 * The authentication mechanism fulfills two goals:
 * 1) assert that the token is valid
 * 2) assert that the contact is authorised to use the token
 *
 * The server ensures the first goal by validating that the token matches the token id (the id is the token hash).
 * The second goal is achieved by letting the contact signing a random auth token which is verified with the
 * authVerificationKey.
 *
 * To ensure a unique token id for two token that contain the same access information, the token should include some
 * random bytes. This prevent the server comparing token id with known tokens. For example, if access is reversed to a
 * previous state but we don't want to let the server know about it yet.
 */

@Serializable
class AuthToken(val id: String, val token: String, val authSigningKey: JsonPrivateKeyData,
                override val type: AuthType = AuthType.TOKEN): AuthInfo {
    suspend fun sign(authToken: ByteArray): String {
        val keyData = authSigningKey.asPrivateKeyData()
        return CryptoHelper.crypto.sign(authToken, keyData.privateKey, keyData.algo).encodeBase64()
    }
}

/**
 * @param id is the hex hash of the token plain data
 * @param hashAlgo the algo used to calculate the id
 */
@Serializable
class ServerToken(val id: String, val hashAlgo: CryptoSettings.HASH_TYPE, val authVerificationKey: JsonPublicKeyData) {
    suspend fun validate(token: AccessToken): Boolean {
        return HashValue(token.hash(hashAlgo)) == HashValue.fromHex(id)
    }
    suspend fun authenticate(authToken: ByteArray, signature: String): Boolean {
        val publicKeyData = authVerificationKey.asPublicKeyData()
        return CryptoHelper.crypto.verifySignature(authToken, signature.decodeBase64(), publicKeyData.publicKey,
                publicKeyData.algo)
    }
}

class TokenBuilder private constructor(val tokenId: String, val hashAlgo: CryptoSettings.HASH_TYPE,
                                       val token: AccessToken, val keyPairData: KeyPairData) {
    companion object {
        suspend fun create(token: AccessToken): TokenBuilder {
            val settings = CryptoSettings.default.signature
            val keyPair = CryptoHelper.crypto.generateKeyPair(settings)

            val hashAlgo = CryptoSettings.HASH_TYPE.SHA256
            val tokenId = token.hash(hashAlgo).toHex()

            return TokenBuilder(tokenId, hashAlgo, token, KeyPairData(keyPair, settings.algo))
        }
    }

    suspend fun getServerToken(): ServerToken {
        return ServerToken(tokenId, hashAlgo, keyPairData.asJsonPublicKeyData())
    }

    suspend fun getContactToken(): AuthToken {
        return AuthToken(tokenId, token.toJson(), keyPairData.asJsonPrivateKeyData())
    }
}