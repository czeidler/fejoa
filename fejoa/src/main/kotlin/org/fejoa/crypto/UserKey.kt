package org.fejoa.crypto

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.fejoa.storage.secretKeyFromJson
import org.fejoa.storage.toJson
import org.fejoa.support.*


@Serializable
class EncData(val encData: String, val iv: String, val algo: CryptoSettings.SYM_ALGO) {
    companion object {
        fun create(encryptedData: ByteArray, iv: ByteArray, algo: CryptoSettings.SYM_ALGO): EncData {
            return EncData(encryptedData.encodeBase64(), iv.encodeBase64(), algo)
        }
    }

    fun getIv(): ByteArray {
        return iv.decodeBase64()
    }

    fun getEncData(): ByteArray {
        return encData.decodeBase64()
    }
}

/**
 * @salt encoded as base64
 */
@Serializable
data class BaseKeyParams(val kdf: CryptoSettings.KDF = CryptoSettings.KDF(), val salt: String)  {
    constructor(kdf: CryptoSettings.KDF = CryptoSettings.KDF(), salt: ByteArray) : this(kdf, salt.encodeBase64())

    fun getSalt(): ByteArray {
        return salt.decodeBase64()
    }
}

/**
 * @param baseKeyParams to derive the base key
 * @param hash to derive a user key from the base key, e.g. SHA256
 * @param algo for the user key
 * @param keySize of the user key
 * @param salt to derive a user key using the given algorithm
 * @salt encoded as base64
 */
@Serializable
data class UserKeyParams(var baseKeyParams: BaseKeyParams, val hash: CryptoSettings.HASH_TYPE,
                         val algo: CryptoSettings.SYM_ALGO, val keySize: Int, val salt: String) {
    constructor(baseKeyParams: BaseKeyParams, hash: CryptoSettings.HASH_TYPE, settings: CryptoSettings.Symmetric,
                salt: ByteArray) : this(baseKeyParams, hash, settings.algo, settings.keySize, salt.encodeBase64())

    fun getSalt(): ByteArray {
        return salt.decodeBase64()
    }
}


/**
 * Caches the expensive calculation of the base key
 */
class BaseKeyCache(var strategy: DeriveStrategy = NonCancleableStrategy()) {
    private val baseKeyCache: MutableMap<String, SecretKey> = HashMap()

    interface DeriveStrategy {
        fun derive(baseKeyParams: BaseKeyParams, password: String): Future<SecretKey>
    }

    class NonCancleableStrategy : DeriveStrategy {
        override fun derive(baseKeyParams: BaseKeyParams, password: String): Future<SecretKey> = async {
            CryptoHelper.crypto.deriveKey(password, baseKeyParams.getSalt(), baseKeyParams.kdf)
        }
    }

    private suspend fun hash(baseKeyParams: BaseKeyParams, password: String): String {
        val hashStream = CryptoHelper.sha256Hash()
        hashStream.write(Json.stringify(BaseKeyParams.serializer(), baseKeyParams).toUTF())
        hashStream.write(password.toUTF())
        return hashStream.hash().toHex()
    }

    /**
     * Calculates the base key using a given worker method.
     *
     * Depending on the worker method the returned future is cancelable.
     */
    suspend fun getBaseKey(baseKeyParams: BaseKeyParams, password: String): Future<SecretKey> {
        val hash = hash(baseKeyParams, password)
        return baseKeyCache[hash]?.let { Future.completedFuture(it) } ?: run {
            val future = strategy.derive(baseKeyParams, password)
            future.whenCompleted { key, _ ->
                if (key != null)
                    baseKeyCache[hash] = key
            }
            future
        }
    }

    suspend fun getUserKey(userKeyParams: UserKeyParams, password: String)
            : Future<SecretKey> {
        val baseKeyFuture = getBaseKey(userKeyParams.baseKeyParams, password)
        return baseKeyFuture.thenSuspend {
            getUserKey(userKeyParams, it)
        }
    }

    private suspend fun getUserKey(userKeyParams: UserKeyParams, baseKey: SecretKey): SecretKey {
        val hashStream = CryptoHelper.getHashStream(userKeyParams.hash)
        hashStream.write(CryptoHelper.crypto.encode(baseKey))
        hashStream.write(userKeyParams.getSalt())
        val rawKey = hashStream.hash().let {
            when {
                it.size == userKeyParams.keySize / 8 -> return@let it
                it.size > userKeyParams.keySize / 8 -> return@let it.copyOf(userKeyParams.keySize / 8)
                else -> throw Exception("Generated user key is too small")
            }
        }
        return CryptoHelper.crypto.secretKeyFromRaw(rawKey, userKeyParams.algo)
    }
}

@Serializable
class PasswordProtectedKey(val userKeyParams: UserKeyParams, val encKey: EncData) {
    companion object {
        suspend fun create(secretKey: SecretKey, userKeyParams: UserKeyParams, password: String, cache: BaseKeyCache)
                : PasswordProtectedKey {
            val userKey = cache.getUserKey(userKeyParams, password).await()

            val settings = CryptoSettings.default.symmetric
            val iv = CryptoHelper.crypto.generateBits(settings.algo.ivSize)
            val credentials = SecretKeyIvData(userKey, iv, settings.algo)
            val encryptedKey = CryptoHelper.crypto.encryptSymmetric(secretKey.toJson().toUTF(), credentials)

            return PasswordProtectedKey(userKeyParams, EncData.create(encryptedKey, iv, settings.algo))
        }
    }

    suspend fun decryptKey(password: String, cache: BaseKeyCache): SecretKey {
        val userKey = cache.getUserKey(userKeyParams, password).await()
        val jsonKey = CryptoHelper.crypto.decryptSymmetric(encKey.getEncData(), userKey,
                encKey.getIv(), encKey.algo).toUTFString()
        return secretKeyFromJson(jsonKey)
    }
}
