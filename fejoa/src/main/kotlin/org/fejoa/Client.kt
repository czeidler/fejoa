package org.fejoa

import kotlinx.serialization.json.Json
import kotlinx.serialization.toUtf8Bytes
import org.fejoa.command.*
import org.fejoa.crypto.*
import org.fejoa.network.*
import org.fejoa.repository.sync.AccessRight
import org.fejoa.storage.HashValue
import org.fejoa.support.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class Client private constructor(val context: FejoaContext, val userData: UserData,
                                 val connectionAuthManager: ConnectionAuthManager,
                                 val syncer: BranchSyncer,
                                 val branchIndexCache: BranchIndexCache) {
    val logger = DispatcherLogger()

    private var outQueueManagers: List<OutQueueManager> = ArrayList()
    private val inQueueManagers: MutableList<InQueueManager> = ArrayList()
    private var branchUpdateNotifier: BranchUpdateNotifier? = null

    val contactRequestHandler = ContactRequestHandler(this)
    val grantAccessHandler = GrantAccessHandler(userData)
    val branchUpdateHandler = BranchUpdateHandler(userData)

    private var started: Boolean = false

    companion object {
        private suspend fun construct(userData: UserData, connectionAuthManager: ConnectionAuthManager): Client {
            val branchIndexCache = BranchIndexCache(userData.context, connectionAuthManager)
            val syncManager = SyncManager(connectionAuthManager)
            val branchWatcher = BranchWatcher.create(syncManager, userData, branchIndexCache)
            val branchSyncer = BranchSyncer(userData.context, branchWatcher, syncManager, userData)

            val client = Client(userData.context, userData, connectionAuthManager, branchSyncer, branchIndexCache)
            client.syncer.setLogger(client.logger)
            return client
        }

        private suspend fun getCoroutineContext(): CoroutineContext {
            return suspendCoroutine { it.resume(it.context) }
        }

        suspend fun create(baseContext: String, namespace: String, password: String,
                           kdf: CryptoSettings.KDF = CryptoSettings.default.kdf,
                           coroutineContext: CoroutineContext? = null,
                           connectionAuthManager: ConnectionAuthManager? = null): Client {
            val userKeyParams = UserKeyParams(BaseKeyParams(kdf, CryptoHelper.crypto.generateSalt16()),
                    CryptoSettings.HASH_TYPE.SHA256, CryptoSettings.default.symmetric,
                    CryptoHelper.crypto.generateSalt16())

            val context = FejoaContext(AccountIO.Type.CLIENT, baseContext, namespace,
                    coroutineContext ?: getCoroutineContext())
            val userData = UserData.create(context, CryptoSettings.default)
            val userDataSettings = userData.createUserDataConfig(password, userKeyParams)

            val accountIO = context.accountIO
            if (accountIO.exists())
                throw Exception("Account exists")

            accountIO.writeUserDataConfig(userDataSettings)

            return Client.construct(userData, connectionAuthManager
                    ?: ConnectionAuthManager(context.baseKeyCache))
        }

        suspend fun open(baseContext: String, namespace: String, password: String,
                         coroutineContext: CoroutineContext? = null): Client {
            val context = FejoaContext(AccountIO.Type.CLIENT, baseContext, namespace,
                    coroutineContext ?: getCoroutineContext())
            return open(context, ConnectionAuthManager(context.baseKeyCache), password)
        }

        suspend fun open(context: FejoaContext, connectionAuthManager: ConnectionAuthManager, password: String)
                : Client {
            val platformIO = platformGetAccountIO(AccountIO.Type.CLIENT, context.context, context.namespace)

            val userDataSettings = platformIO.readUserDataConfig()
            val openedSettings = userDataSettings.open(password, context.baseKeyCache)

            val userData = UserData.open(context, openedSettings.first, openedSettings.second.branch)
            return Client.construct(userData, connectionAuthManager)
        }

        suspend fun retrieveAccount(baseContext: String, namespace: String, url: String, user: String,
                                    passwordGetter: PasswordGetter,
                                    connectionAuthManager: ConnectionAuthManager? = null)
                : Client {
            val context = FejoaContext(AccountIO.Type.CLIENT, baseContext, namespace, getCoroutineContext())
            val remote = Remote("no_id", user, url)
            val authManager = connectionAuthManager ?: ConnectionAuthManager(context.baseKeyCache,
                    passwordGetter)

            val reply = authManager.send(GetServerConfigJob(user),
                    remote, LoginAuthInfo()).await()
            if (reply.code != ReturnType.OK)
                throw Exception(reply.message)
            val userDataConfig = reply.storageConfig?.userDataConfig
                    ?: throw Exception("Missing user data config")

            context.accountIO.writeUserDataConfig(userDataConfig)

            val openPassword = passwordGetter.get(PasswordGetter.Purpose.OPEN_ACCOUNT,
                    namespace)
            ?: throw Exception("Canceled by user")

            // pull user data branch
            val credentials = userDataConfig.open(openPassword, context.baseKeyCache)
            val userDataDir = context.getStorage(credentials.second.branch, credentials.first, false)
            val syncResult = SyncManager.sync(context, userDataDir, authManager, remote,
                    LoginAuthInfo(), passwordGetter).await()

            if (syncResult.code != ReturnType.OK)
                throw Exception("Failed to pull user data from ${remote.server}")

            val client = open(context, authManager, openPassword)
            // pull all other userdata branches
            val branches = client.userData.getBranches(UserData.USER_DATA_CONTEXT, true).filter {
                it.branch.toHex() != userDataDir.branch
            }

            branches.forEach {
                val branchInfo = it.branchInfo.get()
                val dir = client.userData.getStorageDir(it, false)
                val result = SyncManager.sync(context, dir, authManager, remote,
                        LoginAuthInfo(), passwordGetter).await()

                if (result.code != ReturnType.OK)
                    throw Exception("Failed to pull branch ${it.branch} (${branchInfo.description})" +
                            " from ${remote.server}")
            }

            return client
        }
    }

    fun getSyncManager(): SyncManager {
        return syncer.syncManager
    }

    /**
     * Registers an account at a remote server
     *
     * @param remoteId if not null this id is used for the new remote
     */
    suspend fun registerAccount(user: String, url: String, password: String, remoteId: String? = null,
                                userKeyParams: UserKeyParams? = null)
        : Pair<RemoteJob.Result, Remote?> {

        val params = userKeyParams ?: UserKeyParams(
                BaseKeyParams(salt = CryptoHelper.crypto.generateSalt16(), kdf = CryptoSettings.default.kdf),
                CryptoSettings.HASH_TYPE.SHA256, CryptoSettings.default.symmetric, CryptoHelper.crypto.generateSalt16())

        val loginSecret = userData.context.baseKeyCache.getUserKey(params, password).await()
        val loginParams = LoginParams(params,
                CompactPAKE_SHA256_CTR.getSharedSecret(DH_GROUP.RFC5114_2048_256, loginSecret),
                DH_GROUP.RFC5114_2048_256)

        val request = platformCreateHTTPRequest(url)

        val inQueueName = CryptoHelper.generateSha256Id().toHex()
        val accessStore = CryptoHelper.generateSha256Id().toHex()

        val result = RegisterJob(user, loginParams, userData.createServerConfig(password, params,
                inQueueName, accessStore)).run(request)
        if (result.code != ReturnType.OK)
            return result to null

        // add the queue and access store to the user data
        val remote = if (remoteId == null)
            Remote.create(user, url)
        else
            Remote(remoteId, user, url)
        userData.writeRemote(remote)
        userData.createInQueue(inQueueName, remote)
        userData.createAccessStore(accessStore, remote)
        // TODO restart client...

        return result to remote
    }

    /**
     * Send a request to the given remote.
     *
     * If the contact public key is unknown the request is insecure, i.e. vulnerable against man in the middle attacks.
     *
     * @param contactRemote the contact's remote
     * @param contactAsymEnc the contact's asym encryption key (if known)
     * @param contactVerification the contact's verification key (if known)
     */
    suspend fun sendContactRequest(contactRemote: Remote, contactVerification: PublicKeyData?,
                                   contactAsymEnc: PublicKeyData?, addToRequested: Boolean) {
        val senderSigningCredentials = userData.getSigningCredentials()
        val contactRequest = ContactRequestCommand(userData.id.get(),
                userData.remotes.listRemotes().first(),
                userData.getAsymEncCredentials().asJsonPublicKeyData(),
                senderSigningCredentials.asJsonPublicKeyData())

        getOutQueueManagers().firstOrNull()?.outQueue
                ?.post(Json.stringify(ContactRequestCommand.serializer(), contactRequest), userData, contactRemote,
                        contactAsymEnc) ?: throw Exception("Missing out queue")

        if (addToRequested) {
            // save contact request
            val hash = CryptoHelper.sha256Hash()
            hash.write(contactRemote.user.toUtf8Bytes())
            hash.write(contactRemote.server.toUtf8Bytes())
            userData.contacts.requested.get(HashValue(hash.hash())).write(ContactRequest(contactRemote,
                    verificationKey = contactVerification?.asJsonPublicKeyData(),
                    asymEncKey = contactAsymEnc?.asJsonPublicKeyData()))
            userData.commit()
        }

        // There is a race conditions that needs to be considered when the client crashes after the request has been
        // sent but the request hasn't been added to the requested list. In this case, either the client resents the
        // request again or the contact is quicker and approves the requests and sends his details. In the later case
        // the client receives the approval as a contact request and approves/ rejects the request. If the client,
        // rejects the request (even the client sent the initial contact request) this is equivalent of deleting an
        // existing contact from the contact list. In any case the user data stays in a sound state.
    }

    suspend fun grantAccess(contact: Contact, branch: Branch, right: AccessRight) {
        /* 1) Update branch access in the user data
           2) Update the access store(s)
           3) Send access command

           Afterwards:
           4) Wait for access store to be uploaded
           5) Notify contact when branch has been updated
           Step 4 and 5 happen throw the branch update notification manager
        */

        val tokenBuilder = TokenBuilder.create(AccessToken.create(mutableMapOf(branch.branch.toHex() to right)))
        val branchInfo = branch.branchInfo.get()

        // 1)
        val serverToken = tokenBuilder.getServerToken()
        val tokenId = HashValue.fromHex(serverToken.id)
        val contactAccess = branch.contactAccessMap.get(tokenId)
        contactAccess.write(ContactAccess(contact.id.get(), tokenId, tokenBuilder.token))
        userData.commit()

        // 2)
        val remoteIds = branch.locations.listRemoteRefs().map { it.remoteId }
        val accessStoresRemoteRefs = userData.getAccessStores().mapNotNull { storeBranch ->
            storeBranch.locations.listRemoteRefs().firstOrNull {
                remoteIds.contains(it.remoteId)
            } ?.let {
                storeBranch to it
            }
        }
        accessStoresRemoteRefs.forEach {
            val accessStore = AccessStore(userData.getStorageDir(it.first))
            accessStore.token.get(tokenId).write(serverToken)
            accessStore.commit()
        }

        // 3)
        val authToken = tokenBuilder.getContactToken()
        val remoteRefs = accessStoresRemoteRefs.map {
            val remoteRef = it.second
            RemoteRef.TokenRemoteRefIO(remoteRef.remoteId, authToken)
        }.toMutableList()
        val command = GrantAccessCommand(GrantAccessCommand.NAME, userData.id.get(), branch.context, branchInfo,
                remoteRefs)
        getOutQueueManagers().firstOrNull()?.outQueue?.post(Json.stringify(GrantAccessCommand.serializer(), command),
                userData, contact) ?: throw Exception("Missing out queue")

        // update notification map
        branchUpdateNotifier?.notificationStore?.let { store ->
            val remotes = accessStoresRemoteRefs.map {
                it.first to userData.remotes.get(HashValue.fromHex(it.second.remoteId)).get()
            }
            remotes.forEach {
                val accessStoreBranch = it.first.branch
                val remote = it.second
                store.updateNotifyStatus(remote, branch.context, branch.branch, contact.id.get(),
                        WaitForAccessStore(accessStoreBranch))
            }
        }
    }

    suspend fun close() {
        stop()
        userData.close()
    }

    /**
     * Set a branch location for all branches in the given context
     */
    suspend fun setBranchLocation(remote: Remote, authInfo: AuthInfo, context: String) {
        userData.getBranches(context, true).forEach {
             it.updateRemote(remote, authInfo)
        }
        if (syncer.isSyncing)
            syncer.start(userData.remotes.listRemotes())
    }

    suspend fun getOutQueueManagers(forceReload: Boolean = false): List<OutQueueManager> {
        if (outQueueManagers.isNotEmpty() && !forceReload)
            return outQueueManagers

        outQueueManagers = userData.getOutQueues().map {
            val queue = OutQueue(userData.getStorageDir(it))
            OutQueueManager(userData.context, connectionAuthManager, queue, logger)
        }
        return outQueueManagers
    }

    suspend fun start() {
        if (started) return else started = true

        val remotes = userData.remotes.listRemotes()
        syncer.start(remotes)

        // reload out queue managers
        getOutQueueManagers(true).forEach { it.start() }

        userData.getInQueues().forEach { branch ->
            val manager = InQueueManager(context, InQueue(userData.getStorageDir(branch)), logger, {
                val dbValue = userData.keys.asymEncKeys.get(it)
                if (!dbValue.exists()) null else dbValue.get().getPrivateKey()
            }, { signer, keyId ->
                val keyDBValue = userData.contacts.list.get(signer).keys.verificationKeys.get(keyId)
                if (!keyDBValue.exists())
                    null
                else
                keyDBValue.get().asPublicKeyData()
            })
            manager.addCommandHandler(contactRequestHandler)
            manager.addCommandHandler(grantAccessHandler)
            manager.addCommandHandler(branchUpdateHandler)
            manager.addCommandHandler(MigrationHandler(userData, branchIndexCache))
            manager.start()
            inQueueManagers.add(manager)
        }

        branchUpdateNotifier = BranchUpdateNotifier.create(this)
        branchUpdateNotifier?.start(remotes)
    }

    fun stop() {
        if (!started) return else started = false

        syncer.stop()

        outQueueManagers.forEach { it.stop() }
        outQueueManagers = emptyList()

        inQueueManagers.forEach { it.stop() }
        inQueueManagers.clear()

        branchUpdateNotifier?.stop()
        branchUpdateNotifier = null
    }

    suspend fun restart(startIfNotRunning: Boolean = true) {
        if (!startIfNotRunning && !started)
            return
        stop()
        start()
    }

    suspend fun restartSyncer(startIfNotRunning: Boolean = true) {
        if (!startIfNotRunning && !started)
            return
        syncer.stop()
        val remotes = userData.remotes.listRemotes()
        syncer.start(remotes)
    }
}

suspend fun <T>Client.withLooper(block: suspend Client.() -> T): T {
    return context.looper.runSync {
        block.invoke(this)
    }
}