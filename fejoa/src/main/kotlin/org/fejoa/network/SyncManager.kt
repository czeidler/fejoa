package org.fejoa.network

import org.fejoa.*
import org.fejoa.repository.Repository
import org.fejoa.repository.ThreeWayMerge
import org.fejoa.storage.Config
import org.fejoa.storage.Hash
import org.fejoa.storage.StorageDir
import org.fejoa.support.Future
import org.fejoa.support.assert
import org.fejoa.support.async
import org.fejoa.support.await


class SyncManager(val authManager: ConnectionAuthManager) {
    class SyncResult(val code: ReturnType, val message: String, val head: Hash?)

    companion object {
        /**
         * To be called from the event loop
         */
        fun sync(fejoaContext: FejoaContext, storageDir: StorageDir, authManager: ConnectionAuthManager, remote: Remote,
                         authInfo: AuthInfo, passwordGetter: PasswordGetter?): Future<SyncResult> {
            var pullJob: Future<PullJob.Result>? = null
            var pushJob: Future<PushJob.Result>? = null

            val user = remote.user
            val branch = storageDir.branch
            val repository = storageDir.getBackingDatabase() as Repository

            val future = async {
                pullJob = authManager.send(
                        PullJob(repository, storageDir.getCommitSignature(), ThreeWayMerge(), user, branch),
                        remote, authInfo, passwordGetter)

                val pullResult = pullJob!!.await()
                pullJob = null
                if (pullResult.code != ReturnType.OK)
                    return@async SyncResult(pullResult.code, pullResult.message, null)

                val remoteRepoExists = pullResult.remoteHead != null
                val remoteHead = if (pullResult.remoteHead != null) {
                    if (pullResult.remoteHead.value.isZero) {
                        null
                    } else
                        pullResult.remoteHead
                } else null

                // Check if we need to update the branch index in case the local repo has been initialized but
                // not written too. In this case we manually have to write the first branch index entry.
                val branchIndex = BranchIndex(fejoaContext.getLocalIndexStorage())
                fejoaContext.branchIndexLocker.runSync {
                    if (branchIndex.getLogTip(branch) == null) {
                        val branchBackend = (storageDir.getBackingDatabase()
                                as Repository).branchBackend
                        branchBackend.getBranchLog().getHead()?.let {
                            branchIndex.update(branch, it.entryId)
                            branchIndex.commit()
                        }
                    }
                }

                // the following block is fine to not run in the looper
                val localHead = run {
                    val head = storageDir.getHead()
                    if (pullResult.oldHead != head && head != null)
                        storageDir.onTipUpdated(pullResult.oldHead?.value ?: Config.newDataHash(), head.value)

                    if (remoteRepoExists && remoteHead == head)
                        return@async SyncResult(ReturnType.OK, "Sync after pull: $branch", head)

                    head
                }

                val localExists = repository.log.getHead() != null
                if (!localExists)
                    return@async SyncResult(ReturnType.OK, "Nothing to push", null)

                pushJob = authManager.send(PushJob(repository, user, branch), remote, authInfo,
                        passwordGetter)

                val pushResult = pushJob!!.await()
                pushJob = null

                return@async SyncResult(pushResult.code, pushResult.message, localHead)
            }
            future.whenCompleted { _, error ->
                if (error is Future.CancelationException) {
                    pullJob?.cancel()
                    pushJob?.cancel()
                }
            }
            return future
        }
    }

    val ongoingSyncs = OngoingSyncs()

    class OngoingSyncs(val map: MutableMap<String, Pair<Remote, Future<SyncResult>>> = HashMap()) {
        fun isSyncing(branch: String): Future<SyncResult>? {
            return map[branch]?.second
        }

        fun startSyncing(branch: String, remote: Remote, job: Future<SyncResult>) {
            assert(map[branch] == null)
            map[branch] = remote to job
        }

        fun finishSyncing(branch: String) {
            map.remove(branch) ?: throw Exception("branch not syncing")
        }
    }

    /**
     * To be called from the event loop
     */
    fun sync(context: FejoaContext, storageDir: StorageDir, remote: Remote, authInfo: AuthInfo,
                     passwordGetter: PasswordGetter? = null): Future<SyncResult> {
        val branch = storageDir.branch
        val ongoingJob = ongoingSyncs.isSyncing(branch)
        if (ongoingJob != null)
            return ongoingJob

        val future = sync(context, storageDir, authManager, remote, authInfo, passwordGetter)
        ongoingSyncs.startSyncing(branch, remote, future)

        future.whenCompleted { _, _ ->
            ongoingSyncs.finishSyncing(branch)
        }
        return future
    }
}