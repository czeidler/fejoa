package org.fejoa.network

import kotlinx.serialization.Serializable
import kotlinx.serialization.serializer


@Serializable
class ErrorMessage(val code: ReturnType, val message: String)


fun JsonRPCSimpleRequest.makeError(error: ReturnType, message: String): String {
    return JsonRPCMethodRequest.makeError(id, ErrorMessage(ensureError(error), message))
}


fun <T>JsonRPCRequest<T>.makeError(error: ReturnType, message: String): String {
    return JsonRPCMethodRequest.makeError(id, ErrorMessage(ensureError(error), message))
}


fun JsonRPCMethodRequest.Companion.makeError(id: Int, error: ReturnType, message: String): String {
    return makeError(id, ErrorMessage(ensureError(error), message))
}

fun JsonRPCMethodRequest.Companion.makeError(id: Int, error: ErrorMessage): String {
    return JsonRPCError(id = id, error = error).stringify(ErrorMessage.serializer())
}

fun JsonRPCMethodRequest.makeError(error: ReturnType, message: String): String {
    return JsonRPCMethodRequest.Companion.makeError(id, ErrorMessage(ensureError(error), message))
}