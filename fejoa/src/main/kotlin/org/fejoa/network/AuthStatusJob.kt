package org.fejoa.network

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer
import kotlinx.serialization.stringify


class AuthStatusJob : RemoteJob<AuthStatusJob.Result>() {
    companion object {
        val METHOD = "authStatus"
    }

    class Result(code: ReturnType, message: String, val response: Response? = null)
        : RemoteJob.Result(code, message)

    enum class RegistrationType {
        EMAIL
    }

    @Serializable
    class PendingRegistration(val user: String, val type: RegistrationType)

    @Serializable
    class Response(val accounts: List<String>, val pendingRegistrations: List<PendingRegistration>)

    private fun getHeader(): String {
        return Json.stringify(JsonRPCSimpleRequest.serializer(), JsonRPCSimpleRequest(id = id, method = METHOD))
    }

    override suspend fun run(remoteRequest: RemoteRequest): Result {
        val replyString = remoteRequest.send(getHeader()).receiveHeader()

        val response = try {
            JsonRPCResponse.parse(Response.serializer(), replyString, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage.serializer(), replyString, id).error
            return Result(ensureError(error.code), error.message)
        }

        return Result(ReturnType.OK, "Auth status received", response.result)
    }
}
