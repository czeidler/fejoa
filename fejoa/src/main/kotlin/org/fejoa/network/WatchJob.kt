package org.fejoa.network

import kotlinx.serialization.Serializable
import kotlinx.serialization.serializer
import org.fejoa.storage.HashValue
import org.fejoa.storage.HashValueDataSerializer


class WatchJob(val branchWatchList: List<WatchEntry>, val peek: Boolean) : RemoteJob<WatchJob.Result>() {
    companion object {
        val METHOD = "watch"
    }

    class Result(code: ReturnType, message: String, val updated: List<BranchStatus>, val denied: List<String>)
        : RemoteJob.Result(code, message)

    @Serializable
    class WatchEntry(val user: String, val branch: String,
                     @Serializable(with=HashValueDataSerializer::class) val logTip: HashValue?)

    @Serializable
    class RPCParams(val branches: List<WatchEntry>, val peek: Boolean)

    @Serializable
    class BranchStatus(val user: String, val branch: String,
                       @Serializable(with=HashValueDataSerializer::class)val logTip: HashValue?, val logMessage: String)

    @Serializable
    class RPCResult(val status: List<BranchStatus> = ArrayList(), val denied: List<String> = ArrayList())


    private fun getHeader(): String {
        return JsonRPCRequest(id = id, method = METHOD, params = RPCParams(branchWatchList, peek))
                .stringify(RPCParams.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): Result {
        val replyString = remoteRequest.send(getHeader()).receiveHeader()
        val response = JsonRPCResponse.parse(RPCResult.serializer(), replyString, id)
        val result = response.result
        return Result(ReturnType.OK, "ok", result.status, result.denied)
    }
}
