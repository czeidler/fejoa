package org.fejoa.network

import kotlinx.serialization.Serializable
import kotlinx.serialization.serializer
import org.fejoa.storage.StorageBackend


class ListBranchesJob(val user: String) : RemoteJob<ListBranchesJob.Result>() {
    companion object {
        val METHOD = "listBranches"
    }

    @Serializable
    class BranchEntry(val branch: String, val stats: StorageBackend.Stats)

    class Result(status: ReturnType, message: String, val branches: List<BranchEntry>)
        : RemoteJob.Result(status, message)

    @Serializable
    class Params(val user: String)

    @Serializable
    class Reply(val branches: List<BranchEntry>)

    private fun getHeader(): String {
        return JsonRPCRequest(id = id, method = METHOD, params = Params(user)).stringify(Params.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): Result {
        val reply = remoteRequest.send(getHeader())
        val responseHeader = reply.receiveHeader()
        val response = try {
            JsonRPCResponse.parse(Reply.serializer(), responseHeader, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage.serializer(), responseHeader, id).error
            return Result(ensureError(error.code), error.message, emptyList())
        }

        return Result(ReturnType.OK, "ok", response.result.branches)
    }
}
