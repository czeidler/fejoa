package org.fejoa.network

import kotlinx.serialization.Serializable
import kotlinx.serialization.serializer


class DeleteBranchJob(val user: String, val branches: List<String>) : RemoteJob<DeleteBranchJob.Result>() {
    companion object {
        val METHOD = "deleteBranch"
    }

    class Result(status: ReturnType, message: String, val deletedBranches: List<String>)
        : RemoteJob.Result(status, message)


    @Serializable
    class Params(val user: String, val branches: List<String>)

    @Serializable
    class Reply(val deletedBranches: List<String>)

    private fun getHeader(): String {
        return JsonRPCRequest(id = id, method = METHOD, params = Params(user, branches)).stringify(
                Params.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): Result {
        val reply = remoteRequest.send(getHeader())
        val responseHeader = reply.receiveHeader()
        val response = try {
            JsonRPCResponse.parse(Reply.serializer(), responseHeader, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage.serializer(), responseHeader, id).error
            return Result(ensureError(error.code), error.message, emptyList())
        }

        return Result(ReturnType.OK, "ok", response.result.deletedBranches)
    }
}
