package org.fejoa

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.network.RemoteIndexJob
import org.fejoa.network.ReturnType
import org.fejoa.network.SyncManager
import org.fejoa.network.WatchJob
import org.fejoa.repository.Repository
import org.fejoa.storage.StorageDir
import org.fejoa.support.Future
import org.fejoa.support.await


class RemoteIndexWatcher(val context: FejoaContext, val syncManager: SyncManager, val remote: Remote,
                         val branchIndexCache: BranchIndexCache,
                         private val logger: Logger,
                         private val onBranchIndexUpdated: (Remote, StorageDir) -> Any) {
    private var isWorking: Boolean = false
    private var remoteIndexBranch: String? = null
    private var fetchIndexJob: Future<RemoteIndexJob.Result>? = null
    private var watchJob: Future<WatchJob.Result>? = null
    private var syncJob: Future<SyncManager.SyncResult>? = null

    /**
     * Called from a free coroutine (not in the event looper)
     */
    private suspend fun work() {
        var first = true
        while (isWorking) {
            val authInfo = LoginAuthInfo()
            val branch = getRemoteIndexBranch() ?: break

            val indexStorageDir = context.looper.runSync {
                context.getRemoteIndexStorage(remote.id, branch)
            }
            context.looper.runSync {
                if (first) {
                    // An up to date index doesn't mean that all branches has been synced. Thus, notify once before to
                    // handle this case.
                    onBranchIndexUpdated.invoke(remote, indexStorageDir)
                    first = false
                }

                val localHead = (indexStorageDir.getBackingDatabase() as Repository).branchBackend
                        .getBranchLog().getHead()

                watchJob = syncManager.authManager.send(
                        WatchJob(listOf(WatchJob.WatchEntry(remote.user, branch, localHead?.entryId)), false),
                        remote, authInfo)
            }

            // we might have stopped working in the meantime
            if (!isWorking) {
                watchJob?.cancel()
                continue
            }

            val watchResult = watchJob?.await() ?: continue
            watchJob = null

            context.looper.runSync {
                if (watchResult.updated.isEmpty()) {
                    logger.syncInfo(SyncEvent.timeout(SyncEvent.BranchType.REMOTE_INDEX, "", branch, remote))
                    return@runSync
                }
                logger.syncInfo(SyncEvent.watchResult(SyncEvent.BranchType.REMOTE_INDEX, "", branch, remote))
                logger.syncInfo(SyncEvent.syncing(SyncEvent.BranchType.REMOTE_INDEX, "", branch, remote))
                syncJob = syncManager.sync(context, indexStorageDir, remote, authInfo)
            }

            // we might have stopped working in the meantime
            if (!isWorking) {
                syncJob?.cancel()
                continue
            }
            val syncResult = syncJob?.await() ?: continue
            syncJob = null

            context.looper.runSync {
                if (syncResult.code == ReturnType.OK)
                    logger.syncInfo(SyncEvent.synced(SyncEvent.BranchType.REMOTE_INDEX, "", branch, remote))
                else {
                    logger.syncError(SyncEvent.error(SyncEvent.BranchType.REMOTE_INDEX, "", branch,
                            syncResult.message))
                }

                onBranchIndexUpdated.invoke(remote, indexStorageDir)
            }
        }
    }

    private suspend fun getRemoteIndexBranch(): String? {
        if (remoteIndexBranch != null)
            return remoteIndexBranch

        fetchIndexJob = branchIndexCache.getRemoteIndexBranch(remote)
        val result = fetchIndexJob!!.await()
        fetchIndexJob = null
        if (result.code != ReturnType.OK) {
            context.looper.runSync {
                logger.syncError(SyncEvent.error(SyncEvent.BranchType.REMOTE_INDEX, "", "",
                        "Failed to get remote index: ${result.message}$"))
            }
            stop()
            return null
        }
        remoteIndexBranch = result.branch
        return remoteIndexBranch
    }

    fun start() {
        if (isWorking)
            return
        isWorking = true
        GlobalScope.launch(context.coroutineContext) {
            try {
                work()
            } catch (exception: Throwable) {
                this@RemoteIndexWatcher.context.looper.runSync {
                    logger.syncError(SyncEvent.error(SyncEvent.BranchType.REMOTE_INDEX, "", "",
                            exception.message
                            ?: "Exception while syncing the remote index (remote ${remote.server})"))
                }
                if (exception !is Future.CancelationException) {
                    if (isWorking) { // restart on failure TODO: catch where is error happened
                        //delay(1000)
                        //stop()
                        //start()
                    }
                }
            }
        }
    }

    fun stop() {
        isWorking = false

        fetchIndexJob?.cancel()
        fetchIndexJob = null

        watchJob?.cancel()
        watchJob = null

        syncJob?.cancel()
        syncJob = null
    }
}