package org.fejoa

import org.fejoa.crypto.*
import org.fejoa.storage.platformCreateStorage
import org.fejoa.test.testAsync
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.test.Test
import kotlin.test.assertEquals


class UserDataTest {
    private val cleanUpList: MutableList<String> = ArrayList()

    suspend fun cleanUp() {
        val backend = platformCreateStorage("")
        for (namespace in cleanUpList)
            backend.deleteNamespace(namespace)
        cleanUpList.clear()
    }

    @Test
    fun testCreateOpen() = testAsync(cleanUp = { cleanUp() }) {
        val contextPath = ""
        val namespace = "testCreateOpen"
        val password = "Password"
        cleanUpList += namespace
        val coroutineContext = suspendCoroutine<CoroutineContext> { it.resume(it.context)  }
        val context = FejoaContext(AccountIO.Type.CLIENT, contextPath, namespace, coroutineContext)
        val userData = UserData.create(context)
        userData.commit()

        val settings = userData.createUserDataConfig(password,
                UserKeyParams(BaseKeyParams(CryptoSettings.default.kdf, CryptoHelper.crypto.generateSalt16()),
                        CryptoSettings.HASH_TYPE.SHA256,
                        CryptoSettings.default.symmetric,
                        CryptoHelper.crypto.generateSalt16()))
        context.accountIO.writeUserDataConfig(settings)

        val loadedSettings = context.accountIO.readUserDataConfig()

        val newContext = FejoaContext(AccountIO.Type.CLIENT, contextPath, namespace, coroutineContext)
        val plainUserDataSettings = loadedSettings.open(password, newContext.baseKeyCache)
        val loadedUserData = UserData.open(newContext, plainUserDataSettings.first,
                plainUserDataSettings.second.branch)

        assertEquals(userData.branch, loadedUserData.branch)
    }
}
