package org.fejoa

import kotlinx.serialization.json.Json
import org.fejoa.jsbindings.await
import org.fejoa.storage.AccountStorage
import org.fejoa.support.PathUtils
import kotlin.js.json

actual fun platformGetAccountIO(type: AccountIO.Type, context: String, namespace: String): AccountIO {
    return JSAccountIO(context, namespace)
}

class JSAccountIO(val context: String, namespace: String) : AccountIO {
    val namespace: String = PathUtils.toPath(namespace)

    override suspend fun exists(): Boolean {
        return AccountStorage(context, namespace).configExists()
    }

    companion object {
        private const val LOGIN_DATA_KEY = "loginData"
        private const val USER_DATA_CONFIG_KEY = "userDataConfig"
    }

    private suspend fun writeConfig(key: String, value: String) {
        val db = AccountStorage(context, namespace).openOrCreateConfig()
        val transaction = db.transaction(AccountStorage.CONFIG_STORE, "readwrite")
        val store = transaction.objectStore(AccountStorage.CONFIG_STORE)
        store.put(json(AccountStorage.CONFIG_KEY to key, "data" to value)).await()
    }

    private suspend fun readConfig(key: String): String {
        val db = AccountStorage(context, namespace).openConfig()
        val transaction = db.transaction(AccountStorage.CONFIG_STORE, "readonly")
        val store = transaction.objectStore(AccountStorage.CONFIG_STORE)
        return store.get(key).await()["data"].unsafeCast<String>()
    }

    override suspend fun writeLoginData(loginData: LoginParams) {
        writeConfig(LOGIN_DATA_KEY, Json(indented = true).stringify(LoginParams.serializer(), loginData))
    }

    override suspend fun readLoginData(): LoginParams {
        return Json.parse(LoginParams.serializer(), readConfig(LOGIN_DATA_KEY))
    }

    override suspend fun writeUserDataConfig(userDataConfig: UserDataConfig) {
        writeConfig(USER_DATA_CONFIG_KEY, Json(indented = true).stringify(UserDataConfig.serializer(), userDataConfig))
    }

    override suspend fun readUserDataConfig(): UserDataConfig {
        return Json.parse(UserDataConfig.serializer(), readConfig(USER_DATA_CONFIG_KEY))
    }

    override suspend fun writeStorageConfig(storageConfig: StorageConfig) {
        TODO("not implemented")
    }

    override suspend fun readStorageConfig(): StorageConfig {
        TODO("not implemented")
    }

    override suspend fun deleteStorageConfig() {
        TODO("not implemented")
    }

    override suspend fun writeMigrationConfig(config: MigrationConfig) {
        TODO("not implemented")
    }

    override suspend fun readMigrationConfig(): MigrationConfig? {
        TODO("not implemented")
    }

    override suspend fun writeServerUserInfo(info: ServerUserInfo) {
        TODO("not implemented")
    }

    override suspend fun readServerUserInfo(): ServerUserInfo? {
        TODO("not implemented")
    }
}