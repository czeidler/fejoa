package org.fejoa.storage

import org.fejoa.jsbindings.IDBDatabase
import org.fejoa.jsbindings.IDBVersionChangeEvent
import org.fejoa.jsbindings.await
import org.fejoa.jsbindings.indexDB
import kotlin.browser.window
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.js.json


class AccountStorage(val context: String, val namespace: String) {
    companion object {
        const val CHUNKS_OBJECT_STORE = "chunks"
        const val LOG_OBJECT_STORE = "log"
        const val CONFIG_STORE = "config"

        const val CHUNK_STORE_KEY = "key"
        const val LOG_TIME_KEY = "time"
        const val CONFIG_KEY = "entry"
    }

    private fun getDBNamespacePrefix(): String {
        return if (context.isBlank())
            namespace
        else
            "$context/$namespace"
    }
    private fun getDBBranchPrefix() = "${getDBNamespacePrefix()}/branches"
    private fun getDBName(branch: String): String = "${getDBBranchPrefix()}/$branch"
    private fun getDBConfigName(): String = "${getDBNamespacePrefix()}/config"


    suspend fun createBranch(branch: String): IDBDatabase {
        return createDB(getDBName(branch)) { db, _ ->
            db.createObjectStore(CHUNKS_OBJECT_STORE, json("keyPath" to CHUNK_STORE_KEY))
            db.createObjectStore(LOG_OBJECT_STORE, json("keyPath" to LOG_TIME_KEY))
        }
    }

    suspend fun openBranch(branch: String): IDBDatabase {
        return openDB(getDBName(branch))
    }

    suspend fun branchExists(branch: String): Boolean {
        return dbExists(getDBName(branch))
    }

    suspend fun deleteBranch(branch: String) {
        return deleteDB(getDBName(branch))
    }

    suspend fun deleteNamespace() {
        val nameTracker = IndexDatabaseNameTracker.open()
        val names = nameTracker.list()
        val prefix = getDBNamespacePrefix()
        names.forEach {
            if (it.startsWith(prefix))
                deleteDB(it)
        }
    }

    suspend fun listBranches(): List<String> {
        val nameTracker = IndexDatabaseNameTracker.open()
        val names = nameTracker.list()
        val prefix = getDBBranchPrefix()
        return names.filter {
            it.startsWith(prefix) && dbExists(it)
        }.map {
            // remove prefix and trailing "/"
            it.substring(prefix.length + 1)
        }
    }

    suspend fun openOrCreateConfig(): IDBDatabase {
        return createDB(getDBConfigName()) { db, _ ->
            db.createObjectStore(CONFIG_STORE, json("keyPath" to CONFIG_KEY))
        }
    }

    suspend fun openConfig(): IDBDatabase {
        return openDB(getDBConfigName())
    }

    suspend fun configExists(): Boolean {
        return dbExists(getDBConfigName())
    }

    private suspend fun createDB(dbName: String, onUpgrade: (db: IDBDatabase, event: IDBVersionChangeEvent) -> Unit)
            : IDBDatabase {
        IndexDatabaseNameTracker.open().add(dbName)

        val indexDB = window.indexDB()
        val openRequest = indexDB.open(dbName, 1)

        openRequest.onupgradeneeded = {
            var db = openRequest.result
            onUpgrade.invoke(db, it)
        }

        return openRequest.await()
    }

    private suspend fun openDB(dbName: String): IDBDatabase {
        if (!dbExists(dbName))
            throw Exception("Storage $dbName does not exists")

        val indexDB = window.indexDB()
        val openRequest = indexDB.open(dbName, 1)
        return openRequest.await()
    }

    private suspend fun dbExists(dbName: String): Boolean {
        val indexDB = window.indexDB()
        val openRequest = indexDB.open(dbName, 1)

        var exists = true
        openRequest.onupgradeneeded = {
            exists = false
            it.target.transaction!!.abort()
        }

        try {
            suspendCoroutine<Unit> { cont ->
                openRequest.onerror = {
                    cont.resumeWithException(Throwable(openRequest.error.message))
                }
                openRequest.onsuccess = {
                    cont.resume(Unit)
                }
                openRequest.onblocked = {
                    // if blocked somebody is using it; i.e. it exists
                    cont.resume(Unit)
                }
            }
        } catch (e: Throwable) {
            // catch the abort exception
        }

        return exists
    }

    private suspend fun deleteDB(name: String) {
        val indexDB = window.indexDB()
        indexDB.deleteDatabase(name)//.await() TODO: await on deleteDatabase does not work (uncaught block event)
        IndexDatabaseNameTracker.open().remove(name)
    }
}