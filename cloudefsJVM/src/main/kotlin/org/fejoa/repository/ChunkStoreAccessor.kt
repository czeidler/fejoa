package org.fejoa.repository

import org.fejoa.chunkstore.ChunkStore
import org.fejoa.storage.*
import org.fejoa.support.Future


fun ChunkStore.Transaction.toChunkTransaction(): ChunkTransaction {
    val transaction = this
    return object: ChunkTransaction {
        override fun iterator(): ChunkTransaction.Iterator {
            val parentIterator = transaction.iterator()
            return object : ChunkTransaction.Iterator {
                override suspend fun hasNext(): Boolean {
                    return parentIterator.hasNext()
                }

                override suspend fun next(): Pair<HashValue, ByteArray> {
                    val entry = parentIterator.next()
                    return entry.key to entry.data
                }
            }
        }

        override fun getChunk(boxHash: HashValue): Future<ByteArray> {
            val chunk = transaction.getChunk(boxHash)
                    ?: return Future.failedFuture("Failed to read chunk: $boxHash")
            return Future.completedFuture(chunk)
        }

        override fun prepareChunk(data: ByteArray): Future<Pair<HashValue, ByteArray>> {
            return Future.completedFuture(transaction.key(data) to data)
        }

        override fun putChunk(key: HashValue, rawData: ByteArray): Future<Boolean> {
            return Future.completedFuture(transaction.put(rawData).wasInDatabase)
        }

        override fun hasChunk(boxHash: HashValue): Future<Boolean> {
            return Future.completedFuture(transaction.contains(boxHash))
        }
        override fun finishTransaction(): Future<Unit> {
            return Future.completedFuture(transaction.commit())
        }
        override fun cancel(): Future<Unit> {
            return Future.completedFuture(transaction.cancel())
        }

        override fun flush(): Future<Unit> {
            return Future.completedFuture(Unit)
        }
    }
}
