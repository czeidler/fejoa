package org.fejoa.storage

import org.fejoa.crypto.CryptoHelper
import org.fejoa.support.Future
import org.fejoa.support.async
import java.util.*
import kotlin.collections.HashMap


class MemoryBranchLog(val branch: String) : BranchLog {
    val entries: MutableList<BranchLogEntry> = ArrayList()

    override fun getBranchName(): String {
        return branch
    }

    override suspend fun add(id: HashValue, message: ByteArray, changes: List<HashValue>) {
        entries.add(BranchLogEntry(Date().time, id, BranchLogEntry.Message(message), changes.toMutableList()))
    }

    override suspend fun add(entry: BranchLogEntry) {
        entries.add(entry)
    }

    override suspend fun add(entries: List<BranchLogEntry>) {
        this.entries.addAll(entries)
    }

    override suspend fun add(entry: BranchLogEntry, expectedEntryId: HashValue?): Boolean {
        if (getHead()?.entryId != expectedEntryId)
            return false
        entries.add(entry)
        return true
    }

    override suspend fun getEntries(): List<BranchLogEntry> {
        return entries
    }

    override suspend fun getHead(): BranchLogEntry? {
        return entries.lastOrNull()
    }
}

class MemoryChunkStorage : ChunkStorage {
    val map: MutableMap<HashValue, ByteArray> = HashMap()

    var chunksWritten = 0
    inner class MemoryTransaction : ChunkTransaction {
        override fun finishTransaction(): Future<Unit> {
            return Future.completedFuture(Unit)
        }

        override fun cancel(): Future<Unit> {
            return Future.completedFuture(Unit)
        }

        override fun getChunk(boxHash: HashValue): Future<ByteArray> {
            val data = map[boxHash] ?: return Future.failedFuture("Not existing")
            return Future.completedFuture(data)
        }

        override fun prepareChunk(data: ByteArray): Future<Pair<HashValue, ByteArray>> = async {
            val key = HashValue(CryptoHelper.sha256Hash(data))
            return@async key to data
        }

        override fun putChunk(key: HashValue, rawData: ByteArray): Future<Boolean> {
            chunksWritten ++

            if (map.containsKey(key))
                return Future.completedFuture(true)
            map[key] = rawData
            return Future.completedFuture(false)
        }

        override fun hasChunk(boxHash: HashValue): Future<Boolean> {
            val has = map[boxHash] != null
            return Future.completedFuture(has)
        }

        override fun iterator(): ChunkTransaction.Iterator {
            return object : ChunkTransaction.Iterator {
                val it = map.iterator()
                override suspend fun hasNext(): Boolean {
                    return it.hasNext()
                }

                override suspend fun next(): Pair<HashValue, ByteArray> {
                    val next = it.next()
                    return next.key to next.value
                }
            }
        }

        override fun flush(): Future<Unit> {
            println("MEMORY flush: chunks written: $chunksWritten, chunks: ${map.size}")
            return Future.completedFuture(Unit)
        }

    }

    override fun startTransaction(): ChunkTransaction {
        return MemoryTransaction()
    }
}

class MemoryBranchBackend(val branch: String) : StorageBackend.BranchBackend {
    val log = MemoryBranchLog(branch)
    val storage = MemoryChunkStorage()

    override fun getChunkStorage(): ChunkStorage {
       return storage
    }

    override fun getBranchLog(): BranchLog {
        return log
    }

}

class MemoryBackend : StorageBackend {
    class Namespace(val branches: MutableMap<String, StorageBackend.BranchBackend> = HashMap())
    val namespaces: MutableMap<String, Namespace> = HashMap()

    override suspend fun open(namespace: String, branch: String): StorageBackend.BranchBackend {
        return namespaces[namespace]?.branches?.get(branch) ?: throw Exception("Branch does not exists")
    }

    override suspend fun create(namespace: String, branch: String): StorageBackend.BranchBackend {
        val ns = namespaces[namespace] ?: Namespace()
        if (ns.branches.containsKey(branch))
            throw Exception("Branch exists")
        val backend = MemoryBranchBackend(branch)
        ns.branches[branch] = backend
        return backend
    }

    override suspend fun branchStats(namespace: String, branch: String): StorageBackend.Stats {
        return StorageBackend.Stats(0)
    }

    override suspend fun exists(namespace: String, branch: String): Boolean {
        return namespaces[namespace]?.branches?.containsKey(branch) != null
    }

    override suspend fun delete(namespace: String, branch: String) {
        namespaces[namespace]?.branches?.remove(branch)
    }

    override suspend fun deleteNamespace(namespace: String) {
        namespaces.remove(namespace)
    }

    override suspend fun listBranches(namespace: String): Collection<BranchLog> {
        return namespaces[namespace]?.branches?.map { it.value.getBranchLog() } ?: emptyList()
    }

    override suspend fun listNamespaces(): Collection<String> {
        return namespaces.keys
    }
}