package org.fejoa.storage

import org.fejoa.repository.ChunkStoreBranchLog
import org.fejoa.support.PathUtils
import java.io.File
import java.io.IOException


abstract class BaseStorageBackend(val baseDir: String) : StorageBackend {
    companion object {
        fun getNamespaceDir(baseDir: String, namespace: String): File {
            return File(PathUtils.appendDir(baseDir, namespace))
        }

        fun getBranchDir(baseDir: String, namespace: String, branch: String): File {
            // avoid to go backwards in the dir structure
            if (branch.contains(".."))
                throw Exception("Invalid branch name")

            val namespaceDir = getNamespaceDir(baseDir, namespace)
            return File(namespaceDir, branch)
        }
    }

    protected fun getBranchDir(namespace: String, branch: String): File {
        return BaseStorageBackend.getBranchDir(baseDir, namespace, branch)
    }

    private fun dirSize(dir: File): Long {
        var size = 0L
        for (file in dir.listFiles()) {
            size += if (file.isDirectory)
                dirSize(file)
            else
                file.length()
        }
        return size
    }

    override suspend fun branchStats(namespace: String, branch: String): StorageBackend.Stats {
        val branchDir = getBranchDir(baseDir, namespace, branch)
        return StorageBackend.Stats(dirSize(branchDir))
    }

    override suspend fun delete(namespace: String, branch: String) {
        getBranchDir(namespace, branch).deleteRecursively()
    }

    override suspend fun deleteNamespace(namespace: String) {
        BaseStorageBackend.getNamespaceDir(baseDir, namespace).deleteRecursively()
    }

    override suspend fun listBranches(namespace: String): Collection<BranchLog> {
        val namespaceDir = BaseStorageBackend.getNamespaceDir(baseDir, namespace)
        if (!namespaceDir.exists())
            return emptyList()

        val branches = ArrayList<BranchLog>()
        for (branch in namespaceDir.list()) {
            val branchFile = File(namespaceDir, branch)
            if (branchFile.isFile)
                continue
            if (!exists(namespace, branch))
                continue
            try {
                val branchLog = ChunkStoreBranchLog(namespaceDir, branch)
                branches.add(branchLog)
            } catch (e: IOException) {
                continue
            }
        }
        return branches
    }

    override suspend fun listNamespaces(): Collection<String> {
        return File(baseDir).listFiles()
                .filter { it.isDirectory }
                .map { it.name }
    }
}