package org.fejoa.storage

import org.fejoa.chunkstore.ChunkStore
import org.fejoa.repository.ChunkStoreBranchLog
import org.fejoa.repository.toChunkTransaction


class ChunkStoreBackend(baseDir: String) : BaseStorageBackend(baseDir) {
    class ChunkStoreBranchBackend(val chunkStore: ChunkStore, val baseDir: String, val namespace: String, val branch: String)
        : StorageBackend.BranchBackend {
        override fun getChunkStorage(): ChunkStorage {
            return object : ChunkStorage {
                override fun startTransaction(): ChunkTransaction {
                    return chunkStore.openTransaction().toChunkTransaction()
                }
            }
        }

        override fun getBranchLog(): BranchLog {
            val branchDir = getBranchDir(baseDir, namespace, branch)
            return ChunkStoreBranchLog(branchDir, branch)
        }
    }

    override suspend fun open(namespace: String, branch: String): StorageBackend.BranchBackend {
        val branchDir = getBranchDir(namespace, branch)
        return ChunkStoreBranchBackend(ChunkStore.open(branchDir, branch), baseDir, namespace, branch)
    }

    override suspend fun create(namespace: String, branch: String): StorageBackend.BranchBackend {
        val dir = getBranchDir(namespace, branch)
        dir.mkdirs()
        return ChunkStoreBranchBackend(ChunkStore.create(dir, branch), baseDir, namespace, branch)
    }

    override suspend fun exists(namespace: String, branch: String): Boolean {
        val branchDir = getBranchDir(namespace, branch)
        return ChunkStore.exists(branchDir, branch)
    }
}