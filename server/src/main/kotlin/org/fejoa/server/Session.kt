package org.fejoa.server

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.fejoa.*
import org.fejoa.crypto.CompactPAKE_SHA256_CTR
import org.fejoa.crypto.CryptoHelper
import org.fejoa.network.RegisterJob
import org.fejoa.repository.sync.TransactionManager
import org.fejoa.storage.HashValue
import org.fejoa.storage.StorageDir
import org.fejoa.support.Locker
import java.net.URLDecoder
import java.net.URLEncoder
import javax.servlet.http.HttpSession


fun internalUsername(username: String): String {
    return URLEncoder.encode(username, "UTF-8")
}

fun fromInternalUsername(internal: String): String {
    return URLDecoder.decode(internal, "UTF-8")
}

class TokenAuthManager {
    class AuthRequest(val tokenId: String, val authToken: ByteArray)

    private val ongoing: MutableMap<String, AuthRequest> = HashMap()

    fun initAuthRequest(tokenId: String): AuthRequest {
        val authRequest = AuthRequest(tokenId, CryptoHelper.crypto.generateSalt16())
        ongoing[tokenId] = authRequest
        return authRequest
    }

    fun finishAuthRequest(tokenId: String): AuthRequest? {
        return ongoing.remove(tokenId)
    }
}

class OngoingRegistrationManager {
    class Entry(val config: ConfirmRegistrationConfig, val token: String, val params: RegisterJob.Params,
                val userInfo: ServerUserInfo = ServerUserInfo())

    // user -> Entry
    private val confirmationTokens: MutableMap<String, Entry> = HashMap()

    fun getTokens(): Map<String, Entry> = confirmationTokens

    fun getToken(user: String): Entry? {
        return confirmationTokens[user]
    }

    fun setToken(user: String, token: String, config: ConfirmRegistrationConfig, params: RegisterJob.Params) {
        confirmationTokens[user] = Entry(config, token, params)
    }

    fun clear(user: String) {
        confirmationTokens.remove(user)
    }

    fun clear() {
        confirmationTokens.clear()
    }
}

class Session(val baseDir: String, private val session: HttpSession) {
    private val ROLES_KEY = "roles"
    private val LOGIN_COMPACTPAKE_PROVER_KEY = "loginCompactPAKEProver"
    private val TOKEN_AUTH_PROTOCOL_HANDLER = "tokenAuthProtocolHandler"
    private val REGISTRATION_KEY = "registrationManager"
    private val TRANSACTION_KEY = "transactionManager"

    private fun getSessionLock(): Any {
        // get an immutable lock
        return session.id.intern()
    }

    private fun getLoginCompactPAKEProverKey(user: String): String {
        return "$user:$LOGIN_COMPACTPAKE_PROVER_KEY"
    }

    fun setLoginCompactPAKEProver(user: String, prover: CompactPAKE_SHA256_CTR.ProverState0?) = synchronized(getSessionLock()) {
        session.setAttribute(getLoginCompactPAKEProverKey(user), prover)
    }

    fun getLoginCompactPAKEProver(user: String): CompactPAKE_SHA256_CTR.ProverState0? = synchronized(getSessionLock()) {
        return session.getAttribute(getLoginCompactPAKEProverKey(user))?.let {
            it as CompactPAKE_SHA256_CTR.ProverState0
        }
    }

    fun getTokenAuthManager(user: String): TokenAuthManager = synchronized(getSessionLock()) {
        val key = "$user:$TOKEN_AUTH_PROTOCOL_HANDLER"
        return session.getAttribute(key)?.let { it as TokenAuthManager }
                ?: TokenAuthManager().also {
                    session.setAttribute(key, it)
                }
    }

    fun getServerAccessManager(): ServerAccessTracker = synchronized(getSessionLock()) {
        return session.getAttribute(ROLES_KEY)?.let { it as ServerAccessTracker }
                ?: return ServerAccessTracker(DebugSingleton.isNoAccessControl).also {
                    session.setAttribute(ROLES_KEY, it)
                }
    }

    fun getTransactionManager(): TransactionManager = synchronized(getSessionLock()) {
        return session.getAttribute(TRANSACTION_KEY)?.let { it as TransactionManager }
                ?: return TransactionManager().also {
                    session.setAttribute(TRANSACTION_KEY, it)
                }
    }

    fun getOngoingRegistrationManager(): OngoingRegistrationManager = synchronized(getSessionLock()) {
        return session.getAttribute(REGISTRATION_KEY)?.let { it as OngoingRegistrationManager }
                ?: return OngoingRegistrationManager().also {
                    session.setAttribute(REGISTRATION_KEY, it)
                }
    }

    fun getContext(user: String): FejoaContext {
        return FejoaContext(AccountIO.Type.SERVER, baseDir, user, Dispatchers.IO, object : Locker {
            val mutex = Mutex()
            override suspend fun <T> runSync(block: suspend () -> T): T {
                return mutex.withLock { block.invoke() }
            }
        })
    }

    suspend fun getBranchIndex(user: String): StorageDir {
        return getContext(user).getLocalIndexStorage()
    }

    suspend fun getServerToken(user: String, tokenId: String): ServerToken? {
        val context = getContext(user)
        val config = context.accountIO.readStorageConfig()
        val accessStore = AccessStore(context.getStorage(config.accessStore, null, false))
        val tokenIdHashValue = HashValue.fromHex(tokenId)
        val dbValue = accessStore.token.get(tokenIdHashValue)
        if (dbValue.exists())
            return dbValue.get()

        val migrationConfig = context.accountIO.readMigrationConfig() ?: return null
        if (migrationConfig.token.id == tokenId)
            return migrationConfig.token

        return null
    }
}
