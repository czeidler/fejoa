package org.fejoa.server

import kotlinx.serialization.Optional
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import org.apache.commons.cli.*
import org.eclipse.jetty.server.*

import java.io.File
import java.net.InetSocketAddress
import org.eclipse.jetty.server.handler.ContextHandlerCollection
import org.eclipse.jetty.servlet.DefaultServlet
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.eclipse.jetty.util.resource.Resource
import org.fejoa.support.toUTFString
import java.io.FileInputStream
import org.eclipse.jetty.server.ServerConnector
import org.eclipse.jetty.util.ssl.SslContextFactory
import org.eclipse.jetty.server.ForwardedRequestCustomizer
import org.eclipse.jetty.server.SecureRequestCustomizer
import org.eclipse.jetty.server.HttpConfiguration
import java.net.URI
import java.nio.file.Paths


object DebugSingleton {
    var isNoAccessControl = false
}


@Serializable
data class LetsEncryptConfig(@Optional val enabled: Boolean = false,
                             @Optional val keystorePath: String = "")

@Serializable
data class SMTPConfig(@Optional val user: String = "",
                      @Optional val password: String = "",
                      @Optional val auth: Boolean = true,
                      @Optional val starttls: Boolean = true,
                      @Optional val host: String = "",
                      @Optional val port: Int = 587)
@Serializable
data class ConfirmRegistrationConfig(@Optional val type: Portal.RegisterConfirm = Portal.RegisterConfirm.NONE,
                                     @Optional val smtp: SMTPConfig = SMTPConfig())

@Serializable
data class FejoaServerConfig(@Optional var host: String = "localhost",
                             @Optional var port: Int = JettyServer.DEFAULT_PORT,
                             @Optional var securePort: Int = JettyServer.DEFAULT_SECURE_PORT,
                             @Optional var directory: String = ".",
                             @Optional var registration: ConfirmRegistrationConfig = ConfirmRegistrationConfig(),
                             @Optional var letsencrypt: LetsEncryptConfig = LetsEncryptConfig())

class JettyServer(config: FejoaServerConfig) {
    private val server: Server

    constructor(baseDir: String, port: Int = DEFAULT_PORT) : this(FejoaServerConfig("", port = port,
            directory = baseDir))

    init {
        val baseDir = config.directory
        val host = config.host
        val port = config.port

        println(File(baseDir).absolutePath)
        server = if (host.isBlank())
            Server(port)
        else
            Server(InetSocketAddress(host, port))

        if (config.letsencrypt.enabled)
            enableLetsEncrypt(config)

        val fejoaHandler = ServletContextHandler(ServletContextHandler.SESSIONS)
        fejoaHandler.contextPath = "/"
        fejoaHandler.addServlet(ServletHolder(Portal(baseDir, registerConfirm = config.registration)),
                "/$FEJOA_PORTAL_PATH")
        // serve web directory
        val uiHolder = ServletHolder("default", DefaultServlet())
        uiHolder.setInitParameter("resourceBase", "./web")
        fejoaHandler.addServlet(uiHolder, "/*")

        val resourceContext = resourceHandler()

        val contexts = ContextHandlerCollection()
        contexts.handlers = arrayOf(fejoaHandler, fejoaHandler, resourceContext)
        server.handler = contexts
    }

    private fun enableLetsEncrypt(config: FejoaServerConfig) {
        // for details see:
        // https://danielflower.github.io/2017/04/08/Lets-Encrypt-Certs-with-embedded-Jetty.html
        val httpConfig = HttpConfiguration()
        httpConfig.addCustomizer(SecureRequestCustomizer())
        httpConfig.addCustomizer(ForwardedRequestCustomizer())

        // Create the HTTPS end point
        val sslContextFactory = SslContextFactory()
        sslContextFactory.keyStoreType = "PKCS12"
        sslContextFactory.keyStorePath = config.letsencrypt.keystorePath
        val keyStorePassword = ""
        sslContextFactory.setKeyStorePassword(keyStorePassword)
        sslContextFactory.setKeyManagerPassword(keyStorePassword)
        val httpsConnector = ServerConnector(server, sslContextFactory, HttpConnectionFactory(httpConfig))
        httpsConnector.port = config.securePort
        server.addConnector(httpsConnector)

        val keystorePath = Paths.get(URI.create(sslContextFactory.keyStorePath))
        FileWatcher.onFileChange(keystorePath, Runnable {
            sslContextFactory.reload {
                println("SSL certificate reloaded")
            }
        })
    }

    private fun resourceHandler(): Handler {
        val url = JettyServer::class.java.classLoader.getResource("web_test/")

        val context = ServletContextHandler(ServletContextHandler.SESSIONS)
        context.contextPath = "/web_test"
        context.baseResource = Resource.newResource(url.toURI())

        val holder = ServletHolder("default", DefaultServlet::class.java)
        context.addServlet(holder, "/")
        return context
    }

    @Throws(Exception::class)
    fun start() {
        server.start()
    }

    @Throws(Exception::class)
    fun stop() {
        server.stop()
        server.destroy()
        server.join()
    }

    fun setDebugNoAccessControl(noAccessControl: Boolean) {
        DebugSingleton.isNoAccessControl = noAccessControl
    }

    companion object {
        val FEJOA_PORTAL_PATH = "fejoa.portal"
        val DEFAULT_PORT = 8080
        val DEFAULT_SECURE_PORT = 8443

        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {
            val options = Options()

            val configPathOpt = Option("c", "config", true, "Config file path")
            configPathOpt.isRequired = false
            options.addOption(configPathOpt)

            val input = Option("h", "host", true, "Host ip address")
            input.isRequired = false
            options.addOption(input)

            val output = Option("p", "port", true, "Host port")
            output.isRequired = false
            options.addOption(output)

            val dirOption = Option("d", "directory", true, "Storage directory")
            dirOption.isRequired = false
            options.addOption(dirOption)

            val cmd: CommandLine
            try {
                cmd = DefaultParser().parse(options, args)
            } catch (e: ParseException) {
                println(e.message)
                HelpFormatter().printHelp("Fejoa Server", options)

                System.exit(1)
                return
            }

            val config = if (cmd.hasOption("config")) {
                val path = cmd.getOptionValue("config")
                readConfig(path)
            } else
                FejoaServerConfig()

            if (cmd.hasOption("host"))
                config.host = cmd.getOptionValue("host")
            if (config.host.isBlank()) {
                println("Host not specified")
                System.exit(1)
            }

            if (cmd.hasOption("port")) {
                try {
                    config.port = Integer.parseInt(cmd.getOptionValue("port"))
                } catch (e: NumberFormatException) {
                    println("Port must be a number")
                    System.exit(1)
                    return
                }
            }

            if (cmd.hasOption("directory"))
                config.directory = cmd.getOptionValue("directory")

            val server = JettyServer(config)
            server.start()
        }

        private fun readConfig(path: String): FejoaServerConfig {
            val inStream = FileInputStream(path)
            val content = inStream.readAll().toUTFString()
            return JSON(strictMode = false).parse(FejoaServerConfig.serializer(), content)
        }
    }
}

