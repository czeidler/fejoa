package org.fejoa.server

import kotlinx.coroutines.runBlocking
import org.fejoa.network.JsonRPCRequest
import org.fejoa.network.ListBranchesJob
import org.fejoa.network.ReturnType
import org.fejoa.network.makeError
import java.io.*


class ListBranchesHandler : JsonRequestHandler(ListBranchesJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) = runBlocking {
        val request = JsonRPCRequest.parse(
                ListBranchesJob.Params.serializer(), json)

        val user = internalUsername(request.params.user)
        val accessManager = session.getServerAccessManager()
        if (!accessManager.hasAccountAccess(user)) {
            responseHandler.setResponseHeader(request.makeError(ReturnType.ACCESS_DENIED,
                    "User not authenticated"))
            return@runBlocking
        }
        val context = session.getContext(user)
        val storage = context.platformStorage
        val branches = UserThreadContext.withLock(user) {
            storage.listBranches(user).map {
                val branch = it.getBranchName()
                val stats = storage.branchStats(user, branch)
                ListBranchesJob.BranchEntry(branch, stats)
            }
        }

        val response = request.makeResponse(ListBranchesJob.Reply(branches)).stringify(
                ListBranchesJob.Reply.serializer())
        responseHandler.setResponseHeader(response)
    }
}
