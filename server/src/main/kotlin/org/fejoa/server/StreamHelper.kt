package org.fejoa.server

import org.fejoa.support.StreamHelper
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream


fun InputStream.copyTo(outputStream: OutputStream) {
    val buffer = ByteArray(StreamHelper.BUFFER_SIZE)
    while (true) {
        val length = this.read(buffer)
        if (length <= 0)
            break
        outputStream.write(buffer, 0, length)
    }
}

fun InputStream.readAll(): ByteArray {
    val outputStream = ByteArrayOutputStream()
    this.copyTo(outputStream)
    return outputStream.toByteArray()
}
