package org.fejoa.server

import org.fejoa.network.JsonRPCRequest
import org.fejoa.network.RequestRegistrationTokenJob
import org.fejoa.network.ReturnType
import org.fejoa.network.makeError
import java.io.InputStream


class RequestRegistrationTokenHandler : JsonRequestHandler(RequestRegistrationTokenJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) {

        val request = JsonRPCRequest.parse(
                RequestRegistrationTokenJob.Params.serializer(), json)
        val params = request.params
        val user = internalUsername(params.user)

        val pendingEntry = session.getOngoingRegistrationManager().getToken(user)
                ?: run {
            responseHandler.setResponseHeader(request.makeError(ReturnType.ERROR,
                    "No pending registration for user ${params.user}"))
            return
        }

        pendingEntry.userInfo.email = params.data.email

        sendMail(pendingEntry.config.smtp, params.data.email, "no-reply@fejoa.org",
                "Fejoa: Confirm registration", "Token: ${pendingEntry.token}")

        val response = request.makeResponse(RequestRegistrationTokenJob.Reply("Request sent"))
                .stringify(RequestRegistrationTokenJob.Reply.serializer())
        responseHandler.setResponseHeader(response)
    }
}