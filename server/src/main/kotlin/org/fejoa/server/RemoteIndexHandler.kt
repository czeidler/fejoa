package org.fejoa.server

import kotlinx.coroutines.runBlocking
import org.fejoa.network.JsonRPCRequest
import org.fejoa.network.RemoteIndexJob
import org.fejoa.network.ReturnType
import org.fejoa.network.makeError
import java.io.InputStream


class RemoteIndexRequestHandler : JsonRequestHandler(RemoteIndexJob.METHOD) {

    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?, session: Session)
            = runBlocking {
        val request = JsonRPCRequest.parse(RemoteIndexJob.RPCParams.serializer(), json)
        val params = request.params
        val user = internalUsername(params.user)

        if (!session.getServerAccessManager().hasAccountAccess(user)) {
            responseHandler.setResponseHeader(request.makeError(ReturnType.ACCESS_DENIED, "insufficient rights"))
            return@runBlocking
        }

        val branchName = UserThreadContext.withLock(user) {
            val index = session.getBranchIndex(user)
            index.branch
        }
        val response = request.makeResponse(RemoteIndexJob.RPCResult(branchName))
                .stringify(RemoteIndexJob.RPCResult.serializer())
        responseHandler.setResponseHeader(response)
    }
}
