package org.fejoa.server

import kotlinx.coroutines.runBlocking
import kotlinx.serialization.SerializationException
import kotlinx.serialization.internal.StringSerializer
import kotlinx.serialization.json.JSON
import kotlinx.serialization.serializer
import org.fejoa.AccessToken
import org.fejoa.network.JsonRPCRequest
import org.fejoa.network.ReturnType
import org.fejoa.network.TokenAuthJob
import org.fejoa.network.makeError
import org.fejoa.support.encodeBase64
import java.io.InputStream


class TokenAuthHandler : JsonRequestHandler(TokenAuthJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?, session: Session) {
        try {
            val initParams = JsonRPCRequest.parse(
                    TokenAuthJob.InitParams.serializer(), json)
            if (initParams.params.type != TokenAuthJob.AuthState.INIT)
                throw Exception("Invalid type")
            handleInit(initParams, responseHandler, session)
        } catch (e: SerializationException) {
            val finishParams = JsonRPCRequest.parse(
                    TokenAuthJob.FinishParams.serializer(), json)
            if (finishParams.params.type != TokenAuthJob.AuthState.FINISH)
                throw Exception("Invalid type")
            handleFinish(finishParams, responseHandler, session)
        }
    }

    private fun handleInit(request: JsonRPCRequest<TokenAuthJob.InitParams>, responseHandler: Portal.ResponseHandler,
                           session: Session) {
        val user = internalUsername(request.params.user)
        val authManager = session.getTokenAuthManager(user)
        val authToken = authManager.initAuthRequest(request.params.data.tokenId)

        val result = TokenAuthJob.InitResponse(authToken.authToken.encodeBase64())
        val response = request.makeResponse(result).stringify(TokenAuthJob.InitResponse.serializer())
        responseHandler.setResponseHeader(response)
    }

    private fun handleFinish(request: JsonRPCRequest<TokenAuthJob.FinishParams>,
                             responseHandler: Portal.ResponseHandler, session: Session) = runBlocking {
        val params = request.params
        val user = internalUsername(params.user)
        val authManager = session.getTokenAuthManager(user)
        val tokenId = params.data.tokenId
        val authRequest = authManager.finishAuthRequest(tokenId) ?: run {
            responseHandler.setResponseHeader(request.makeError(ReturnType.ERROR,
                    "Invalid token auth request state"))
            return@runBlocking
        }

        val accessToken = JSON.parse<AccessToken>(AccessToken.serializer(), params.data.token)

        UserThreadContext.withLock(user) {
            val serverToken = session.getServerToken(user, tokenId) ?: run {
                responseHandler.setResponseHeader(request.makeError(ReturnType.ERROR,
                        "Auth token not found"))
                return@withLock
            }

            if (!serverToken.validate(accessToken)) {
                responseHandler.setResponseHeader(request.makeError(ReturnType.ERROR,
                        "Invalid token"))
                return@withLock
            }

            if (!serverToken.authenticate(authRequest.authToken, params.data.signature)) {
                responseHandler.setResponseHeader(request.makeError(ReturnType.ERROR,
                        "Failed to verify token"))
                return@withLock
            }

            accessToken.branches.forEach {
                val branch = it.key
                val accessRight = it.value
                session.getServerAccessManager().addBranchAccess(user, branch, accessRight)
            }

            val response = request.makeResponse("Token $tokenId authenticated").stringify(StringSerializer)
            responseHandler.setResponseHeader(response)
        }
    }

}
