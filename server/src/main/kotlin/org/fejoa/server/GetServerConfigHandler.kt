package org.fejoa.server

import kotlinx.coroutines.runBlocking
import kotlinx.serialization.serializer
import org.fejoa.AccountIO
import org.fejoa.network.JsonRPCRequest
import org.fejoa.network.GetServerConfigJob
import org.fejoa.network.ReturnType
import org.fejoa.network.makeError
import org.fejoa.platformGetAccountIO
import java.io.InputStream


class GetServerConfigHandler : JsonRequestHandler(GetServerConfigJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) = runBlocking {

        val request = JsonRPCRequest.parse(GetServerConfigJob.RPCParams.serializer(), json)
        val params = request.params

        val user = internalUsername(params.user)
        if (!session.getServerAccessManager().hasAccountAccess(user)) {
            val response = request.makeError(ReturnType.ACCESS_DENIED, "User $user not authenticated")
            return@runBlocking responseHandler.setResponseHeader(response)
        }

        val accountIO = platformGetAccountIO(AccountIO.Type.SERVER, session.baseDir, user)
        val loginParams = accountIO.readLoginData()
        val userDataConfig = try {
            accountIO.readStorageConfig()
        } catch (e: Exception) {
            null
        }

        val response = request.makeResponse(GetServerConfigJob.RPCResult(loginParams, userDataConfig))
                .stringify(GetServerConfigJob.RPCResult.serializer())
        responseHandler.setResponseHeader(response)
    }
}