package org.fejoa.server

import kotlinx.coroutines.runBlocking
import kotlinx.serialization.internal.StringSerializer
import org.fejoa.command.InQueue
import org.fejoa.network.CommandJob
import org.fejoa.network.JsonRPCRequest
import org.fejoa.network.ReturnType
import org.fejoa.network.makeError
import org.fejoa.repository.Repository
import java.io.InputStream

class CommandHandler : JsonRequestHandler(CommandJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) = runBlocking {
        val request = JsonRPCRequest.parse(CommandJob.Params.serializer(), json)
        val params = request.params

        if (data == null) {
            val response = request.makeError(ReturnType.DATA_STREAM_MISSING_REQUEST, "No command data")
            return@runBlocking responseHandler.setResponseHeader(response)
        }

        val command = CommandJob.CommandData.read(data.readAll())

        val user = internalUsername(params.user)
        val context = session.getContext(user)
        val response = UserThreadContext.withLock(user) {
            val config = context.accountIO.readStorageConfig()
            val inQueueDir = context.getStorage(config.inQueue, null, false)
            if ((inQueueDir.getBackingDatabase() as Repository).branchBackend.getBranchLog().getHead() == null)
                return@withLock request.makeError(ReturnType.ERROR, "Inqueue not found")

            val inQueue = InQueue(inQueueDir)
            inQueue.add(System.nanoTime(), command.command, command.receiverKey)

            request.makeResponse("Command received").stringify(StringSerializer)
        }

        responseHandler.setResponseHeader(response)
    }
}
