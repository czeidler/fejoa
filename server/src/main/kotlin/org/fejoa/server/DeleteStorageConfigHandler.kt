package org.fejoa.server

import kotlinx.coroutines.runBlocking
import org.fejoa.AccountIO
import org.fejoa.network.*
import org.fejoa.platformGetAccountIO
import java.io.InputStream


class DeleteStorageConfigHandler : JsonRequestHandler(DeleteStorageConfigJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) = runBlocking {

        val request = JsonRPCRequest.parse(
                DeleteStorageConfigJob.RPCParams.serializer(), json)
        val params = request.params

        val user = internalUsername(params.user)
        if (!session.getServerAccessManager().hasAccountAccess(user)) {
            val response = request.makeError(ReturnType.ACCESS_DENIED, "User $user not authenticated")
            return@runBlocking responseHandler.setResponseHeader(response)
        }

        val accountIO = platformGetAccountIO(AccountIO.Type.SERVER, session.baseDir, user)
        accountIO.deleteStorageConfig()

        val response = request.makeResponse(DeleteStorageConfigJob.RPCResult())
                .stringify(DeleteStorageConfigJob.RPCResult.serializer())
        responseHandler.setResponseHeader(response)
    }
}