package org.fejoa.repository

import org.fejoa.support.Random
import org.fejoa.chunkcontainer.*
import org.fejoa.crypto.CryptoHelper
import org.fejoa.crypto.CryptoSettings
import org.fejoa.crypto.SecretKeyData
import org.fejoa.storage.*
import org.fejoa.support.*
import org.fejoa.test.testAsync
import kotlin.test.*


class RepositoryTest : RepositoryTestBase() {

    val simpleCommitSignature = object : CommitSignature {
        override suspend fun signMessage(message: ByteArray, rootHashValue: HashValue, parents: Collection<HashValue>): ByteArray {
            return message
        }

        override suspend fun verifySignedMessage(signedMessage: ByteArray, rootHashValue: HashValue, parents: Collection<HashValue>): Boolean {
            return true
        }
    }

    @Test
    fun testEmptyRepository() = testAsync({ setUp() }, { tearDown() }) {
        val dirName = "testEmptyDir"
        val branch = "testEmpty"

        val storage = prepareStorage(dirName, branch)
        val repoConfig = getRepoConfig()
        repoConfig.boxSpec.encInfo = BoxSpec.EncryptionInfo(BoxSpec.EncryptionInfo.Type.PLAIN)
        val repository = Repository.create(branch, storage, repoConfig, null, true)

        // check that there is an initial value in the log
        val entry = storage.getBranchLog().getHead()!!
        val headRef = repository.branchLogIO.readFromLog(entry.message.data)
        assertTrue { headRef.head.hash.value.isZero }

        // test opening a repository without any commit
        val ref = repository.getRepositoryRef()
        val headCommit = Repository.open(branch, ref, storage, null).getHeadCommit()

        assertTrue { headCommit == null }
    }

    @Test
    fun testEmptyRepository2() = testAsync({ setUp() }, { tearDown() }) {
        val dirName = "testEmptyDir2"
        val branch = "testEmpty2"

        val storage = prepareStorage(dirName, branch)
        val repoConfig = getRepoConfig()
        repoConfig.boxSpec.encInfo = BoxSpec.EncryptionInfo(BoxSpec.EncryptionInfo.Type.PLAIN)
        val repository = Repository.create(branch, storage, repoConfig, null, false)

        // check that there is no initial value in the log
        val entry = storage.getBranchLog().getHead()
        assertTrue {entry == null }

        // test opening an uninitialized repository
        val ref = repository.getRepositoryRef()
        val headCommit = Repository.open(branch, ref, storage, null).getHeadCommit()
        assertTrue { headCommit == null }
    }

    @Test
    fun testEncrypted() = testAsync({ setUp() }, { tearDown() }) {
        val dirName = "testEncryptedDir"
        val branch = "testEnc"

        val storage = prepareStorage(dirName, branch)
        val crypto = CryptoHelper.crypto
        val settings = CryptoSettings.default
        val secretKey = crypto.generateSymmetricKey(settings.symmetric)
        val rawAccessor = storage.getChunkStorage().startTransaction().toChunkAccessor()
        val accessor = rawAccessor.encrypted(crypto, secretKey, settings.symmetric.algo)

        val testData = ByteArray(1024 * 1000)
        Random().read(testData)

        val specSeed = ByteArray(20)
        val config = ContainerSpec(HashSpec.createCyclicPoly(HashSpec.HashType.FEJOA_CYCLIC_POLY_2KB_8KB, specSeed),
                BoxSpec())
        var container = ChunkContainer.create(accessor, config)
        val outStream = ChunkContainerOutStream(container)
        outStream.write(testData)
        outStream.close()

        container = ChunkContainer.read(accessor, container.ref)
        assertTrue(testData contentEquals ChunkContainerInStream(container).readAll())
    }

    @Test
    fun testBasics() = testAsync({ setUp() }, { tearDown() }) {
        val dirName = "testBasics"
        val branch = "basicBranch"

        val storage = prepareStorage(dirName, branch)
        val repoConfig = getRepoConfig()
        repoConfig.hashSpec.setFixedSizeChunking(500)

        var repository = Repository.create(branch, storage, repoConfig,
                SecretKeyData(secretKey!!, settings.symmetric.algo), true)

        repository.putBytes("test", "test".toUTF())
        assertEquals("test", repository.readBytes("test").toUTFString())

        repository.commit(ByteArray(0), simpleCommitSignature)
        assertEquals("test", repository.readBytes("test").toUTFString())

        repository = Repository.open(branch, repository.getRepositoryRef(), storage, repository.crypto)
        assertEquals("test", repository.readBytes("test").toUTFString())
    }

    @Test
    fun testRepositoryBasics() = testAsync({ setUp() }, { tearDown() }) {
        val dirName = "testRepositoryBasicsDir"
        val branch = "basicBranch"

        var repository = createRepo(dirName, branch)
        val storage = repository.branchBackend
        val symCredentials = SecretKeyData(secretKey!!, settings.symmetric.algo)

        val content = HashMap<String, DatabaseStringEntry>()
        add(repository, content, DatabaseStringEntry("file1", "file1"))
        add(repository, content, DatabaseStringEntry("dir1/file2", "file2"))
        add(repository, content, DatabaseStringEntry("dir1/file3", "file3"))
        add(repository, content, DatabaseStringEntry("dir2/file4", "file4"))
        add(repository, content, DatabaseStringEntry("dir1/sub1/file5", "file5"))
        add(repository, content, DatabaseStringEntry("dir1/sub1/sub2/file6", "file6"))

        repository.commit(ByteArray(0), simpleCommitSignature)
        containsContent(repository, content)
        val tip = repository.getHead()!!
        assertEquals(tip, repository.getHeadCommit()!!.hash)

        repository = Repository.open(branch, repository.getRepositoryRef(), storage, symCredentials)
        containsContent(repository, content)

        // test add to existing dir
        add(repository, content, DatabaseStringEntry("dir1/file6", "file6"))
        repository.commit(ByteArray(0), simpleCommitSignature)
        // do various head commit tests:
        val parentOfCommit2 = repository.getHeadCommit()!!.parents.first()
        assertEquals(tip, parentOfCommit2.hash)
        val logEntry = repository.branchLogIO.readFromLog(repository.log.getHead()!!.message.data)
        val commit2Head = repository.getHeadCommit()!!
        assertEquals(logEntry.head.hash, commit2Head.hash)
        val commit2FromCache = repository.commitCache.getCommit(ObjectRef(commit2Head.hash, commit2Head.ref!!))
        assertEquals(commit2FromCache.parents.first().hash, parentOfCommit2.hash)
        repository = Repository.open(branch, repository.getRepositoryRef(), storage, symCredentials)
        containsContent(repository, content)

        // test update
        add(repository, content, DatabaseStringEntry("dir1/file3", "file3Update"))
        add(repository, content, DatabaseStringEntry("dir1/sub1/file5", "file5Update"))
        add(repository, content, DatabaseStringEntry("dir1/sub1/sub2/file6", "file6Update"))
        repository.commit(ByteArray(0), simpleCommitSignature)
        repository = Repository.open(branch, repository.getRepositoryRef(), storage, symCredentials)
        containsContent(repository, content)

        // test remove
        remove(repository, content, "dir1/sub1/file5")
        repository.commit(ByteArray(0), simpleCommitSignature)
        repository = Repository.open(branch, repository.getRepositoryRef(), storage, symCredentials)
        containsContent(repository, content)

        assertEquals(0, repository.listFiles("notThere").size)
        assertEquals(0, repository.listDirectories("notThere").size)
        assertEquals(0, repository.listFiles("file1").size)
        assertEquals(0, repository.listDirectories("file1").size)
        assertEquals(0, repository.listDirectories("dir1/file2").size)
    }

    @Test
    fun testRepositoryOpenModes() = testAsync({ setUp() }, { tearDown() }) {
        val repository = createRepo("RepoOpenModeTest", "repoBranch")

        var randomDataAccess: RandomDataAccess? = null
        try {
            randomDataAccess = repository.open("test", RandomDataAccess.READ)
        } catch (e: IOException) {

        }
        // file does not exist
        assertNull(randomDataAccess)

        randomDataAccess = repository.open("test", RandomDataAccess.WRITE)
        randomDataAccess.write("Hello World".toUTF())
        randomDataAccess.close()
        assertEquals("Hello World", repository.readBytes("test").toUTFString())

        randomDataAccess = repository.open("test", RandomDataAccess.APPEND)
        randomDataAccess.write("!".toUTF())
        randomDataAccess.close()
        assertEquals("Hello World!", repository.readBytes("test").toUTFString())

        randomDataAccess = repository.open("test", RandomDataAccess.WRITE)
        randomDataAccess.seek(6)
        randomDataAccess.write("there".toUTF())
        randomDataAccess.close()
        assertEquals("Hello there!",  repository.readBytes("test").toUTFString())

        randomDataAccess = repository.open("test", RandomDataAccess.INSERT)
        randomDataAccess.seek(5)
        randomDataAccess.write(" you".toUTF())
        randomDataAccess.close()
        assertEquals("Hello you there!",  repository.readBytes("test").toUTFString())

        randomDataAccess = repository.open("test", RandomDataAccess.TRUNCATE)
        randomDataAccess.write("New string!".toUTF())
        randomDataAccess.close()
        assertEquals("New string!", repository.readBytes("test").toUTFString())

        // delete
        randomDataAccess = repository.open("test", RandomDataAccess.WRITE)
        randomDataAccess.delete(4, 2)
        randomDataAccess.close()
        assertEquals("New ring!", repository.readBytes("test").toUTFString())

        // try to write in read mode
        randomDataAccess = repository.open("test", RandomDataAccess.READ)
        var failed = false
        try {
            randomDataAccess.write("Hello World".toUTF())
        } catch (e: IOException) {
            failed = true
        }
        randomDataAccess.close()
        assertTrue(failed)

        // try to read in write mode
        randomDataAccess = repository.open("test", RandomDataAccess.WRITE)
        failed = false
        try {
            val buffer = ByteArray(2)
            randomDataAccess.read(buffer)
        } catch (e: IOException) {
            failed = true
        }
        randomDataAccess.close()
        assertTrue(failed)
    }

    @Test
    fun testNewFile() = testAsync({ setUp() }, { tearDown() }) {
        val repository = createRepo("RepoTestNewFile", "repoBranch")

        var randomDataAccess: RandomDataAccess? = null
        try {
            randomDataAccess = repository.open("test", RandomDataAccess.READ)
        } catch (e: IOException) {

        }
        // file does not exist
        assertNull(randomDataAccess)

        randomDataAccess = repository.open("test", RandomDataAccess.WRITE)
        assertEquals(1, repository.listFiles("").size)
        randomDataAccess.close()
        assertEquals("", repository.readBytes("test").toUTFString())
    }

    @Test
    fun testDirs() = testAsync({ setUp() }, { tearDown() }) {
        val repository = createRepo("RepoTestDirs", "repoBranch")

        repository.mkDir("dir")
        assertEquals(1, repository.listDirectories("").size)
        assertEquals(0, repository.listDirectories("dir").size)

        var randomDataAccess: RandomDataAccess? = null
        try {
            randomDataAccess = repository.open("dir/test", RandomDataAccess.READ)
        } catch (e: IOException) {

        }
        // file does not exist
        assertNull(randomDataAccess)

        randomDataAccess = repository.open("dir/sub/test", RandomDataAccess.WRITE)
        assertEquals(1, repository.listFiles("dir/sub").size)
        randomDataAccess.close()
        assertEquals("", repository.readBytes("dir/sub/test").toUTFString())

        repository.remove("dir/sub/test")
        assertEquals(0, repository.listDirectories("dir/sub").size)
        assertEquals(1, repository.listDirectories("dir").size)
        repository.remove("dir/sub")
        assertEquals(0, repository.listDirectories("dir").size)
        repository.remove("dir/")
        assertEquals(0, repository.listDirectories("").size)
    }

    @Test
    fun testFSAttribute() = testAsync({ setUp() }, { tearDown() }) {
        var repository = createRepo("testFSAttrbute", "repoBranch")
        repository.maintainFSAttr = true

        var randomDataAccess = repository.open("test", RandomDataAccess.WRITE)
        randomDataAccess.write("test".toUTF())
        randomDataAccess.close()

        val attr1T = (repository.getAttribute("test", AttrType.FS_ATTR) as? FSAttribute
                ?: fail("Missing attr")).mTime
        repository.commit("commit".toUTF(), null)

        repository = openRepo("testFSAttrbute", "repoBranch", repository.getRepositoryRef())
        repository.maintainFSAttr = true
        val attr2 = repository.getAttribute("test", AttrType.FS_ATTR) as? FSAttribute
                ?: fail("Missing attr")

        assertEquals(attr1T, attr2.mTime)

        repository.mkDir("dir/subDir")
        assertTrue((repository.getAttribute("dir/subDir", AttrType.FS_ATTR) as? FSAttribute ?: fail("Missing attr")).mTime != 0L)
        val dirAttr0T = (repository.getAttribute("dir", AttrType.FS_ATTR) as? FSAttribute
                ?: fail("Missing attr")).mTime
        assertTrue(dirAttr0T != 0L)

        randomDataAccess = repository.open("dir/test2", RandomDataAccess.WRITE)
        randomDataAccess.write("test2".toUTF())
        randomDataAccess.close()
        val dirAttr1 = repository.getAttribute("dir", AttrType.FS_ATTR) as? FSAttribute
                ?: fail("Missing attr")
        val dirFileAttr = repository.getAttribute("dir/test2", AttrType.FS_ATTR) as? FSAttribute
                ?: fail("Missing attr")
        assertTrue(dirAttr1.mTime != 0L)
        assertTrue(dirFileAttr.mTime != 0L)
        assertNotEquals(dirAttr0T, dirAttr1.mTime)

        randomDataAccess = repository.open("dir/subDir/test2", RandomDataAccess.WRITE)
        randomDataAccess.write("test2".toUTF())
        randomDataAccess.close()

        val dirSubTest2Attr0T = (repository.getAttribute("dir/subDir/test2", AttrType.FS_ATTR) as? FSAttribute
                ?: fail("Missing attr")).mTime
        val dirSubAttr0T = (repository.getAttribute("dir/subDir", AttrType.FS_ATTR) as? FSAttribute
                ?: fail("Missing attr")).mTime
        assertTrue(dirSubAttr0T > dirAttr1.mTime)
        assertEquals(dirAttr1.mTime,
                (repository.getAttribute("dir", AttrType.FS_ATTR) as? FSAttribute ?: fail("Missing attr")).mTime)

        repository.commit("commit 2".toUTF(), null)
        repository = openRepo("testFSAttrbute", "repoBranch", repository.getRepositoryRef())

        assertEquals(dirSubAttr0T, (repository.getAttribute("dir/subDir", AttrType.FS_ATTR) as? FSAttribute
                ?: fail("Missing attr")).mTime)
        assertEquals(dirSubTest2Attr0T, (repository.getAttribute("dir/subDir/test2", AttrType.FS_ATTR) as? FSAttribute
                ?: fail("Missing attr")).mTime)
    }
}