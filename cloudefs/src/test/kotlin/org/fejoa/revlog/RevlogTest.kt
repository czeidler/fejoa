package org.fejoa.revlog

import org.fejoa.chunkcontainer.BoxSpec
import org.fejoa.chunkcontainer.ChunkContainerTestBase
import org.fejoa.chunkcontainer.ContainerSpec
import org.fejoa.storage.HashSpec
import org.fejoa.support.toUTF
import org.fejoa.test.testAsync
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue


class RevlogTest : ChunkContainerTestBase() {
    @Test
    fun testBasics() = testAsync({ setUp() }, { tearDown() }) {
        val dirName = "testRevlogContainerDir"
        val name = "test"
        val config = ContainerSpec(HashSpec(HashSpec.HashType.FEJOA_FIXED_8K, null), BoxSpec())
        config.hashSpec.setFixedSizeChunking(180)
        var chunkContainer = prepareContainer(dirName, name, config)

        val revlog = Revlog(chunkContainer)
        var pos = revlog.add("Some initial data".toUTF(), -1)
        pos = revlog.add("Some initial data and some changes".toUTF(), pos)
        pos = revlog.add("Some initial data and some changes, more changes".toUTF(), pos)
        pos = revlog.add("Some older data and some changes, more changes".toUTF(), pos)
        val inventory = revlog.inventory()
        assertEquals(4, inventory.size)
        assertTrue(revlog.get(pos) contentEquals "Some older data and some changes, more changes".toUTF())
    }
}