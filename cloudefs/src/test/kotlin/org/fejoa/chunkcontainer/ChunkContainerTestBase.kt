package org.fejoa.chunkcontainer

import org.fejoa.crypto.*
import org.fejoa.repository.*
import org.fejoa.storage.*
import org.fejoa.support.*
import org.fejoa.test.testAsync

import kotlin.test.BeforeTest
import kotlin.test.AfterTest


open class ChunkContainerTestBase {
    protected val cleanUpList: MutableList<String> = ArrayList()
    protected var settings = CryptoSettings.default
    protected var secretKey: SecretKey? = null
    protected var storageBackend: StorageBackend? = null

    suspend fun setUp() {
        secretKey = CryptoHelper.crypto.generateSymmetricKey(settings.symmetric)
        storageBackend = platformCreateStorage("")
    }

    suspend fun tearDown() {
        for (namespace in cleanUpList)
            storageBackend!!.deleteNamespace(namespace)
    }

    protected suspend fun prepareStorage(dirName: String, name: String): StorageBackend.BranchBackend {
        return storageBackend?.let {
            val branchBackend = if (it.exists(dirName, name))
                it.open(dirName, name)
            else {
                it.create(dirName, name)
            }
            cleanUpList.add(dirName)
            return@let branchBackend
        } ?: throw Exception("storageBackend should not be null")
    }

    private fun ChunkStorage.prepare(zipType: BoxSpec.ZipType): ChunkAccessor {
        // first compressed then encrypted
        return this.startTransaction().toChunkAccessor()
                .encrypted(CryptoHelper.crypto, secretKey!!, settings.symmetric.algo).compressed(zipType)
    }

    protected fun getRepoConfig(): RepositoryConfig {
        val seed = ByteArray(10) // just some zeros
        val hashSpec = HashSpec.createCyclicPoly(HashSpec.HashType.FEJOA_CYCLIC_POLY_2KB_8KB, seed)

        val boxSpec = BoxSpec(
                encInfo = BoxSpec.EncryptionInfo(BoxSpec.EncryptionInfo.Type.PARENT),
                zipType = BoxSpec.ZipType.ZSTANDARD,
                zipBeforeEnc = true
        )

        return RepositoryConfig(
                hashSpec = hashSpec,
                boxSpec = boxSpec
        )
    }

    protected fun ChunkStorage.prepareAccessors(): ChunkAccessors {
        return RepoChunkAccessors(startTransaction(), getRepoConfig(),
                SecretKeyData(secretKey!!, settings.symmetric.algo))
    }

    protected fun prepareContainer(storage: StorageBackend.BranchBackend, config: ContainerSpec)
            : ChunkContainer {
        val accessor = storage.getChunkStorage().prepare(config.boxSpec.zipType)
        return ChunkContainer.create(accessor, config)
    }

    protected suspend fun prepareContainer(dirName: String, name: String, config: ContainerSpec): ChunkContainer {
        val accessor = prepareStorage(dirName, name).getChunkStorage().prepare(config.boxSpec.zipType)
        return ChunkContainer.create(accessor, config)
    }

    protected suspend fun openContainer(dirName: String, name: String, pointer: ChunkContainerRef): ChunkContainer {
        val accessor = prepareStorage(dirName, name).getChunkStorage().prepare(pointer.boxSpec.zipType)
        return ChunkContainer.read(accessor, pointer)
    }

    protected suspend fun toString(inStream: AsyncInStream): String {
        return inStream.readAll().toUTFString()
    }

    protected suspend fun printStream(inStream: AsyncInStream) {
        println(toString(inStream))
    }
}