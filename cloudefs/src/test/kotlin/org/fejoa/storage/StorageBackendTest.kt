package org.fejoa.storage

import org.fejoa.test.testAsync
import kotlin.test.*


class StorageBackendTest {
    protected val cleanUpList: MutableList<String> = ArrayList()
    protected var storageBackend: StorageBackend? = null

    fun setUp() {
        storageBackend = platformCreateStorage("testContext")
    }

    suspend fun cleanUp() {
        for (namespace in cleanUpList)
            storageBackend!!.deleteNamespace(namespace)
    }


    suspend fun assertFails(block: suspend () -> Unit): Throwable {
        try {
            block()
        } catch (e: Throwable) {
            assertTrue(true)
            return e
        }
        asserter.fail("Exception expected")
    }


    @Test
    fun testStorageBackendBasics() = testAsync({setUp()}, {cleanUp()}) {
        val storage = storageBackend!!
        val namespace = "storagebackendns"
        val branch = "storagebackendbranch"
        val branch2 = "storagebackendbranch2"
        cleanUpList += namespace

        storage.create(namespace, branch)
        assertTrue(storage.exists(namespace, branch))
        storage.create(namespace, branch2)
        assertTrue(storage.exists(namespace, branch2))
        val branchBackend2 = storage.open(namespace, branch2)
        assertNull(branchBackend2.getBranchLog().getHead())

        assertEquals(2, storage.listBranches(namespace).size)
        storage.delete(namespace, branch)
        assertEquals(1, storage.listBranches(namespace).size)
        storage.delete(namespace, branch2)
        assertEquals(0, storage.listBranches(namespace).size)

        assertFails({storage.open(namespace, branch2)})

        // test that open doesn't create the branch
        assertFalse (storage.exists(namespace, branch2))

        assertEquals(0, storage.listBranches(namespace).size)
        // test that listBranches doesn't create any branches
        assertEquals(0, storage.listBranches(namespace).size)
    }

    @Test
    fun testListNamespaces() = testAsync({setUp()}, {cleanUp()}) {
        val storage = storageBackend!!
        val namespace1 = "ns1"
        val namespace2 = "namespace2"
        cleanUpList += namespace1
        cleanUpList += namespace2

        storage.create(namespace1, "branch1")
        var namespaces = storage.listNamespaces()
        assertTrue(namespaces.size == 1)
        assertEquals(namespace1, namespaces.first())

        storage.create(namespace1, "branch2")
        namespaces = storage.listNamespaces()
        assertTrue(namespaces.size == 1)
        assertEquals(namespace1, namespaces.first())

        storage.create(namespace2, "branch1")
        namespaces = storage.listNamespaces()
        assertTrue(namespaces.size == 2)
        assertEquals(listOf(namespace1, namespace2).sorted(), namespaces.sorted())

        storage.create(namespace2, "branch2")
        namespaces = storage.listNamespaces()
        assertTrue(namespaces.size == 2)
        assertEquals(listOf(namespace1, namespace2).sorted(), namespaces.sorted())
    }
}
