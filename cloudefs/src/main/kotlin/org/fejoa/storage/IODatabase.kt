package org.fejoa.storage

import org.fejoa.repository.AttrType
import org.fejoa.repository.Attribute


interface IODatabase {
    enum class FileType {
        FILE,
        DIRECTORY,
        NOT_EXISTING
    }

    suspend fun getHash(path: String): HashValue
    suspend fun probe(path: String): FileType
    suspend fun open(path: String, mode: Mode): RandomDataAccess
    suspend fun remove(path: String)

    suspend fun listFiles(path: String): Collection<String>
    suspend fun listDirectories(path: String): Collection<String>

    suspend fun readBytes(path: String): ByteArray
    suspend fun putBytes(path: String, data: ByteArray)

    suspend fun mkDir(path: String)

    suspend fun setAttribute(path: String, attribute: Attribute)
    suspend fun getAttribute(path: String, attrType: AttrType): Attribute?
}
