package org.fejoa.storage


/**
 * The main reason for this class is the chunk container usage in revlogs. To load a chunk container from a revlog
 * multiple write operations to the chunk container needs to be applied, i.e. to construct the container from the diffs.
 * This results in garbage chunks. To avoid these chunks the WriteCacheChunkAccessor caches these chunks and if the
 * chunk container is never flushed these chunks are never written to the chunk store.
 * 
 * TODO: find a more elegant solution
 */
/*
class WriteCacheChunkAccessor(val parent: ChunkAccessor, var maxSize: Int = 1024 * 1024 * 1,
                              var maxSizeAfterFlush: Int = 1024 * 512) : ChunkAccessor {
    private var cacheSize: Int = 0
    private val writeCache: MutableMap<HashValue, ByteArray> = HashMap()
    var lock = Mutex()

    override suspend fun getBoxChunk(key: HashValue): ByteArray = lock.withLock {
        val cachedValue = writeCache[key]
        if (cachedValue != null)
            return cachedValue
        return parent.getBoxChunk(key)
    }

    override fun putBoxChunk(key: HashValue, rawData: ByteArray): Future<Boolean> = lock.withLock  {
        writeCache[key] = rawData
        cacheSize += rawData.size
        if (cacheSize > maxSize)
            flush(maxSizeAfterFlush)
    }

    override suspend fun unboxChunk(boxData: ByteArray, ref: ChunkRef): ByteArray {
        return parent.unboxChunk(boxData, ref)
    }

    override suspend fun boxChunk(data: ByteArray, ivHash: HashValue): Pair<HashValue, ByteArray> {
        return parent.boxChunk(data, ivHash)
    }

    private suspend fun flush(maxSizeAfterFlush: Int) {
        val copy: MutableMap<HashValue, ByteArray> = HashMap(writeCache)
        for (entry in copy) {
            val key = entry.key
            val value = entry.value
            parent.putBoxChunk(key, value)

            writeCache.remove(key)
            cacheSize -= value.size
            if (cacheSize <= maxSizeAfterFlush)
                break
        }
    }

    override suspend fun flush() = lock.withLock  {
        flush(0)
        parent.flush()
    }

    override suspend fun releaseChunk(data: HashValue) {
        return parent.releaseChunk(data)
    }
}
*/