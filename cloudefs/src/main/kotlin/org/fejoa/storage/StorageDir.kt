package org.fejoa.storage

import org.fejoa.repository.AttrType
import org.fejoa.repository.Attribute
import org.fejoa.repository.Repository
import org.fejoa.support.*


class StorageDir : IOStorageDir {
    constructor(storageDir: StorageDir) : this(storageDir, storageDir.baseDir, true)

    constructor(storageDir: StorageDir, path: String, absoluteBaseDir: Boolean = false)
            : super(storageDir, path, absoluteBaseDir) {
        parent = storageDir
        storageDir.childDirs.add(this)
    }

    constructor(database: Database, baseDir: String, commitSignature: CommitSignature?,
                looper: SuspendQueue)
            : super(StorageDirCache(database, commitSignature, looper), baseDir)

    private val storageDirCache: StorageDirCache
        get() = database as StorageDirCache

    fun getBackingDatabase(): Database {
        return storageDirCache.database
    }

    fun setAllowCommits(allow: Boolean) {
        (getBackingDatabase() as Repository).allowCommit = allow
    }

    suspend fun getHead(): Hash? {
        return storageDirCache.getHead()
    }

    val branch: String
        get() = storageDirCache.getBranch()

    fun setCommitSignature(commitSignature: CommitSignature?) {
        storageDirCache.commitSignature = commitSignature
    }

    fun getCommitSignature(): CommitSignature? {
        return storageDirCache.commitSignature
    }

    interface Listener {
        fun callSynchronous(): Boolean {
            return false
        }

        suspend fun onBeforeCommit(tip: HashValue) {}
        suspend fun onAfterCommit(base: HashValue, newTip: HashValue) {}
        /**
         * Called when the storage dir is closed
         */
        suspend fun onClose() {}
    }

    // the directory this directory is derived from
    private var parent: StorageDir? = null
    // dirs derived from this directory
    private val childDirs: MutableList<StorageDir> = ArrayList()
    // listeners associated with this StorageDir
    private val localListeners : MutableList<Listener> = ArrayList()

    fun addListener(listener: Listener) {
        localListeners.add(listener)
        storageDirCache.addListener(listener)
    }

    fun removeListener(listener: Listener) {
        localListeners.remove(listener)
        storageDirCache.removeListener(listener)
    }

    private suspend fun notifyClose() {
        localListeners.forEach {
            if (it.callSynchronous())
                it.onClose()
            else {
                storageDirCache.looper.schedule {
                    it.onClose()
                }
            }
        }
    }

    suspend fun close() {
        notifyClose()

        localListeners.forEach {
            storageDirCache.removeListener(it)
        }
        localListeners.clear()

        childDirs.forEach { it.close() }
        val that = this
        parent?.also { it.childDirs.remove(that) }
    }

    /**
     * The StorageDirCache is shared between all StorageDir that are build from the same parent.
     */
    private class StorageDirCache(val database: Database, var commitSignature: CommitSignature?,
                                  //val looper: SuspendQueue) : Database by database {
                                  // TODO doesn't work because of a JS compiler bug (KT-23094)
                                  val looper: SuspendQueue) : Database {
        private val listeners: MutableList<Listener> = ArrayList()

        private suspend fun notifyAfterCommit(base: HashValue, newTip: HashValue) {
            for (listener in ArrayList(listeners)) {
                if (listener.callSynchronous()) {
                    listener.onAfterCommit(base, newTip)
                } else {
                    looper.schedule {
                        listener.onAfterCommit(base, newTip)
                    }
                }
            }
        }

        override suspend fun open(path: String, mode: Mode): RandomDataAccess {
            return database.open(path, mode)
        }

        override fun getBranch(): String {
            return database.getBranch()
        }

        override suspend fun getHead(): Hash? {
            return database.getHead()
        }

        override suspend fun commit(message: ByteArray, signature: CommitSignature?): Hash {
            return database.commit(message, signature)
        }

        override suspend fun merge(mergeParents: Collection<Database>, mergeStrategy: MergeStrategy): Database.MergeResult {
            return database.merge(mergeParents, mergeStrategy)
        }

        override suspend fun getDiff(baseCommit: HashValue, endCommit: HashValue): DatabaseDiff {
            return database.getDiff(baseCommit, endCommit)
        }

        override suspend fun getHash(path: String): HashValue {
            return database.getHash(path)
        }

        override suspend fun probe(path: String): IODatabase.FileType {
            return database.probe(path)
        }

        override suspend fun remove(path: String) {
            return database.remove(path)
        }

        override suspend fun listFiles(path: String): Collection<String> {
            return database.listFiles(path)
        }

        override suspend fun listDirectories(path: String): Collection<String> {
            return database.listDirectories(path)
        }

        override suspend fun readBytes(path: String): ByteArray {
            return database.readBytes(path)
        }

        override suspend fun putBytes(path: String, data: ByteArray) {
            return database.putBytes(path, data)
        }

        override suspend fun mkDir(path: String) {
            database.mkDir(path)
        }

        override suspend fun getAttribute(path: String, attrType: AttrType): Attribute? {
            return database.getAttribute(path, attrType)
        }

        override suspend fun setAttribute(path: String, attribute: Attribute) {
            database.setAttribute(path, attribute)
        }

        private suspend fun notifyBeforeCommit(tip: HashValue) {
            for (listener in listeners) {
                if (listener.callSynchronous()) {
                    listener.onBeforeCommit(tip)
                } else {
                    looper.schedule {
                        listener.onBeforeCommit(tip)
                    }
                }
            }
        }

        fun addListener(listener: Listener) {
            listeners.add(listener)
        }

        fun removeListener(listener: Listener) {
            listeners.remove(listener)
        }

        suspend fun commit(message: ByteArray) {
            val base = getHead()?.value ?: Config.newDataHash()
            notifyBeforeCommit(base)

            commit(message, commitSignature)

            val tip = getHead() ?: throw Exception("Internal error")
            notifyAfterCommit(base, tip.value)
        }

        suspend fun onTipUpdated(old: HashValue, newTip: HashValue) {
            notifyAfterCommit(old, newTip)
        }
    }

    suspend fun commit(message: ByteArray = "Client commit".toUTF()) {
        storageDirCache.commit(message)
    }

    suspend fun getDiff(baseCommit: HashValue, endCommit: HashValue): DatabaseDiff {
        return storageDirCache.getDiff(baseCommit, endCommit)
    }

    suspend fun onTipUpdated(old: HashValue, newTip: HashValue) {
        storageDirCache.onTipUpdated(old, newTip)
    }

    suspend fun printContent(dataPrinter: (data: ByteArray) -> String = { it.toUTFString() }): String {
        return (getBackingDatabase() as Repository).printContent("", 0, dataPrinter)
    }

    suspend fun printHistory(): String {
        return (getBackingDatabase() as Repository).printHistory()
    }
}
