package org.fejoa.storage

import kotlinx.serialization.Serializable


interface StorageBackend {
    interface BranchBackend {
        fun getChunkStorage(): ChunkStorage
        fun getBranchLog(): BranchLog
    }

    /**
     * @param size the size of the branch storage
     */
    @Serializable
    class Stats(val size: Long)

    /**
     * Open a ChunkStorage
     *
     * @param branch ChunkStorage branch name
     * @param namespace in which the branch exists
     */
    suspend fun open(namespace: String, branch: String): BranchBackend
    suspend fun create(namespace: String, branch: String): BranchBackend
    suspend fun branchStats(namespace: String, branch: String): Stats

    /**
     * @return true if the database exists (this does not mean that the Repository is initialized)
     */
    suspend fun exists(namespace: String, branch: String): Boolean
    suspend fun delete(namespace: String, branch: String)
    suspend fun deleteNamespace(namespace: String)

    suspend fun listBranches(namespace: String): Collection<BranchLog>
    suspend fun listNamespaces(): Collection<String>
}
