package org.fejoa.repository

import org.fejoa.storage.Hash
import org.fejoa.support.IOException


class TreeAccessor(val root: Directory) {
    // indicates if
    private var isModified = false

    var dirChangeListener: ((changedEntry: Pair<Directory, String>) -> Any)? = null

    private fun checkPath(path: String): String {
        var p = path
        while (p.startsWith("/"))
            p = p.substring(1)
        return p
    }

    operator fun get(path: String): DirectoryEntry? {
        var p = path
        p = checkPath(p)
        val parts = p.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (parts.isEmpty())
            return root
        val entryName = parts[parts.size - 1]
        val currentDir = get(parts, parts.size - 1, false) ?: return null
        return if (entryName == "") currentDir else currentDir.getEntry(entryName)
    }

    /**
     * @param parts List of directories
     * @param nDirs Number of dirs in parts that should be follow
     * @return null or an entry pointing to the request directory, the object is loaded
     */
    operator fun get(parts: Array<String>, nDirs: Int, invalidateTouchedDirs: Boolean): Directory? {
        var currentDir: Directory = root
        if (invalidateTouchedDirs)
            currentDir.markModified()
        for (i in 0 until nDirs) {
            val subDir = parts[i]
            val entry = currentDir.getEntry(subDir) ?: return null
            if (entry.isFile())
                return null

            if (invalidateTouchedDirs)
                entry.markModified()

            currentDir = entry as Directory
        }
        return currentDir
    }

    suspend fun putBlob(path: String, hash: Hash, newRef: Ref?): BlobEntry {
        val result = prepareInsertDirectory(path)
        val parent = result.first
        val fileName = result.second
        val blob = parent.getEntry(fileName)?.let {
            if (!it.isFile())
                throw Exception("Directory at insert path")
            val b = it as BlobEntry
            BlobEntry(it.nameAttrData, hash, newRef ?: b.onDiskRef, newRef == null)
        } ?: run {
            BlobEntry(fileName, hash, newRef, false)
        }
        this.isModified = true
        parent.put(blob)
        return blob
    }

    suspend fun putDir(path: String): Directory {
        val result = prepareInsertDirectory(path)
        val parent = result.first
        val dirName = result.second
        val existing = parent.getEntry(dirName)
        if (existing != null && !existing.isDir())
            throw Exception("File at insert path")
        val directory = Directory(dirName, root.getHash().spec)
        this.isModified = true
        parent.put(directory)
        return directory
    }

    suspend fun put(path: String, entry: DirectoryEntry) {
        val result = prepareInsertDirectory(path)
        entry.name = result.second
        this.isModified = true
        result.first.put(entry)
    }

    suspend fun setAttribute(path: String, attribute: Attribute) {
        val node = prepareInsertDirectory(path, true)
        val entry = node.first.getEntry(node.second) ?: throw IOException("no entry at $path")
        when (attribute) {
            is FSAttribute -> entry.nameAttrData.setFSAttr(attribute)
            else -> throw IOException("Unsupported attribute type")
        }
    }

    fun getAttribute(path: String, attrType: AttrType): Attribute? {
        val entry = get(path) ?: throw IOException("no entry at $path")
        return when (attrType) {
            AttrType.FS_ATTR -> entry.nameAttrData.getFSAttr()
            AttrType.EXTENDED_ATTR_DIR -> TODO()
            AttrType.EXTENDED_ATTR_LOCAL -> TODO()
        }
    }

    private suspend fun prepareInsertDirectory(path: String, mustExist: Boolean = false): Pair<Directory, String> {
        var p = path
        p = checkPath(p)
        val parts = p.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val fileName = parts[parts.size - 1]
        var currentDir = root
        val touchedEntries = ArrayList<Directory>()
        touchedEntries.add(currentDir)
        for (i in 0 until parts.size - 1) {
            val subDir = parts[i]
            var currentEntry = currentDir.getEntry(subDir)
            if (currentEntry == null) {
                if (mustExist)
                    throw IOException("Entry not exist: $p")
                currentEntry = Directory(subDir, root.getHash().spec)
                currentDir.put(currentEntry)
                currentDir = currentEntry
            } else {
                if (currentEntry.isFile())
                    throw IOException("Invalid insert path: $p")
                currentDir = currentEntry as Directory
            }
            touchedEntries.add(currentEntry)
        }
        for (touched in touchedEntries)
            touched.markModified()

        val pair = currentDir to fileName
        dirChangeListener?.let { listener ->
            listener(pair)
        }
        return pair
    }

    fun remove(path: String): DirectoryEntry? {
        val p = checkPath(path)
        this.isModified = true
        val parts = p.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val entryName = parts[parts.size - 1]
        val currentDir = get(parts, parts.size - 1, true) ?: return null
        // invalidate entry
        dirChangeListener?.invoke(currentDir to p)
        return currentDir.remove(entryName)
    }

    suspend fun build(): Hash {
        isModified = false
        return root.getHash()
    }
}
