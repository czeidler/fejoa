package org.fejoa.repository

import org.fejoa.storage.Database
import org.fejoa.storage.IODatabase
import org.fejoa.storage.MergeStrategy


class ThreeWayMerge(val conflictSolver: IConflictSolver = TakeOursSolver()) : MergeStrategy {
    class TakeOursSolver : ThreeWayMerge.IConflictSolver {
        override fun solve(path: String, ours: DirectoryEntry, theirs: DirectoryEntry): DirectoryEntry {
            return ours
        }
    }

    interface IConflictSolver {
        fun solve(path: String, ours: DirectoryEntry, theirs: DirectoryEntry): DirectoryEntry
    }

    override suspend fun merge(ours: Database, theirs: Database, commonAncestor: Database) {
        if (ours !is Repository || theirs !is Repository)
            throw Exception("Unsupported database type")
        val treeAccessor = ours.ioDatabase.treeAccessor
        val treeIterator = TreeIterator(ours.ioDatabase.getRootDirectory(), theirs.ioDatabase.getRootDirectory(),
                includeAllAdded = false, includeAllRemoved = false)
        while (treeIterator.hasNext()) {
            val change = treeIterator.next()
            val ancestorType = commonAncestor.probe(change.path)
            if (change.type === DiffIterator.Type.ADDED) {
                // add if common ancestor does not has it
                if (ancestorType == IODatabase.FileType.NOT_EXISTING)
                    treeAccessor.put(change.path, change.theirs!!)
                else // it has been removed in ours
                    treeAccessor.remove(change.path)
            } else if (change.type === DiffIterator.Type.REMOVED) {
                // remove if common ancestor has it
                if (ancestorType == IODatabase.FileType.NOT_EXISTING)
                    treeAccessor.put(change.path, change.ours!!)
                else
                    treeAccessor.remove(change.path)
            } else if (change.type === DiffIterator.Type.MODIFIED) {
                if (change.ours!!.isDir())
                    continue
                treeAccessor.put(change.path, conflictSolver.solve(change.path, change.ours, change.theirs!!))
            }
        }
    }
}
