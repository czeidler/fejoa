package org.fejoa.repository

import org.fejoa.storage.HashValue
import org.fejoa.crypto.CryptoHelper


interface BranchLogIO {
    /**
     * @return an identifier to be publicly stored in the BranchLog
     */
    suspend fun logHash(repoRef: RepositoryRef): HashValue {
        // Don't store the repo head in plain text
        val hashStream = CryptoHelper.sha256Hash()
        hashStream.write(repoRef.head.hash.value.bytes)
        // TODO Think about adding a repo specific secret salt value to make the ownership of a certain commit deniable.
        // This can be done in a backward compatible manner by hashing the salt after the head value:
        //val salt = ByteArray(0)
        //hashStream.write(salt)
        return HashValue(hashStream.hash())
    }

    /**
     * Serializes the latest object index chunk container ref.
     *
     * Here is also the place to encrypt/decrypt the objectIndexConfig.
     */
    suspend fun writeToLog(repoRef: RepositoryRef): ByteArray
    suspend fun readFromLog(logEntry: ByteArray): RepositoryRef
}
