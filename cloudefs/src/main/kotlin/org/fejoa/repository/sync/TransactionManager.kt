package org.fejoa.repository.sync

import org.fejoa.storage.ChunkTransaction
import org.fejoa.support.AsyncIterator
import org.fejoa.support.Future
import org.fejoa.support.await


class TransactionManager {
    class Transaction(val id: Long, val branch: String, val accessRight: AccessRight,
                      val chunkTransaction: ChunkTransaction) {
        private val iterators: MutableMap<Long, ChunkTransaction.Iterator> = HashMap()

        fun commit(): Future<Unit> {
            return chunkTransaction.finishTransaction()
        }

        private var nextIteratorId: Long = 1
        private fun getNextIteratorId(): Long {
            return nextIteratorId++
        }

        fun newIterator(): Long {
            val id = getNextIteratorId()
            iterators[id] = chunkTransaction.iterator()
            return id
        }

        fun getIterator(id: Long): ChunkTransaction.Iterator? {
            return iterators[id]
        }

        fun closeIterator(id: Long) {
            iterators.remove(id)?.close()
        }
    }

    private val transactions: MutableMap<Long, Transaction> = HashMap()
    private var nextTransactionId: Long = 1
    private fun getNextId(): Long {
        return nextTransactionId++
    }

    fun startTransaction(branch: String, accessRight: AccessRight, chunkTransaction: ChunkTransaction): Transaction {
        val id = getNextId()
        return Transaction(id, branch, accessRight, chunkTransaction).also {
            transactions[id] = it
        }
    }

    fun getTransaction(id: Long): Transaction? {
        return transactions[id]
    }

    suspend fun finishTransaction(id: Long) {
        val transaction = transactions.remove(id)
        transaction?.commit()?.await()
    }
}
