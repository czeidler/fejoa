package org.fejoa.repository.sync

import org.fejoa.network.RemotePipe
import org.fejoa.storage.BranchLog
import org.fejoa.storage.StorageBackend
import org.fejoa.support.*


class RequestHandler(val branch: String, val transactionManager: TransactionManager,
                     private val branchBackend: StorageBackend.BranchBackend, private val logGetter: BranchLogGetter) {
    enum class Result(val value: Int, val description: String) {
        OK(0, "ok"),
        MISSING_ACCESS_RIGHTS(1, "missing access rights"),
        ERROR(-1, "error")
    }

    interface BranchLogGetter {
        operator fun get(branch: String): BranchLog?
    }

    private fun checkAccessRights(request: SyncProtocol.CommandType, accessRights: AccessRight): Boolean {
        when (request) {
            SyncProtocol.CommandType.GET_REMOTE_TIP,
            SyncProtocol.CommandType.GET_CHUNKS -> if (accessRights.value and AccessRight.PULL.value == 0)
                return false
            SyncProtocol.CommandType.PUT_CHUNKS,
            SyncProtocol.CommandType.HAS_CHUNKS -> if (accessRights.value and AccessRight.PUSH.value == 0)
                return false
            SyncProtocol.CommandType.GET_ALL_CHUNKS -> if (accessRights.value and AccessRight.PULL_CHUNK_STORE.value == 0)
                return false
            else -> throw Exception("Invalid command type")
        }
        return true
    }

    suspend fun handle(remotePipe: RemotePipe, accessRights: AccessRight): Result {
        try {
            val request = SyncProtocol.handle(remotePipe.inStream)
            if (!checkAccessRights(request.header.getCommandType(), accessRights))
                return Result.MISSING_ACCESS_RIGHTS

            val commandType = request.header.getCommandType()
            val dummy = when (commandType) {
                SyncProtocol.CommandType.GET_REMOTE_TIP
                    -> BranchLogProtocol.handle(request, remotePipe, logGetter)
                SyncProtocol.CommandType.GET_CHUNKS
                    -> GetProtocol.handle(branch, request, remotePipe, transactionManager , branchBackend)
                SyncProtocol.CommandType.PUT_CHUNKS
                    -> PutProtocol.handle(branch, request, remotePipe, transactionManager, branchBackend)
                SyncProtocol.CommandType.HAS_CHUNKS
                    -> HasChunkProtocol.handle(branch, request, remotePipe, transactionManager, branchBackend)
                SyncProtocol.CommandType.GET_ALL_CHUNKS
                    -> ChunkIteratorProtocol.handle(branch, request, remotePipe, transactionManager, branchBackend)
                SyncProtocol.CommandType.ERROR,
                SyncProtocol.CommandType.RESULT -> {
                    val sender = SyncProtocol.Sender(remotePipe.outStream, SyncProtocol.CommandType.ERROR)
                    sender.writeErrorBlockSubmit(SyncProtocol.Error(SyncProtocol.Error.Type.PROTOCOL_ERROR,
                            "Unexpected request $commandType"))
                    return Result.OK
                }
            }
        } catch (e: Exception) {
            try {
                val sender = SyncProtocol.Sender(remotePipe.outStream, SyncProtocol.CommandType.ERROR)
                sender.writeErrorBlockSubmit(SyncProtocol.Error(SyncProtocol.Error.Type.EXCEPTION, "Internal error $e"))
            } catch (e1: IOException) {
                println(e1.toString())
            }

            return Result.ERROR
        }

        return Result.OK
    }
}
