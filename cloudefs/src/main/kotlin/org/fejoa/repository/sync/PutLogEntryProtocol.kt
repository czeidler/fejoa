package org.fejoa.repository.sync

import kotlinx.serialization.SerialId
import kotlinx.serialization.Serializable
import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.storage.BranchLogEntry
import org.fejoa.storage.HashValue
import org.fejoa.storage.HashValueDataSerializer


object PutLogEntryProtocol {
    /**
     * @param expectedRemoteEntryId the expected entry id on the remote, if the id has been updated remotely the put
     * request fails
     */
    @Serializable
    class PutLogRequest(@SerialId(LOG_ENTRY_TAG) val entry: BranchLogEntry,
                        @SerialId(EXPECTED_ID_TAG)
                        @Serializable(with= HashValueDataSerializer::class)
                        val expectedRemoteEntryId: HashValue?) {
        companion object {
            const val LOG_ENTRY_TAG = 0
            const val EXPECTED_ID_TAG = 1

            fun read(data: ByteArray): PutLogRequest {
                val buffer = ProtocolBufferLight(data)
                val entry = buffer.getBytes(LOG_ENTRY_TAG)?.let { BranchLogEntry.read(it) }
                        ?: throw Exception("Missing chunk data")
                val expectedRemoteEntryId = buffer.getBytes(EXPECTED_ID_TAG)?.let { HashValue(it) }
                return PutLogRequest(entry, expectedRemoteEntryId)
            }
        }

        fun toProtoBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            buffer.put(LOG_ENTRY_TAG, entry.toProtoBuffer().toByteArray())
            if (expectedRemoteEntryId != null)
                buffer.put(EXPECTED_ID_TAG, expectedRemoteEntryId.bytes)
            return buffer
        }

        fun asBlock(): SyncProtocol.ProtocolBlock {
            return SyncProtocol.ProtocolBlock(SyncProtocol.BlockType.PUT_BRANCH_LOG_TIP, toProtoBuffer().toByteArray())
        }
    }
}
