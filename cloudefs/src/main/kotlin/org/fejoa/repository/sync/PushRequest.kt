package org.fejoa.repository.sync

import org.fejoa.chunkcontainer.ChunkContainer
import org.fejoa.chunkcontainer.ChunkContainerNode
import org.fejoa.chunkcontainer.ChunkContainerRef
import org.fejoa.network.RemotePipe
import org.fejoa.repository.*
import org.fejoa.repository.sync.SyncProtocol.Companion.MAX_CHUNKS_PER_REQUEST
import org.fejoa.repository.sync.SyncProtocol.Companion.MAX_HAS_CHUNGS_PER_REQUEST
import org.fejoa.storage.ChunkAccessor
import org.fejoa.storage.HashValue


class PushRequest(private val repository: Repository) {
    private suspend fun collectChunkContainer(pointer: ChunkContainerRef, chunks: MutableSet<HashValue>) {
        if (pointer.boxHash.isZero)
            return

        chunks.add(pointer.boxHash)
        // TODO: be more efficient and calculate the container diff
        val accessor = repository.objectIndex.getChunkAccessor(pointer)
        val chunkContainer = ChunkContainer.read(accessor, pointer)
        getChunkContainerNodeChildChunks(chunkContainer, accessor, chunks)
    }

    private suspend fun getChunkContainerNodeChildChunks(node: ChunkContainerNode, accessor: ChunkAccessor,
                                                         chunks: MutableSet<HashValue>) {
        for (chunkPointer in node.chunkPointers) {
            chunks.add(chunkPointer.getChunRef().boxHash)
            if (!ChunkContainerNode.isDataPointer(chunkPointer)) {
                val child = ChunkContainerNode.read(accessor, node, chunkPointer)
                getChunkContainerNodeChildChunks(child, accessor, chunks)
            }
        }
    }

    suspend fun push(remotePipe: RemotePipe, accessors: ChunkAccessors, branch: String)
            : Request.ResultType {
        // TODO double check if everything is coroutine safe
        val rawTransaction = accessors.getRawAccessor()

        // WARNING race condition: We have to use the commit from the branch log because we send this log entry
        // straight to the server. This means the headCommit and the branch log entry must be in sync!
        val localLogTip = repository.log.getHead() ?: return Request.ResultType.ERROR

        val headCommitPointer = repository.branchLogIO.readFromLog(localLogTip.message.data)
        val remoteLogTip = BranchLogProtocol.getBranchLog(remotePipe, branch, 1).entries
                .firstOrNull()
        val remoteRepoRef = if (remoteLogTip != null && remoteLogTip.message.data.isNotEmpty())
            repository.branchLogIO.readFromLog(remoteLogTip.message.data) else null

        val containersToPush = if (remoteRepoRef != null && !remoteRepoRef.head.hash.value.isZero) {
            if (headCommitPointer.head.hash.value.isZero)
                return Request.ResultType.PULL_REQUIRED
            // remote has this branch, only push the diff to the common ancestor
            val remoteCommit = repository.commitCache.getCommit(remoteRepoRef.head)

            val headCommit = repository.commitCache.getCommit(headCommitPointer.head)
            val commonAncestors = CommonAncestorsFinder.find(repository.commitCache, remoteCommit,
                    repository.commitCache, headCommit)
            commonAncestors.commonAncestors.firstOrNull { it.hash == remoteCommit.hash }
                    ?: return Request.ResultType.PULL_REQUIRED

            val remoteObjectIndex = ObjectIndex.open(remoteRepoRef.objectIndexObjectIndexRef,
                    repository.config, accessors)
            ObjectIndex.getModified(remoteObjectIndex, repository.objectIndex)
        } else if (!headCommitPointer.head.hash.value.isZero) {
            // push everything
            ObjectIndex.getModified(null, repository.objectIndex)
        } else {
            ArrayList()
        }

        val chunksToPush = let {
            val set: MutableSet<HashValue> = HashSet()
            containersToPush.forEach {
                collectChunkContainer(it, set)
            }
            // add our object index
            collectChunkContainer(repository.objectIndex.chunkContainer.ref, set)
            set.toMutableList()
        }

        val remoteChunks = HasChunkProtocol.hasChunks(remotePipe, branch, chunksToPush,
                MAX_HAS_CHUNGS_PER_REQUEST)
        for (chunk in remoteChunks)
            chunksToPush.remove(chunk)

        // start request
        PutProtocol.putChunks(rawTransaction, remotePipe, branch, chunksToPush,
                PutLogEntryProtocol.PutLogRequest(localLogTip, remoteLogTip?.entryId),
                MAX_CHUNKS_PER_REQUEST)
        return Request.ResultType.OK
    }
}
