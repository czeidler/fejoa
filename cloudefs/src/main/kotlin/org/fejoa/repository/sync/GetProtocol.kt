package org.fejoa.repository.sync

import kotlinx.serialization.SerialId
import kotlinx.serialization.Serializable
import org.fejoa.network.RemotePipe
import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.storage.HashValue
import org.fejoa.storage.HashValueDataSerializer
import org.fejoa.storage.StorageBackend
import org.fejoa.support.await

object GetProtocol {
    enum class BlockType(val value: Long) {
        PULL_CHUNKS(1)
    }

    @Serializable
    class PullRequest(@SerialId(CHUNKS_TAG) @Serializable(with= HashValueDataSerializer::class)
                      val hashes: List<HashValue>) {
        companion object {
            const val CHUNKS_TAG = 0

            fun read(data: ByteArray): PullRequest {
                val buffer = ProtocolBufferLight(data)
                val hashes = buffer.getByteArrayList(CHUNKS_TAG)?.map { HashValue(it) }
                        ?: throw Exception("Missing chunk hashes")
                return PullRequest(hashes)
            }
        }

        fun toProtoBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            val rawHashes = hashes.map { it.bytes }
            buffer.put(CHUNKS_TAG, rawHashes)
            return buffer
        }

        fun asBlock(): SyncProtocol.ProtocolBlock {
            return SyncProtocol.ProtocolBlock(SyncProtocol.BlockType.GET_CHUNKS, toProtoBuffer().toByteArray())
        }
    }

    class PullRequestReply(@SerialId(CHUNKS_TAG) val chunks: List<ProtocolChunk>,
                           @SerialId(MISSING_CHUNKS_TAG) @Serializable(with=HashValueDataSerializer::class)
                           val missingChunks: List<HashValue>) {
        companion object {
            const val CHUNKS_TAG = 0
            const val MISSING_CHUNKS_TAG = 1

            fun read(data: ByteArray): PullRequestReply {
                val buffer = ProtocolBufferLight(data)
                val chunks: List<ProtocolChunk> = buffer.getByteArrayList(CHUNKS_TAG)?.map {
                    ProtocolChunk.read(it)
                } ?: emptyList()
                val missingChunks: List<HashValue> = buffer.getByteArrayList(MISSING_CHUNKS_TAG)?.map { HashValue(it) }
                        ?: emptyList()
                return PullRequestReply(chunks, missingChunks)
            }
        }

        fun toProtoBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            val rawChunks = chunks.map { it.toProtoBuffer().toByteArray() }
            val rawMissingChunks = missingChunks.map { it.bytes }
            buffer.put(CHUNKS_TAG, rawChunks)
            buffer.put(MISSING_CHUNKS_TAG, rawMissingChunks)
            return buffer
        }

        fun asBlock(): SyncProtocol.ProtocolBlock {
            return SyncProtocol.ProtocolBlock(SyncProtocol.BlockType.GET_CHUNKS, toProtoBuffer().toByteArray())
        }
    }

    suspend fun getChunks(remotePipe: RemotePipe, branch: String, requestedChunks: List<HashValue>,
                          maxChunksPerRequest: Int, onBunchFetched: suspend (List<PullRequestReply>) -> Unit) {
        var transactionId: Long = -1
        val windowedRequest = requestedChunks.windowed(maxChunksPerRequest, maxChunksPerRequest, true)
        windowedRequest.forEachIndexed { index, list ->
            val finishTransaction = index == windowedRequest.lastIndex
            val result: MutableList<PullRequestReply> = ArrayList()
            val transId = getChunks(remotePipe, branch, list, result, transactionId, finishTransaction)
            if (transactionId >= 0 && transId != transactionId)
                throw Exception("Unexpected transaction id $transId but $transactionId expected")
            transactionId = transId
            onBunchFetched(result)
        }
    }

    /**
     * @return the transaction id
     */
    private suspend fun getChunks(remotePipe: RemotePipe, branch: String, requestedChunks: List<HashValue>,
                                  result: MutableList<PullRequestReply>,
                                  remoteTransactionId: Long = -1, finishTransaction: Boolean): Long {
        val outputStream = remotePipe.outStream
        val request = SyncProtocol.Sender(outputStream, SyncProtocol.CommandType.GET_CHUNKS)

        TransactionProtocol.send(request, remoteTransactionId, branch, AccessRight.PULL, finishTransaction) {
            // get chunks
            request.write(PullRequest(requestedChunks).asBlock())
        }

        // receive reply
        val reply = request.submitWithErrorCheck(remotePipe.inStream)
        return TransactionProtocol.receiveReply(reply, remoteTransactionId) {
            while (reply.hasNext()) {
                val block = reply.next()
                when (block.type) {
                    SyncProtocol.BlockType.GET_CHUNKS.value -> {
                        val pullBlock = PullRequestReply.read(block.data)
                        result += pullBlock
                    }
                }
            }
        }
    }

    suspend fun handle(branch: String, request: SyncProtocol.Receiver, remotePipe: RemotePipe,
                       transactionManager: TransactionManager,
                       branchBackend: StorageBackend.BranchBackend) {
        val requestedChunks: MutableList<HashValue> = ArrayList()
        TransactionProtocol.Handler(request, branch, transactionManager, branchBackend).receive {
            // read requested chunks
            while (request.hasNext()) {
                val block = request.next()
                when (block.type) {
                    SyncProtocol.BlockType.GET_CHUNKS.value -> {
                        val pullRequest = PullRequest.read(block.data)
                        requestedChunks.addAll(pullRequest.hashes)
                    }
                    else -> {
                    }
                }
            }
        }.reply(remotePipe.outStream) { reply, transaction ->
            val maxChunksPerBlock = 100
            val chunks: MutableList<ProtocolChunk> = ArrayList()
            val missingChunks: MutableList<HashValue> = ArrayList()
            requestedChunks.forEachIndexed { index, hashValue ->
                if (!transaction.chunkTransaction.hasChunk(hashValue).await())
                    missingChunks.add(hashValue)
                else {
                    val chunk = transaction.chunkTransaction.getChunk(hashValue).await()
                    chunks.add(ProtocolChunk(hashValue, chunk))
                }

                // periodically write results
                if (chunks.size >= maxChunksPerBlock || index == requestedChunks.lastIndex) {
                    reply.write(PullRequestReply(chunks, missingChunks).asBlock())
                    chunks.clear()
                    missingChunks.clear()
                }
            }

            reply.submit()
        }
    }
}