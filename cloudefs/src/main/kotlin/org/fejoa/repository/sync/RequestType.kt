package org.fejoa.repository.sync

object Request {
    val BRANCH_REQUEST_METHOD = "branchRequest"

    // errors
    enum class ResultType(val value: Int) {
        PULL_REQUIRED(-2),
        ERROR(-1),
        OK(0),
    }
}
