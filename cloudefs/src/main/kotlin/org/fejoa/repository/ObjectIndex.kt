package org.fejoa.repository

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.fejoa.chunkcontainer.*
import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.protocolbufferlight.VarInt
import org.fejoa.revlog.Revlog
import org.fejoa.storage.*
import org.fejoa.support.*


/**
 * Reference to an entry in the object index
 */
data class Ref(val objectId: Int, val type: Type, val position: Long) {
    enum class Type(val value: Int) {
        CONTAINER(0), // position is not used
        REV_LOG(1), // position is the rev log entry position
    }

    companion object {
        fun read(inStream: InStream): Ref {
            val (objectId, _, typeBit) = VarInt.read(inStream, 1)
            val type = if (typeBit == 0) Type.CONTAINER else Type.REV_LOG
            val position = if (type == Type.REV_LOG) VarInt.read(inStream).first else 0
            return Ref(objectId.toInt(), type, position)
        }
    }

    fun write(outStream: OutStream) {
        VarInt.write(outStream, objectId.toLong(), type.value, 1)
        if (type == Type.REV_LOG)
            VarInt.write(outStream, position)
    }

    suspend fun write(outStream: AsyncOutStream) {
        VarInt.write(outStream, objectId.toLong(), type.value, 1)
        if (type == Type.REV_LOG)
            VarInt.write(outStream, position)
    }
}

data class ObjectRef(val hash: Hash, val ref: Ref)

class ObjectIndex private constructor(val config: RepositoryConfig,
                                      val chunkContainer: ChunkContainer,
                                      private val accessors: ChunkAccessors) {

    private val stream = ChunkContainerRandomDataAccess(chunkContainer,
            RandomDataAccess.READ.join(RandomDataAccess.WRITE))

    class ObjectIndexRef internal constructor(val chunkContainerRef: ChunkContainerRef) {
        companion object {
            private const val VERSION_TAG = 0
            private const val CONTAINER_TAG = 1

            fun read(data: ByteArray, parentConfig: ChunkingConfig?): ObjectIndexRef {
                val buffer = ProtocolBufferLight(data)
                val version = buffer.getLong(VERSION_TAG)?.toInt() ?: throw IOException()
                if (version != VERSION)
                    throw IOException("Unknown version: $version")

                val chunkRefBytes = buffer.getBytes(CONTAINER_TAG) ?: throw IOException()

                return ObjectIndexRef(ChunkContainerRef.read(ByteArrayInStream(chunkRefBytes), parentConfig))
            }
        }

        fun write(outStream: OutStream) {
            val buffer = ProtocolBufferLight()
            buffer.put(VERSION_TAG, VERSION)
            val containerBuffer = ByteArrayOutStream()
            chunkContainerRef.write(containerBuffer)
            buffer.put(CONTAINER_TAG, containerBuffer.toByteArray())
            return buffer.write(outStream)
        }
    }

    private val lock = Mutex()

    companion object {
        private const val ENTRY_SIZE = 128L
        const val VERSION = 1

        fun create(config: RepositoryConfig, containerSpec: ContainerSpec, accessors: ChunkAccessors): ObjectIndex {
            val objectIndexCC = ChunkContainer.create(accessors.getObjectIndexAccessor(containerSpec),
                    containerSpec)
            return ObjectIndex(config, objectIndexCC, accessors)
        }

        suspend fun open(objectIndexRef: ObjectIndexRef, repoConfig: RepositoryConfig, accessors: ChunkAccessors)
                : ObjectIndex {
            val objectIndexCC = ChunkContainer.read(accessors.getObjectIndexAccessor(
                    repoConfig.containerSpec), objectIndexRef.chunkContainerRef)
            return ObjectIndex(repoConfig, objectIndexCC, accessors)
        }

        suspend fun getModified(base: ObjectIndex?, other: ObjectIndex): List<ChunkContainerRef> {
            val content: MutableMap<HashValue, ChunkContainerRef> = HashMap()
            base?.let {
                for (i in 0 until it.getNEntries()) {
                    val ref = it.getChunkContainerRef(i.toInt(), it.config.hashSpec)
                    content[ref.dataHash] = ref
                }
            }

            val results: MutableList<ChunkContainerRef> = ArrayList()
            for (i in 0 until other.getNEntries()) {
                val ref = other.getChunkContainerRef(i.toInt(), other.config.hashSpec)
                if (!content.containsKey(ref.dataHash))
                    results += ref
            }
            return results
        }
    }

    suspend fun flush(): ObjectIndexRef = lock.withLock {
        chunkContainer.flush(false)
        return@withLock ObjectIndexRef(chunkContainer.ref)
    }

    fun getRef(): ObjectIndexRef {
        return ObjectIndexRef(chunkContainer.ref)
    }

    fun getNEntries(): Long {
        return chunkContainer.getDataLength() / ENTRY_SIZE
    }

    suspend fun getChunkContainerRef(ref: Ref): ChunkContainer = lock.withLock {
        val containerRef = getEntryUnlocked(ref.objectId, chunkContainer.ref.hash.spec)
        val container =  ChunkContainer.read(getChunkAccessor(containerRef), containerRef)
        if (ref.type == Ref.Type.CONTAINER)
            return container

        val revLog = Revlog(container)
        val data = revLog.get(ref.position)
        // use rev log container spec
        val newContainerRef = ChunkContainerRef(containerRef.hash.spec, containerRef.boxSpec)
        val newContainer = ChunkContainer.create(getChunkAccessor(newContainerRef),
                newContainerRef.containerSpec)
        val outStream = ChunkContainerOutStream(newContainer,
                normalizeChunkSize = newContainerRef.boxSpec.dataNormalization)
        outStream.write(data)
        outStream.close()
        return newContainer
    }

    suspend fun getChunkContainerRef(objectId: Int, parent: HashSpec?): ChunkContainerRef = lock.withLock {
        return@withLock getEntryUnlocked(objectId, parent)
    }

    private suspend fun getEntryUnlocked(objectId: Int, parent: HashSpec?): ChunkContainerRef {
        val pos = objectId * ENTRY_SIZE
        stream.seek(pos)

        val buffer = ByteArray(ENTRY_SIZE.toInt())
        stream.readFully(buffer)

        return ChunkContainerRef.read(ByteArrayInStream(buffer), parent?.chunkingConfig)
    }

    private suspend fun updateEntry(objectId: Int, containerRef: ChunkContainerRef) {
        val pos = objectId * ENTRY_SIZE
        if (pos > chunkContainer.getDataLength())
            throw IOException("Invalid objectId")

        stream.seek(pos)
        try {
            val containerBytes = ByteArrayOutStream()
            val size = containerRef.write(containerBytes)
            if (size > ENTRY_SIZE)
                throw IOException("ChunkContainerRef is too large: $size")
            stream.write(containerBytes.toByteArray())

            if (size < ENTRY_SIZE) {
                val padding = ByteArray((ENTRY_SIZE - size).toInt())
                stream.write(padding)
            }
        } finally {
            stream.close()
        }
    }

    private suspend fun append(containerRef: ChunkContainerRef): Int {
        val dataSize = chunkContainer.getDataLength()
        if (dataSize % ENTRY_SIZE != 0L)
            throw IOException("Unexpected object index size")
        val id = (dataSize / ENTRY_SIZE).toInt()
        updateEntry(id, containerRef)
        return id
    }

    suspend fun putContainer(chunkContainer: ChunkContainer, ref: Ref?): Ref = lock.withLock {
        chunkContainer.flush(false)

        return if (chunkContainer.getDataLength() < config.revLog.maxEntrySize) {
            val revLogContainer = if (ref == null) {
                val newContainerRef = ChunkContainerRef(ContainerSpec(config.hashSpec, config.boxSpec))
                ChunkContainer.create(getChunkAccessor(newContainerRef), newContainerRef.containerSpec)
            } else {
                val containerRef = getEntryUnlocked(ref.objectId, chunkContainer.ref.hash.spec)
                ChunkContainer.read(getChunkAccessor(containerRef), containerRef)
            }
            val revLog = Revlog(revLogContainer)
            val basePosition = ref?.position ?: -1
            val revLogPos = revLog.add(ChunkContainerInStream(chunkContainer).readAll(), basePosition)

            revLogContainer.flush(false)
            val objectId = if (ref == null || ref.type == Ref.Type.CONTAINER) {
                append(revLogContainer.ref)
            } else {
                updateEntry(ref.objectId, revLogContainer.ref)
                ref.objectId
            }

            Ref(objectId, Ref.Type.REV_LOG, revLogPos)
        } else {
            val objectId = append(chunkContainer.ref)
            Ref(objectId, Ref.Type.CONTAINER, 0)
        }
    }

    fun getChunkAccessor(ref: ChunkContainerRef): ChunkAccessor {
        return when (ref.boxSpec.encInfo.type) {
            BoxSpec.EncryptionInfo.Type.PARENT -> chunkContainer.blobAccessor
            BoxSpec.EncryptionInfo.Type.PLAIN
            -> accessors.getRawAccessor().toChunkAccessor().prepareAccessor(ref.boxSpec, null)
        }
    }
}
