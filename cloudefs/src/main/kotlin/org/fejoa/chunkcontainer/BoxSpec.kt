package org.fejoa.chunkcontainer

import org.fejoa.protocolbufferlight.VarInt
import org.fejoa.support.*


suspend fun AsyncOutStream.writeVarIntDelimited(byteArray: ByteArray): Int {
    var bytesWritten= VarInt.write(this, byteArray.size)
    write(byteArray)
    bytesWritten += byteArray.size
    return bytesWritten
}

suspend fun AsyncInStream.readVarIntDelimited(): Pair<ByteArray, Int> {
    val result  = VarInt.read(this)
    val read = result.second
    val length = result.first
    if (length > 10 * 1000 * 1024)
        throw IOException("Refuse to read large ByteArray")
    val buffer = ByteArray(length.toInt())
    read(buffer)
    return buffer to read + buffer.size
}

fun OutStream.writeVarIntDelimited(byteArray: ByteArray): Int {
    var bytesWritten= VarInt.write(this, byteArray.size)
    write(byteArray)
    bytesWritten += byteArray.size
    return bytesWritten
}

fun InStream.readVarIntDelimited(): Pair<ByteArray, Int> {
    val result  = VarInt.read(this)
    val read = result.second
    val length = result.first
    if (length > 10 * 1000 * 1024)
        throw IOException("Refuse to read large ByteArray")
    val buffer = ByteArray(length.toInt())
    read(buffer)
    return buffer to read + buffer.size
}


const val MOST_SIGNIFICANT_BIT = 1 shl 7

/**
 * // Format:
 * A EncryptionInfo followed by:
 * | 7         | 6        | 5         | 4         | 3        | 2 | 1 | 0 |
 * | extension | reserved | data norm | node norm | zip order| ZipType   |
 *
 * (zip order indicates if compression is applied before (1) or after (0) encryption
 * Size: min 2 bytes
 */
data class BoxSpec(var encInfo: EncryptionInfo = EncryptionInfo(),
              val zipType: ZipType = ZipType.DEFLATE, val zipBeforeEnc: Boolean = true,
              val nodeNormalization: Boolean = false,
              val dataNormalization: Boolean = false) {
    fun clone(): BoxSpec {
        return BoxSpec(encInfo = encInfo.clone(),
                zipType = zipType,
                nodeNormalization = nodeNormalization,
                dataNormalization = dataNormalization)
    }

    /**
     * Format:
     * | 7         | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
     * | extension | reserved      | enc type  |
     * | extension data (VarInt delimited)     |
     *
     * If extension is set a VarInt delimited field is followed
     *
     * Size: min 1 byte
     */
    data class EncryptionInfo(val type: EncryptionInfo.Type, val data: ByteArray) {
        constructor() : this(Type.PARENT, ByteArray(0))
        constructor(type: EncryptionInfo.Type) : this(type, ByteArray(0))

        fun clone(): EncryptionInfo {
            return EncryptionInfo(type, data.copyOf())
        }

        enum class Type(val value: Int) {
            PLAIN(0),
            PARENT(1), // encryption key and parameters are derived from the repo config
            //CUSTOM(2),
        }

        companion object {
            private const val ENC_TYPE_MASK = 0x7

            suspend fun read(inStream: AsyncInStream): EncryptionInfo {
                val rawByte = inStream.readByteValue()
                val typeValue = rawByte and ENC_TYPE_MASK
                val type = EncryptionInfo.Type.values().firstOrNull { it.value == typeValue }
                        ?: throw IOException("Unknown type $typeValue")
                val data = if (rawByte and MOST_SIGNIFICANT_BIT != 0) inStream.readVarIntDelimited().first
                else ByteArray(0)
                return EncryptionInfo(type, data)
            }

            fun read(inStream: InStream): EncryptionInfo {
                val rawByte = inStream.readByteValue()
                val typeValue = rawByte and ENC_TYPE_MASK
                val type = EncryptionInfo.Type.values().firstOrNull { it.value == typeValue }
                        ?: throw IOException("Unknown type $typeValue")
                val data = if (rawByte and MOST_SIGNIFICANT_BIT != 0) inStream.readVarIntDelimited().first
                else ByteArray(0)
                return EncryptionInfo(type, data)
            }
        }

        suspend fun write(outStream: AsyncOutStream): Int {
            var bytesWritten = outStream.write(type.value)
            if (type.value and MOST_SIGNIFICANT_BIT != 0)
                bytesWritten += outStream.writeVarIntDelimited(data)
            return bytesWritten
        }

        fun write(outStream: OutStream): Int {
            var bytesWritten = outStream.writeByte(type.value)
            if (type.value and MOST_SIGNIFICANT_BIT != 0)
                bytesWritten += outStream.writeVarIntDelimited(data)
            return bytesWritten
        }
    }

    companion object {
        private const val ZIP_TYPE_MASK = 0x7
        private const val ZIP_ORDER_MASK = 1 shl 3
        private const val NODE_NORM_MASK = 1 shl 4
        private const val DATA_NORM_MASK = 1 shl 5

        suspend fun read(inStream: AsyncInStream): BoxSpec {
            val encInfo = EncryptionInfo.read(inStream)
            val config = inStream.readByteValue()

            if (config and MOST_SIGNIFICANT_BIT != 0)
                inStream.readVarIntDelimited() // read extension but ignore it for now

            val zipTypeValue = config and ZIP_TYPE_MASK
            val zipType = BoxSpec.ZipType.values().firstOrNull { it.value == zipTypeValue }
                    ?: throw IOException("Unknown type")
            val zipBeforeEnc = config and ZIP_ORDER_MASK != 0
            val nodeNormalization = config and NODE_NORM_MASK != 0
            val dataNormalization = config and DATA_NORM_MASK != 0

            return BoxSpec(encInfo, zipType, zipBeforeEnc = zipBeforeEnc,
                    nodeNormalization = nodeNormalization, dataNormalization = dataNormalization)
        }

        fun read(inStream: InStream): BoxSpec {
            val encInfo = EncryptionInfo.read(inStream)
            val config = inStream.readByteValue()

            if (config and MOST_SIGNIFICANT_BIT != 0)
                inStream.readVarIntDelimited() // read extension but ignore it for now

            val zipTypeValue = config and ZIP_TYPE_MASK
            val zipType = BoxSpec.ZipType.values().firstOrNull { it.value == zipTypeValue }
                    ?: throw IOException("Unknown type")
            val zipBeforeEnc = config and ZIP_ORDER_MASK != 0
            val nodeNormalization = config and NODE_NORM_MASK != 0
            val dataNormalization = config and DATA_NORM_MASK != 0

            return BoxSpec(encInfo, zipType, zipBeforeEnc = zipBeforeEnc,
                    nodeNormalization = nodeNormalization, dataNormalization = dataNormalization)
        }
    }

    suspend fun write(outStream: AsyncOutStream): Int {
        var bytesWritten = encInfo.write(outStream)
        var config = zipType.value and ZIP_TYPE_MASK
        if (zipBeforeEnc)
            config = config or ZIP_ORDER_MASK
        if (nodeNormalization)
            config = config or NODE_NORM_MASK
        if (dataNormalization)
            config = config or DATA_NORM_MASK
        
        bytesWritten += outStream.write(config)
        return bytesWritten
    }

    fun write(outStream: OutStream): Int {
        var bytesWritten = encInfo.write(outStream)
        var config = zipType.value and ZIP_TYPE_MASK
        if (zipBeforeEnc)
            config = config or ZIP_ORDER_MASK
        if (nodeNormalization)
            config = config or NODE_NORM_MASK
        if (dataNormalization)
            config = config or DATA_NORM_MASK

        bytesWritten += outStream.writeByte(config)
        return bytesWritten
    }

    enum class ZipType(val value: Int) {
        NONE(0),
        DEFLATE(1),
        ZSTANDARD(2),
    }
}