package org.fejoa.crypto

import kotlinx.coroutines.newSingleThreadContext
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.fejoa.support.ByteArrayOutStream
import org.fejoa.support.Executor
import org.fejoa.support.Future
import org.fejoa.support.JVMExecutor

import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.security.spec.ECGenParameterSpec
import java.util.logging.Level
import java.util.logging.Logger
import javax.crypto.*
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec

import java.math.BigInteger
import java.security.*
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.RSAKeyGenParameterSpec
import java.security.spec.X509EncodedKeySpec
import javax.crypto.spec.SecretKeySpec


val cryptoThread = newSingleThreadContext("Fejoa encryption")

class BCCryptoInterface : CryptoInterface {

    companion object {
        // enable “Unlimited Strength” JCE policy
        // This is necessary to use AES256!
        private val remover = JavaSecurityRestrictionRemover()

        private val encryptCiphers: MutableMap<String, Cipher> = HashMap()
        private val decryptCiphers: MutableMap<String, Cipher> = HashMap()
    }

    internal class JavaSecurityRestrictionRemover {
        // Based on http://stackoverflow.com/questions/1179672/how-to-avoid-installing-unlimited-strength-jce-policy-files-when-deploying-an

        private// This simply matches the Oracle JRE, but not OpenJDK.
        val isRestrictedCryptography: Boolean
            get() = "Java(TM) SE Runtime Environment" == System.getProperty("java.runtime.name")

        init {
            val logger = Logger.getGlobal()
            if (!isRestrictedCryptography) {
                logger.fine("Cryptography restrictions removal not needed")
            } else {
                try {
                    /*
                 * Do the following, but with reflection to bypass access checks:
                 *
                 * JceSecurity.isRestricted = false;
                 * JceSecurity.defaultPolicy.perms.clear();
                 * JceSecurity.defaultPolicy.add(CryptoAllPermission.INSTANCE);
                 */
                    val jceSecurity = Class.forName("javax.crypto.JceSecurity")
                    val cryptoPermissions = Class.forName("javax.crypto.CryptoPermissions")
                    val cryptoAllPermission = Class.forName("javax.crypto.CryptoAllPermission")

                    val isRestrictedField = jceSecurity.getDeclaredField("isRestricted")
                    isRestrictedField.isAccessible = true
                    val modifiersField = Field::class.java.getDeclaredField("modifiers")
                    modifiersField.isAccessible = true
                    modifiersField.setInt(isRestrictedField, isRestrictedField.modifiers and Modifier.FINAL.inv())
                    isRestrictedField.set(null, false)

                    val defaultPolicyField = jceSecurity.getDeclaredField("defaultPolicy")
                    defaultPolicyField.isAccessible = true
                    val defaultPolicy = defaultPolicyField.get(null) as PermissionCollection

                    val perms = cryptoPermissions.getDeclaredField("perms")
                    perms.isAccessible = true
                    (perms.get(defaultPolicy) as MutableMap<*, *>).clear()

                    val instance = cryptoAllPermission.getDeclaredField("INSTANCE")
                    instance.isAccessible = true
                    defaultPolicy.add(instance.get(null) as Permission)

                    logger.fine("Successfully removed cryptography restrictions")
                } catch (e: Exception) {
                    logger.log(Level.WARNING, "Failed to remove cryptography restrictions", e)
                }
            }
        }
    }

    init {
        Security.addProvider(BouncyCastleProvider())
    }

    override suspend fun deriveKey(secret: String, salt: ByteArray, settings: CryptoSettings.KDF): SecretKey {
        val factory = SecretKeyFactory.getInstance(settings.algo.javaName)
        val spec = PBEKeySpec(secret.toCharArray(), salt, settings.params.iterations, settings.keySize)
        val encoded = factory.generateSecret(spec).encoded

        return secretKeyFromRaw(encoded, settings.keyAlgo)
    }

    override suspend fun generateKeyPair(settings: CryptoSettings.Asymmetric): KeyPair {
        val keyGen = KeyPairGenerator.getInstance(settings.algo.javaKey)
        val keyPair = when (settings) {
            is CryptoSettings.RSAAsymmetric -> {
                keyGen.initialize(RSAKeyGenParameterSpec(settings.modulusLength,
                        BigInteger.valueOf(settings.publicExponent.javaValue.toLong())))
                keyGen.genKeyPair()
            }
            is CryptoSettings.ECAsymmetric -> {
                keyGen.initialize(ECGenParameterSpec(settings.algo.javaCurve))
                keyGen.genKeyPair()
            }
            else -> {
                throw Exception("Unknown type")
            }
        }

        return KeyPair(PublicKeyJVM(keyPair.public, settings.algo), PrivateKeyJVM(keyPair.private, settings.algo))
    }

    override suspend fun encryptAsymmetric(input: ByteArray, key: PublicKey, algo: CryptoSettings.ASYM_ALGO)
            : ByteArray {
        val cipher = Cipher.getInstance(algo.javaName)
        cipher.init(Cipher.ENCRYPT_MODE, (key as PublicKeyJVM).key)
        return cipher.doFinal(input)
    }

    override suspend fun decryptAsymmetric(input: ByteArray, key: PrivateKey, algo: CryptoSettings.ASYM_ALGO)
            : ByteArray {
        val cipher = Cipher.getInstance(algo.javaName)
        cipher.init(Cipher.DECRYPT_MODE, (key as PrivateKeyJVM).key)
        return cipher.doFinal(input)
    }

    override suspend fun generateSymmetricKey(settings: CryptoSettings.Symmetric): SecretKey {
        val keyGenerator = KeyGenerator.getInstance(settings.algo.javaKey)
        keyGenerator.init(settings.keySize, SecureRandom())
        return SecreteKeyJVM(keyGenerator.generateKey(), settings.algo)
    }

    override suspend fun encryptSymmetric(input: ByteArray, secretKey: SecretKey, iv: ByteArray,
                                          algo: CryptoSettings.SYM_ALGO): ByteArray = synchronized(encryptCiphers) {
        val cipher = encryptCiphers[algo.javaName]
                ?: Cipher.getInstance(algo.javaName).also { encryptCiphers[algo.javaName] = it }
        val ips = IvParameterSpec(iv)
        cipher.init(Cipher.ENCRYPT_MODE, (secretKey as SecreteKeyJVM).key, ips)
        return cipher.doFinal(input)
    }

    override fun encryptSymmetricAsync(input: ByteArray, secretKey: SecretKey, iv: ByteArray,
                                       algo: CryptoSettings.SYM_ALGO): Future<ByteArray>
            = Future(JVMExecutor(cryptoThread.executor)) {
        synchronized(encryptCiphers) {
            val cipher = encryptCiphers[algo.javaName]
                    ?: Cipher.getInstance(algo.javaName).also { encryptCiphers[algo.javaName] = it }
            val ips = IvParameterSpec(iv)
            cipher.init(Cipher.ENCRYPT_MODE, (secretKey as SecreteKeyJVM).key, ips)
            cipher.doFinal(input)
        }
    }

    override fun decryptSymmetricAsync(input: ByteArray, secretKey: SecretKey, iv: ByteArray,
                                       algo: CryptoSettings.SYM_ALGO): Future<ByteArray>
            = Future(JVMExecutor(cryptoThread.executor)) {
        synchronized(decryptCiphers) {
            val cipher = decryptCiphers[algo.javaName]
                    ?: Cipher.getInstance(algo.javaName).also { decryptCiphers[algo.javaName] = it }
            val ips = IvParameterSpec(iv)
            cipher.init(Cipher.DECRYPT_MODE, (secretKey as SecreteKeyJVM).key, ips)
            cipher.doFinal(input)
        }
    }

    override suspend fun decryptSymmetric(input: ByteArray, secretKey: SecretKey, iv: ByteArray,
                                          algo: CryptoSettings.SYM_ALGO): ByteArray = synchronized(decryptCiphers) {
        val cipher = decryptCiphers[algo.javaName]
                ?: Cipher.getInstance(algo.javaName).also { decryptCiphers[algo.javaName] = it }
        val ips = IvParameterSpec(iv)
        cipher.init(Cipher.DECRYPT_MODE, (secretKey as SecreteKeyJVM).key, ips)
        return cipher.doFinal(input)
    }

    override suspend fun sign(input: ByteArray, key: PrivateKey, algo: CryptoSettings.ASYM_ALGO): ByteArray {
        val signature = Signature.getInstance(algo.javaName)
        signature.initSign((key as PrivateKeyJVM).key as java.security.PrivateKey)
        signature.update(input)
        return signature.sign()
    }

    override suspend fun verifySignature(message: ByteArray, signature: ByteArray, key: PublicKey,
                                         algo: CryptoSettings.ASYM_ALGO): Boolean {
        val sig = Signature.getInstance(algo.javaName)
        sig.initVerify((key as PublicKeyJVM).key as java.security.PublicKey)
        sig.update(message)

        val signature1 = when (algo) {
            CryptoSettings.ASYM_ALGO.ECDSA_SECP256R1_SHA256 -> {
                // Ensure the signature is in ASN1 format. Chrome produces signatures that simply concatenates r and s.
                // Also see:
                // https://crypto.stackexchange.com/questions/1795/how-can-i-convert-a-der-ecdsa-signature-to-asn-1
                if (signature.size == 64) {
                    val rData = signature.copyOfRange(0, 32)
                    val sData = signature.copyOfRange(32, 64)
                    val r = BigInteger(1, rData).toByteArray()
                    val s = BigInteger(1, sData).toByteArray()

                    val stream = ByteArrayOutStream()
                    stream.write(0x30)
                    stream.write((r.size + s.size + 4).toByte())
                    stream.write(0x02)
                    stream.write(r.size.toByte())
                    stream.write(r)
                    stream.write(0x02)
                    stream.write(s.size.toByte())
                    stream.write(s)
                    stream.toByteArray()
                } else
                    signature
            }
            CryptoSettings.ASYM_ALGO.RSASSA_PKCS1_v1_5_SHA256,
            CryptoSettings.ASYM_ALGO.RSA_OAEP_SHA256 -> signature
        }
        return sig.verify(signature1)
    }

    override suspend fun encode(key: Key): ByteArray {
        return (key as KeyJVM).toByteArray()
    }

    override suspend fun secretKeyFromRaw(key: ByteArray, algo: CryptoSettings.SYM_ALGO): SecretKey {
        return SecreteKeyJVM(SecretKeySpec(key, 0, key.size, algo.javaKey), algo)
    }

    override suspend fun privateKeyFromRaw(key: ByteArray, algo: CryptoSettings.ASYM_ALGO): PrivateKey {
        val spec = PKCS8EncodedKeySpec(key)
        val keyFactory = KeyFactory.getInstance(algo.javaKey)
        return PrivateKeyJVM(keyFactory.generatePrivate(spec), algo)
    }

    override suspend fun publicKeyFromRaw(key: ByteArray, algo: CryptoSettings.ASYM_ALGO): PublicKey {
        val spec = X509EncodedKeySpec(key)
        val keyFactory = KeyFactory.getInstance(algo.javaKey)
        return PublicKeyJVM(keyFactory.generatePublic(spec), algo)
    }
}
