package org.fejoa.support

import java.io.InputStream
import java.io.OutputStream


fun OutputStream.toOutStream(): OutStream {
    val that = this
    return object : OutStream {
        override fun write(byte: Byte): Int {
            that.write(byte.toInt())
            return 1
        }
    }
}

fun InputStream.toInStream(): InStream {
    val that = this
    return object : InStream {
        override fun read(): Int {
            return that.read()
        }
    }
}
