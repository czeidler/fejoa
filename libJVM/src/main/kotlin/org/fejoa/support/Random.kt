package org.fejoa.support

class RandomJVM {
    private val random: java.util.Random

    constructor() {
        random = java.util.Random()
    }

    constructor(seed: Long) {
        random = java.util.Random(seed)
    }

    fun readByte(): Byte {
        val buffer = ByteArray(1)
        read(buffer)
        return buffer[0]
    }

    fun read(buffer: ByteArray): Int {
        random.nextBytes(buffer)
        return buffer.size
    }

    fun readFloat(): Float {
        return random.nextFloat()
    }
}

actual typealias Random = RandomJVM