package org.fejoa.support

import kotlin.test.Test
import kotlin.test.assertEquals


class Base64ImplTest {
    @Test
    fun testStrings() {
        assertEquals("VGVzdCBTdHJpbmcgMQ==", Base64.encode("Test String 1".toUTF()))
        assertEquals("VGVzdCBTdHJpbmcgMTI=", Base64.encode("Test String 12".toUTF()))
        assertEquals("VGVzdCBTdHJpbmcgMTIz", Base64.encode("Test String 123".toUTF()))

        assertEquals("Test String 1", Base64.decode("VGVzdCBTdHJpbmcgMQ==").toUTFString())
        assertEquals("Test String 12", Base64.decode("VGVzdCBTdHJpbmcgMTI=").toUTFString())
        assertEquals("Test String 123", Base64.decode("VGVzdCBTdHJpbmcgMTIz").toUTFString())
    }
}