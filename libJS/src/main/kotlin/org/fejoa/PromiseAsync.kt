package org.fejoa

import org.fejoa.support.Future
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.js.Promise


fun <T>Promise<T>.toFuture(): Future<T> {
    val promise = Future<T>()
    this.then({
        promise.setResult(it)
    }, {
        promise.setError(it)
    })
    return promise
}
