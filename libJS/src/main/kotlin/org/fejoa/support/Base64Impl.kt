package org.fejoa.support

/*
 * Port of base64-arraybuffer to kotlin.
 *
 * https://github.com/niklasvh/base64-arraybuffer
 *
 * Licensed under the MIT license.
 */
object Base64 {
    private val chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    // Use a lookup table to find the index.
    private val lookup = IntArray(256)

    init {
        for (i in 0 until chars.length)
            lookup[chars[i].toInt()] = i
    }

    fun encode(bytes: ByteArray): String {
        val len = bytes.size
        var base64 = ""

        var i = 0
        while (i < len) {
            val byte0 = bytes[i].toInt() and 0xFF
            val byte1 = if (i + 1 < len) bytes[i + 1].toInt() and 0xFF else 0
            val byte2 = if (i + 2 < len) bytes[i + 2].toInt() and 0xFF else 0

            base64 += chars[byte0 shr 2]
            base64 += chars[((byte0 and 3) shl 4) or (byte1 shr 4)]
            base64 += chars[((byte1 and 15) shl 2) or (byte2 shr 6)]
            base64 += chars[byte2 and 63]

            i+=3
        }

        if ((len % 3) == 2) {
            base64 = base64.substring(0, base64.length - 1) + "="
        } else if (len % 3 == 1) {
            base64 = base64.substring(0, base64.length - 2) + "=="
        }

        return base64
    }

    fun decode(base64: String): ByteArray {
        var bufferLength = base64.length * 3 / 4
        val len = base64.length

        if (base64[base64.length - 1] == '=') {
            bufferLength--
            if (base64[base64.length - 2] == '=') {
                bufferLength--
            }
        }

        val buffer = ByteArray(bufferLength)

        var p = 0
        var i = 0
        while (i < len) {
            val encoded1 = lookup[base64[i].toInt()]
            val encoded2 = lookup[base64[i + 1].toInt()]
            val encoded3 = lookup[base64[i + 2].toInt()]
            val encoded4 = lookup[base64[i + 3].toInt()]

            buffer[p++] = ((encoded1 shl 2) or (encoded2 shr 4)).toByte()
            buffer[p++] = (((encoded2 and 15) shl 4) or (encoded3 shr 2)).toByte()
            buffer[p++] = (((encoded3 and 3) shl 6) or (encoded4 and 63)).toByte()

            i+=4
        }

        return buffer
    }
}
