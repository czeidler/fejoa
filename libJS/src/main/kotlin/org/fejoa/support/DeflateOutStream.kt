package org.fejoa.support

import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Uint8Array
import io.github.nodeca.pako.deflate
import io.github.nodeca.pako.inflate


actual class DeflateOutStream actual constructor(val outStream: OutStream) : OutStream {
    val buffer = ByteArrayOutStream()

    override fun write(byte: Byte): Int {
        return buffer.write(byte)
    }

    override fun close() {
        val data = buffer.toByteArray()
        val compressed = deflate(Uint8Array(data.unsafeCast<ArrayBuffer>()))
        outStream.write(compressed)
        super.close()
    }
}

actual class DeflateCompression : Compression {
    override fun Compress(data: ByteArray): Future<ByteArray> {
        return Future.completedFuture(deflate(Uint8Array(data.unsafeCast<ArrayBuffer>())))
    }

    override fun Decompress(data: ByteArray): Future<ByteArray> {
        return Future.completedFuture(inflate(Uint8Array(data.unsafeCast<ArrayBuffer>())))
    }

}