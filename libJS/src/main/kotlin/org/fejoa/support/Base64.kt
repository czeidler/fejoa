package org.fejoa.support

actual fun encodeBase64String(data: ByteArray): String {
    return Base64.encode(data)
}

actual fun decodeBase64(string: String): ByteArray {
    return Base64.decode(string)
}
