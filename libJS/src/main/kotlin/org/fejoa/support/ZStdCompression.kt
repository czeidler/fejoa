package org.fejoa.support

import com.github.yoshihitoh.zstdcodec.zstdSimpleCompress
import com.github.yoshihitoh.zstdcodec.zstdSimpleDecompress
import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Uint8Array


actual class ZStdCompression actual constructor() : Compression {
    override fun Compress(data: ByteArray): Future<ByteArray> {
        val jsData = Uint8Array(data.unsafeCast<ArrayBuffer>())
        return Future.completedFuture(zstdSimpleCompress(jsData).unsafeCast<ByteArray>())
    }

    override fun Decompress(data: ByteArray): Future<ByteArray> {
        val jsData = Uint8Array(data.unsafeCast<ArrayBuffer>())
        return Future.completedFuture(zstdSimpleDecompress(jsData).unsafeCast<ByteArray>())
    }
}
