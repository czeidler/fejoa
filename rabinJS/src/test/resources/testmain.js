requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: '/base/rabinJS/build/node_modules',

    paths: { // paths are relative to this file
        'big-integer': '../../../jsbindings/node_modules/big-integer/BigInteger.min',
        'pako': '../../../jsbindings/node_modules/pako/dist/pako.min',
        'rabinJS': '../../build/classes/kotlin/main/rabinJS',
        'rabinJS_test': '../../build/classes/kotlin/test/rabinJS_test',
    },

    deps: ['big-integer', 'pako', 'rabinJS_test'],

    // start tests when done
    callback: window.__karma__.start
});
