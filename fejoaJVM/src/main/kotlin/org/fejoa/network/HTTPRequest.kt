package org.fejoa.network

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.asCoroutineDispatcher
import org.fejoa.support.*
import java.io.*
import java.io.IOException
import java.net.*
import java.util.concurrent.Executors
import javax.servlet.ServletException


fun OutputStream.toAsyncOutStream(pool: CoroutineDispatcher): AsyncOutStream {
    val that = this
    return object : AsyncOutStream {
        override suspend fun write(buffer: ByteArray, offset: Int, length: Int): Int = async(pool) {
            that.write(buffer, offset, length)
            return@async length
        }.await()

        override suspend fun close() {
            that.close()
        }
    }
}

fun InputStream.toAsyncInStream(pool: CoroutineDispatcher): AsyncInStream {
    val that = this
    return object : AsyncInStream {
        override suspend fun read(buffer: ByteArray, offset: Int, length: Int): Int = async(pool) {
            return@async that.read(buffer, offset, length)
        }.await()

        override suspend fun close() {
            that.close()
        }
    }
}


class HTTPRequest(val url: String) : RemoteRequest {
    private var connection: HttpURLConnection? = null

    companion object {
        val pool = Executors.newCachedThreadPool().asCoroutineDispatcher()

        fun parseHeader(inputStream: InputStream): String {
            val outStream = ByteArrayOutStream()
            while (true) {
                val byte = inputStream.read()
                if (byte <= 0)
                    break
                outStream.writeByte(byte)
            }
            return outStream.toByteArray().toUTFString()
        }
    }

    init {
        if (CookieHandler.getDefault() == null)
            CookieHandler.setDefault(CookieManager(null, CookiePolicy.ACCEPT_ALL))
    }

    private class Reply(val header: String, val dataInStream: InputStream) : RemoteRequest.Reply {
        private var closed = false
        private val inStream = dataInStream.toAsyncInStream(pool)

        override suspend fun receiveHeader(): String {
            return header
        }

        override suspend fun inStream(): AsyncInStream {
            return inStream
        }

        override fun close() {
            if (closed)
                return
            closed = true

            dataInStream.close()
        }

    }

    private class Sender(val connection: HttpURLConnection, val outStream: AsyncOutStream) : RemoteRequest.DataSender {
        override fun outStream(): AsyncOutStream {
            return outStream
        }

        override suspend fun send(): RemoteRequest.Reply = async(pool) {
            val inputStream = connection.inputStream

            try {
                val receivedHeader = parseHeader(inputStream)
                println("RECEIVED: $receivedHeader")

                return@async Reply(receivedHeader, inputStream)
            } catch (e: ServletException) {
                e.printStackTrace()
                throw IOException("Unexpected server response.")
            }
        }.await()
    }

    override suspend fun send(header: String): RemoteRequest.Reply {
        return sendData(header, false).send()
    }

    override suspend fun sendData(header: String): RemoteRequest.DataSender {
        return sendData(header, true)
    }

    private suspend fun sendData(header: String, outgoingData: Boolean): RemoteRequest.DataSender
            = async(pool) {
        cancel()
        println("SEND:     " + header)

        val server = URL(url)

        val connection = server.openConnection() as HttpURLConnection
        connection.useCaches = false
        connection.doOutput = true
        connection.doInput = true
        connection.requestMethod = "POST"
        connection.setRequestProperty("Content-Type", "application/octet-stream")
        connection.setRequestProperty("Content-Transfer-Encoding", "binary")

        connection.connect()
        val outputStream = connection.outputStream
        outputStream.write(header.toUTF())
        outputStream.write(0)

        this@HTTPRequest.connection = connection
        return@async Sender(connection, outputStream.toAsyncOutStream(pool))
    }.await()

    override fun cancel() {
        connection?.disconnect()
        connection = null
    }

}

actual fun platformCreateHTTPRequest(url: String): RemoteRequest {
    return HTTPRequest(url)
}
