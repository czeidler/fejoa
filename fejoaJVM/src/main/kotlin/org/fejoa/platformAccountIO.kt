package org.fejoa

import kotlinx.serialization.json.Json
import org.fejoa.support.PathUtils
import java.io.*


actual fun platformGetAccountIO(type: AccountIO.Type, context: String, namespace: String): AccountIO {
    return JVMAccountIO(context, namespace)
}


class JVMAccountIO(val context: String, namespace: String) : AccountIO {
    val namespace = PathUtils.toPath(namespace)

    private fun accountDir(path: String, namespace: String): File {
        return File(PathUtils.appendDir(path, namespace))
    }

    private fun authDataFile(accountDir: File): File {
        return File(accountDir, "AuthData.json")
    }

    private fun userDataSettingsFile(accountDir: File): File {
        return File(accountDir, "UserDataConfig.json")
    }

    private fun storageConfigFile(accountDir: File): File {
        return File(accountDir, "StorageConfig.json")
    }

    private fun migrationConfigFile(accountDir: File): File {
        return File(accountDir, "MigrationConfig.json")
    }

    private fun userInfoFile(accountDir: File): File {
        return File(accountDir, "UserInfo.json")
    }

    override suspend fun exists(): Boolean {
        val dir = accountDir(context, namespace)
        if (!dir.exists())
            return false

        if (authDataFile(dir).exists())
            return true
        if (userDataSettingsFile(dir).exists())
            return true

        return false
    }

    override suspend fun writeLoginData(loginData: LoginParams) {
        val dir = accountDir(context, namespace)
        dir.mkdirs()
        val fileName = authDataFile(dir)
        val writer = BufferedWriter(OutputStreamWriter(FileOutputStream(fileName)))
        writer.write(Json(indented = true).stringify(LoginParams.serializer(), loginData))
        writer.close()
    }

    override suspend fun readLoginData(): LoginParams {
        val fileName = authDataFile(accountDir(context, namespace))
        val input = BufferedReader(InputStreamReader(FileInputStream(fileName)))
        val json = input.readText()
        input.close()
        return Json.parse(LoginParams.serializer(), json)
    }


    override suspend fun writeUserDataConfig(userDataConfig: UserDataConfig) {
        val dir = accountDir(context, namespace)
        dir.mkdirs()
        val fileName = userDataSettingsFile(dir)
        val writer = BufferedWriter(OutputStreamWriter(FileOutputStream(fileName)))
        writer.write(Json(indented = true).stringify(UserDataConfig.serializer(), userDataConfig))
        writer.close()
    }

    override suspend fun readUserDataConfig(): UserDataConfig {
        val dir = accountDir(context, namespace)
        val fileName = userDataSettingsFile(dir)
        val input = BufferedReader(InputStreamReader(FileInputStream(fileName)))
        val json = input.readText()
        input.close()
        return Json.parse(UserDataConfig.serializer(), json)
    }

    override suspend fun writeStorageConfig(storageConfig: StorageConfig) {
        val dir = accountDir(context, namespace)
        dir.mkdirs()
        val fileName = storageConfigFile(dir)
        val writer = BufferedWriter(OutputStreamWriter(FileOutputStream(fileName)))
        writer.write(Json(indented = true).stringify(StorageConfig.serializer(), storageConfig))
        writer.close()
    }

    override suspend fun deleteStorageConfig() {
        val dir = accountDir(context, namespace)
        val fileName = storageConfigFile(dir)
        fileName.delete()
    }

    override suspend fun readStorageConfig(): StorageConfig {
        val dir = accountDir(context, namespace)
        val fileName = storageConfigFile(dir)
        val input = BufferedReader(InputStreamReader(FileInputStream(fileName)))
        val json = input.readText()
        input.close()
        return Json.parse(StorageConfig.serializer(), json)
    }

    override suspend fun writeMigrationConfig(config: MigrationConfig) {
        val dir = accountDir(context, namespace)
        dir.mkdirs()
        val fileName = migrationConfigFile(dir)
        val writer = BufferedWriter(OutputStreamWriter(FileOutputStream(fileName)))
        writer.write(Json(indented = true).stringify(MigrationConfig.serializer(), config))
        writer.close()
    }

    override suspend fun readMigrationConfig(): MigrationConfig? {
        val dir = accountDir(context, namespace)
        val fileName = migrationConfigFile(dir)
        return try {
            val input = BufferedReader(InputStreamReader(FileInputStream(fileName)))
            val json = input.readText()
            input.close()
            Json.parse(MigrationConfig.serializer(), json)
        } catch (e: Exception) {
            null
        }
    }

    override suspend fun writeServerUserInfo(info: ServerUserInfo) {
        val dir = accountDir(context, namespace)
        dir.mkdirs()
        val fileName = userInfoFile(dir)
        val writer = BufferedWriter(OutputStreamWriter(FileOutputStream(fileName)))
        writer.write(Json(indented = true).stringify(ServerUserInfo.serializer(), info))
        writer.close()
    }

    override suspend fun readServerUserInfo(): ServerUserInfo? {
        val dir = accountDir(context, namespace)
        val fileName = userInfoFile(dir)
        return try {
            val input = BufferedReader(InputStreamReader(FileInputStream(fileName)))
            val json = input.readText()
            input.close()
            Json.parse(ServerUserInfo.serializer(), json)
        } catch (e: Exception) {
            null
        }
    }
}
