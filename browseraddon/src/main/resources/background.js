requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: 'js',

    paths: { // paths are relative to this file
        'big-integer': 'libs/BigInteger.min',
        'pako': 'libs/pako.min',
        'zstd-codec': 'libs/bundle'
    },

    deps: ['big-integer', 'pako', 'zstd-codec', 'browseraddon'],

    callback: onPackagesLoaded
});

function onPackagesLoaded(biginteger, pako, zstdcodec, browseraddon) {
    ZstdCodec.run(zstd => {
        window.zstdCodecSimple = new zstd.Simple();

        // Simply store the loaded module so that it can be used in the popup window
        window.browseraddon = browseraddon;
        browseraddon.org.fejoa.auth.ui.initializeFejoaAuth();
    });
}