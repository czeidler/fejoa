chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.method == "getPageInfo") {
            getPageInfo(sendResponse);
        } else if (request.method == "fillRegisterPassword") {
            fillRegisterPassword(request.password);
        } else if (request.method == "updateLoginStatus") {
            updateLoginStatus(request);
        } else if (request.method == "updatePendingCredentials") {
            if (request.hasPendingCredentials)
                showStorePWNotification();
            else
                hideStorePWNotification();
        } else {
            sendResponse({status: -1, message: "invalid request"});
        }
    });

function PortalInfo(status, message = "", path = "") {
    this.status = status;
    this.message = message;
    this.path = path;
}

function PageInfo(portalInfo, passwordFormType) {
    this.portalInfo = portalInfo;
    this.passwordFormType = passwordFormType;
}

var getPageInfo = function(sendResponse) {
    var portalInfo = getPortalInfo();
    var passwordFormType = getPasswordFormType();
    var pageInfo = new PageInfo(portalInfo, passwordFormType);
    sendResponse(pageInfo);
}

var getPortalInfo = function() {
    var fejoaAuthDiv = document.getElementById("fejoa-portal");
    if (fejoaAuthDiv == null)
        return new PortalInfo(-1, "fejoa-auth element not found");

    var path = fejoaAuthDiv.getAttribute("data-path")
    if (path == null)
        return new PortalInfo(-1, "data-path element not set");

    return new PortalInfo(0, "", path);
}

var getPasswordFormType = function() {
    for (var i = 0; i < document.forms.length; i++) {
        var form = document.forms[i];
        var fields = PasswordFields.getPasswordFields(form, false);
        if (!fields) {
            return "none";
        } else if (fields.length == 1) {
            return "login";
        } else if (fields.length == 2) {
            return "register";
        } else if (fields.length == 3) {
            return "new_password";
        }
    }
    return "none";
}

var fillRegisterPassword = function(password) {
    for (var i = 0; i < document.forms.length; i++) {
        var form = document.forms[i];
        var fields = PasswordFields.getPasswordFields(form, false);
        if (!fields || fields.length < 2)
            return;
        // Fill the last to password fields. This assumes that if there are 3 field it is a "change password" page
        // and the first password field is the old password
        fields[fields.length - 1].element.value = password;
        fields[fields.length - 2].element.value = password;
    }
}

var updateLoginStatus = function(request) {
    var origin = request.origin;
    var user = request.user;
    var authStatus = request.authStatus;
    window.postMessage({
            type: "fejoa_auth_update",
            origin: origin,
            user: user,
            authStatus: authStatus
        }, "*");
}

var notifyNewCredentials = function(form) {
    var [usernameField, newPasswordField, oldPasswordField] = PasswordFields.getFormFields(form, true);
    if (usernameField != null && newPasswordField != null) {
        // store credentials
        chrome.runtime.sendMessage({
            method: "newCredentials",
            user: usernameField.value,
            password: newPasswordField.value
        });
    }
}

// init password manager functionality
var initPasswordManager = function() {
    window.addEventListener("onbeforeunload", function() {
        for (var i = 0; i < document.forms.length; i++) {
            var form = document.forms[i];
            notifyNewCredentials(form);
        }
    })
    for (var i = 0; i < document.forms.length; i++) {
        document.forms[i].addEventListener("submit", function() {
            var form = this;
            notifyNewCredentials(form);
        });

        var [usernameField, newPasswordField, oldPasswordField] = PasswordFields.getFormFields(document.forms[i],
            false);
        if (usernameField != null && newPasswordField != null && oldPasswordField == null) {
            // listen for username input
            usernameField.addEventListener('input', function () {
                // request credentials from the password manager and fill them in
                chrome.runtime.sendMessage({
                        method: "getCredentials",
                        user: usernameField.value
                    }, function(response) {
                    if (response == undefined)
                        return;
                    var user = response.user
                    var password = response.password;
                    if (password != "" && user == usernameField.value)
                        newPasswordField.value = password;
                });
            });
        }
    }
}

initPasswordManager();

// check for pending notifications
window.addEventListener("load", function() {
    chrome.runtime.sendMessage({method: "getCredentialsInfo"}, function(response) {
        if (response.hasPendingCredentials)
            showStorePWNotification();
    });
});


// store password notification dialog
var storePWNotificationId = "fejoa-auth-store-pw-notification";

var showStorePWNotification = function() {
    if (document.getElementById(storePWNotificationId) != null)
        return;
    var url = chrome.extension.getURL("StorePasswordNotification.html");
    var iframe = document.createElement('iframe');
    iframe.id = storePWNotificationId;
    iframe.src = url;
    iframe.style.position = "fixed";
    iframe.style.zIndex = 2147483647;
    iframe.style.top = "5px";
    iframe.style.right = "10px";
    iframe.style.maxWidth = "350px"
    iframe.style.maxHeight = "120px"
    iframe.style.boxShadow="0px 0px 3px"
    iframe.height = "100%";
    iframe.width = "100%";
    iframe.display = "block";
    iframe.frameborder = "0";
    document.body.appendChild(iframe);
}

var hideStorePWNotification = function() {
    var element = document.getElementById(storePWNotificationId);
    if (element == null)
        return;
    element.remove();
}
