package org.fejoa.auth

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.crypto.CryptoHelper
import org.fejoa.crypto.CryptoSettings
import org.fejoa.crypto.SecretKey
import org.fejoa.support.Future
import org.w3c.dom.Worker
import org.w3c.dom.events.Event
import kotlin.js.json

class KDFWorkerClient {
    private val worker: Worker = Worker("KDFWorker.js")

    fun deriveKey(password: String, salt: ByteArray, settings: CryptoSettings.KDF): Future<SecretKey> {
        val future = Future<SecretKey>()
        worker.onmessage = { event: Event ->
            val data = event.asDynamic().data
            val encodedKey = data.key.unsafeCast<ByteArray>()
            GlobalScope.launch {
                val key = CryptoHelper.crypto.secretKeyFromRaw(encodedKey, settings.keyAlgo)
                future.setResult(key)
            }
        }
        worker.onerror = {
            future.setError(Exception("KDF worker failed"))
        }

        worker.postMessage(json("password" to password,
                "salt" to salt,
                "settings" to settings))

        future.whenCompleted { _, error ->
            if (error is Future.CancelationException)
                worker.terminate()
        }

        return future
    }
}

external var onmessage: (message: dynamic) -> Unit
external var self: dynamic

class KDFWorker {
    init {
        onmessage = { message -> handle(message) }
    }

    fun handle(message: dynamic) {
        if (message == null)
            return
        GlobalScope.launch {
            val crypto = CryptoHelper.crypto
            val key = crypto.deriveKey(message.data.password, message.data.salt, message.data.settings)
            // send the encoded key / when sending the key directly all type information is lost
            val encoded = crypto.encode(key)
            self.postMessage(json("key" to encoded))
            Unit
        }
    }
}
