package org.fejoa.auth.ui

import org.fejoa.PasswordGetter
import org.fejoa.support.Future


class PasswordRequestManager {
    class PWRequest(val password: Future<String>, val purpose: PasswordGetter.Purpose, val resource: String,
                    val info: String)

    val requests: MutableList<PWRequest> = ArrayList()
    var currentRequest: PWRequest? = null
        private set

    val updateListeners: MutableList<() -> Unit> = ArrayList()

    fun startRequest(purpose: PasswordGetter.Purpose, resource: String, info: String): Future<String> {
        val future = Future<String>()
        val request = PWRequest(future, purpose, resource, info)
        requests += request
        if (currentRequest == null)
            currentRequest = request

        future.whenCompleted { _, _ ->
            requests.remove(request)
            if (currentRequest == request)
                currentRequest = if (requests.isNotEmpty()) requests[0] else null
            notifyListeners()
        }

        notifyListeners()
        return future
    }

    private fun notifyListeners() {
        updateListeners.forEach { it.invoke() }
    }
}
