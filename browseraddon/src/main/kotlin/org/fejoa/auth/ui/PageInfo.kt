package org.fejoa.auth.ui

import kotlinx.coroutines.await
import org.fejoa.jsbindings.chrome
import org.w3c.dom.url.URL
import kotlin.js.Json
import kotlin.js.Promise
import kotlin.js.json
import kotlin.properties.Delegates


class PageInfoManager {
    var pageInfo: Pair<PageInfo?, String?> by Delegates.observable<Pair<PageInfo?, String?>>(
            null to "Page info not updated") { _, _, _ ->
        onPageInfoChanged.forEach { it.invoke() }
    }

    var onPageInfoChanged = ArrayList<(() -> Unit)>()

    fun hasFejoaAuth(): Boolean {
        return pageInfo.first != null
    }

    suspend fun updatePageInfo() {
        pageInfo = PageInfo.getCurrentPage().await()
    }
}


class PageInfo(val url: URL, private val authPath: String, val passwordFormType: String) {
    fun getFejoaAuthPath(): String {
        // The authPath must be relative to the current window origin to avoid cross side attacks.
        // For example, a malicious site could add a link to an external FejoaAuth page and monitor the auth status of
        // a user. By always using the window's origin only the windows with the same origin get notified about the auth
        // status.
        return "${url.origin}/$authPath"
    }

    fun getFejoaAuthPathOrigin(): String {
        // must be the url origin (the url of the current window) see getFejoAuthPath comment...
        return url.origin
    }

    companion object {
        fun getCurrentPage(): Promise<Pair<PageInfo?, String?>> {
            return Promise { resolve, _ ->
                chrome.tabs.query(json("active" to true)) { tabArray ->
                    val tab = tabArray.firstOrNull() ?: return@query resolve(null to "No active tab")
                    val tabId = tab.id ?: return@query resolve(null to "No tab id")
                    if (tabId < 0)
                        return@query resolve(null to "No to selected")

                    chrome.tabs.sendMessage(tabId, json("method" to "getPageInfo"), null) { response ->
                        try {
                            if (response == null)
                                return@sendMessage resolve(null to "Failed to query content page")
                            // portal info
                            val portalInfo = response["portalInfo"]?.unsafeCast<Json>()
                                    ?: return@sendMessage resolve(null to "No portal info")
                            val status = portalInfo["status"] ?: return@sendMessage resolve(null to "Failed to query content page")
                            if (status != 0) {
                                val message = portalInfo["message"]?.unsafeCast<String>() ?: "Unknown error"
                                return@sendMessage resolve(null to message)
                            }
                            val path = portalInfo["path"]?.unsafeCast<String>() ?: return@sendMessage resolve(null to "Fejoa auth portal path missing")

                            // tab url
                            val url = tab.url ?: return@sendMessage resolve(null to "Tab has no url")

                            // password form info
                            val passwordFormType = response["passwordFormType"]?.unsafeCast<String>()
                                    ?: return@sendMessage resolve(null to "No passwordFormType info")

                            // send result
                            resolve(PageInfo(URL(url), path, passwordFormType) to null)
                        } catch (e: dynamic) {
                            resolve(null to (e.message ?: e))
                        }
                    }
                }
            }
        }
    }
}