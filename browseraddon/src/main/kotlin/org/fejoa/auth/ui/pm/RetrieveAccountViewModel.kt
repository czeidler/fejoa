package org.fejoa.auth.ui.pm

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.auth.FejoaAuthClient
import org.fejoa.auth.passwordmanager.PMAccountManager
import org.fejoa.auth.ui.AuthManager
import org.fejoa.auth.ui.Log
import org.fejoa.auth.ui.pm.RetrieveAccountView.pmRetrieveButtonId
import org.w3c.dom.*
import kotlin.properties.Delegates


class RetrieveAccountViewModel(window: Window, client: FejoaAuthClient, accountManager: PMAccountManager, log: Log) {
    val document = window.document
    val authManager = client.authManager

    val remoteDropdownButton = document.getElementById("pm-retrieve-remote-dropdown-button")
            .unsafeCast<HTMLButtonElement>()
    val remoteDropdown = document.getElementById("pm-remote-retrieve-dropdown").unsafeCast<HTMLUListElement>()
    val accountNameInput = document.getElementById("pm-retrieve-account-name").unsafeCast<HTMLInputElement>()
    val retrieveButton = document.getElementById(pmRetrieveButtonId).unsafeCast<HTMLButtonElement>()
    val selectedAccountSpan = document.getElementById("pm-retrieve-selected-account")
            .unsafeCast<HTMLSpanElement>()
    var isDead = false

    private var selectedAuthStatus: AuthManager.AuthStatus? by Delegates
            .observable<AuthManager.AuthStatus?>(null) { _, _, newValue ->
                if (isDead) return@observable

                if (newValue == null) {
                    selectedAccountSpan.hidden = true
                    selectedAccountSpan.innerText = ""
                } else {
                    selectedAccountSpan.hidden = false
                    selectedAccountSpan.innerText = "${newValue.user}@${newValue.url}"
                }
    }

    private val statusListener = AuthManager.StatusListener("*", {
        updateRemoteDropDown()
    })

    init {
        authManager.statusListeners += statusListener

        window.addEventListener("unload", {
            isDead = true
            authManager.statusListeners.remove(statusListener)
        })

        reset()
        updateRemoteDropDown()

        retrieveButton.onclick = {
            val accountName = accountNameInput.value
            enableUI(false)
            GlobalScope.launch {
                try {
                    accountManager.retrieveAccount(accountName, selectedAuthStatus!!.user, selectedAuthStatus!!.url)
                    reset()
                } catch (e: Exception) {
                    log.status("${e.message}")
                }
            }
        }

        accountNameInput.oninput = { _ -> validate()}
    }

    private fun reset() {
        selectedAccountSpan.hidden = true
        retrieveButton.disabled = true
        accountNameInput.value = ""
        selectedAuthStatus = null
        accountNameInput.disabled = false
        remoteDropdownButton.disabled = false
    }

    private fun enableUI(enable: Boolean) {
        retrieveButton.disabled = enable
        accountNameInput.disabled = enable
        remoteDropdownButton.disabled = enable
    }

    private fun validate() {
        retrieveButton.disabled = true
        if (accountNameInput.value.isBlank())
            return
        if (selectedAuthStatus == null)
            return

        retrieveButton.disabled = false
    }

    private fun updateRemoteDropDown() {
        selectedAuthStatus = null

        // repopulate the drop down
        while (remoteDropdown.firstChild != null)
            remoteDropdown.removeChild(remoteDropdown.firstChild!!)

        // fill logged in remotes
        authManager.getAutStatusList()
                .filter { it.second.status == AuthManager.Status.LOGGED_IN }
                .forEach {
                    val authInfo = it.second
                    val li = document.createElement("li").unsafeCast<HTMLLIElement>()
                    li.onclick = {
                        selectedAuthStatus = authInfo
                        validate()
                        Unit
                    }

                    val ref = document.createElement("a").unsafeCast<HTMLLinkElement>()
                    ref.href = "#"
                    ref.innerText = "${authInfo.user}@${authInfo.url}"
                    li.appendChild(ref)
                    remoteDropdown.appendChild(li)
                }
    }
}