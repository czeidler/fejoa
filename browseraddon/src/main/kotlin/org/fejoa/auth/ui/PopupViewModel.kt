package org.fejoa.auth.ui

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.auth.FejoaAuthClient
import org.fejoa.auth.ui.pm.PMView
import org.fejoa.auth.ui.pm.PMViewModel
import org.fejoa.jsbindings.chrome
import org.w3c.dom.*


/**
 * Start the main popup and checks if the page supports fejoa auth
 */
class PopupViewModel(val fejoaAuthClient: FejoaAuthClient, val window: Window) {
    val pageInfoManager = PageInfoManager()
    val document = window.document
    val log = LogViewModel(window)

    val navBar = NavBar()

    private fun onPageInfoChanged() {
        pageInfoManager.pageInfo.first?.let {
            navBar.view = NavBar.VIEW.AUTH
            GlobalScope.launch {
                FejoaAuthViewModel(window, fejoaAuthClient.authManager, it, log).start()
            }
        }
        pageInfoManager.pageInfo.second?.let {
            navBar.view = NavBar.VIEW.PASSWORD_MANAGER
        }
    }

    fun start() {
        NavBarViewModel(window, navBar, pageInfoManager)
        pageInfoManager.onPageInfoChanged.add(this::onPageInfoChanged)
        GlobalScope.launch {
            pageInfoManager.updatePageInfo()
        }

        // password request
        PasswordRequestViewModel(window, fejoaAuthClient.pwRequestManager)

        // password manager
        PMViewModel(window, fejoaAuthClient, pageInfoManager, log)
    }
}

private fun setupView(document: Document) {
    val mainContainer = document.getElementById("main-container").unsafeCast<HTMLDivElement>()

    // status view
    mainContainer.appendChild(StatusView.build())

    // Password Request
    mainContainer.appendChild(buildPasswordRequestView())
    // Fejoa auth
    mainContainer.appendChild(buildFejoaAuthView())
    // password manager
    mainContainer.appendChild(PMView.build())
}


private fun ready(window: Window, block: () -> Unit) {
    val document = window.document
    if (document.readyState == DocumentReadyState.COMPLETE)
        block.invoke()
    else
        window.addEventListener("DOMContentLoaded", { block.invoke() })
}

fun initializeFejoaAuth(): Window {
    val bgWindow = chrome.extension.getBackgroundPage().unsafeCast<dynamic>()
    if (bgWindow.fejoaAuthClient == undefined) {
        bgWindow.fejoaAuthClient = FejoaAuthClient()
    }
    return bgWindow.unsafeCast<Window>()
}

fun initPopup(window: Window) {
    val bgWindow = initializeFejoaAuth().unsafeCast<dynamic>()
    window.unsafeCast<dynamic>().bgWindow = bgWindow

    ready(window) {
        setupView(window.document)

        val fejoaAuthClient = bgWindow.fejoaAuthClient.unsafeCast<FejoaAuthClient>()
        val popup = PopupViewModel(fejoaAuthClient, window)
        popup.start()
    }
}