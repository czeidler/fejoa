package org.fejoa.auth.ui.pm

import kotlinx.html.*
import org.fejoa.auth.ui.buildBenchmarkView


fun buildCreateAccountView(parent: DIV) {
    parent.apply {
        // account name
        div("form-group") {
            label {
                attributes += "for" to "pm-create-account-name"
                +"Account name"
            }
            input(classes = "form-control") {
                id = "pm-create-account-name"
                placeholder = "Account Name"
            }
        }
        // password 1
        div("form-group") {
            label {
                attributes += "for" to "pm-account-password"
                +"Password"
            }
            input(classes = "form-control") {
                id = "pm-account-password"
                attributes += "type" to "password"
                type = InputType.password
                placeholder = "Password"
            }
        }
        // password 2
        div("form-group") {
            label {
                attributes += "for" to "pm-account-password2"
                +"Retype Password"
            }
            input(classes = "form-control") {
                id = "pm-account-password2"
                attributes += "type" to "password"
                type = InputType.password
                placeholder = "Password"
            }
        }

        buildBenchmarkView(this, "pm-")

        button(classes = "btn btn-primary") {
            id = "pm-create-button"
            +"Create"
        }
    }
}
