package org.fejoa.auth.ui

import kotlinx.html.*
import kotlinx.html.dom.create
import kotlinx.html.js.div
import org.fejoa.PasswordGetter
import org.w3c.dom.*
import kotlin.browser.document
import kotlin.dom.addClass
import kotlin.dom.removeClass


private const val pwRequestContainer = "password-request-container"
private const val pwRequestLabel = "password-request-label"
private const val pwRequestUrlSpan = "password-request-url"
private const val pwRequestOkButton = "password-request-ok"
private const val pwRequestCancelButton = "password-request-cancel"
private const val pwRequestInput = "password-request-input"


fun buildPasswordRequestView(): HTMLElement {
    return document.create.div("password-request-container collapse form-group") {
        id = pwRequestContainer

        span(classes = "label label-primary") {
            id = pwRequestUrlSpan
            +"displayurl.org"
        }

        div("form-group") {
            label {
                id = pwRequestLabel
                attributes += "for" to pwRequestInput
                +"Password"
            }
            input(classes = "form-control") {
                id = pwRequestInput
                type = InputType.password
            }
        }
        div("form-group") {
            button(classes = "btn btn-primary btn-sm") {
                id = pwRequestOkButton
                attributes += "data-toggle" to "collapse"
                attributes += "data-target" to "#$pwRequestContainer"
                +"Ok"
            }

            button(classes = "btn btn-sm") {
                id = pwRequestCancelButton
                attributes += "data-toggle" to "collapse"
                attributes += "data-target" to "#$pwRequestContainer"
                +"Cancel"
            }
        }
    }
}

class PasswordRequestViewModel(val window: Window, val model: PasswordRequestManager) {
    val document = window.document
    val remotePasswordContainer = document.getElementById(pwRequestContainer).unsafeCast<HTMLDivElement>()
    val heading = document.getElementById(pwRequestLabel).unsafeCast<HTMLHeadingElement>()
    val urlSpan = document.getElementById(pwRequestUrlSpan).unsafeCast<HTMLSpanElement>()
    val okButton = document.getElementById(pwRequestOkButton).unsafeCast<HTMLButtonElement>()
    val cancelButton = document.getElementById(pwRequestCancelButton)
            .unsafeCast<HTMLButtonElement>()
    val input = document.getElementById(pwRequestInput).unsafeCast<HTMLInputElement>()

    private val updateListener: (() -> Unit) = {
        updateView()
    }

    init {
        model.updateListeners += updateListener
        window.addEventListener("unload", {
            model.updateListeners.remove(updateListener)
        })

        okButton.onclick = {
            model.currentRequest?.let {
                it.password.setResult(input.value)
            }
        }
        cancelButton.onclick = {
            model.currentRequest?.let {
                it.password.setError(Exception("Canceled by user"))
            }
        }
        updateView()
    }

    private fun updateView() {
        val current = model.currentRequest ?: return

        // show
        remotePasswordContainer.removeClass("collapse")
        // somehow there is a style="height: 0px;" that needs to be removed
        remotePasswordContainer.removeAttribute("style")
        remotePasswordContainer.addClass("collapse in")

        input.value = ""

        heading.innerText = when(current.purpose) {
            PasswordGetter.Purpose.SERVER_LOGIN -> "Login password required:"
            PasswordGetter.Purpose.OPEN_ACCOUNT -> "Account password required:"
            PasswordGetter.Purpose.OTHER -> TODO()
        }
        urlSpan.innerText = current.resource
    }
}
