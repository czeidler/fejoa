package org.fejoa.auth.ui

import kotlinx.html.*


fun buildBenchmarkView(parent: DIV, prefix: String) {
    parent.apply {
        div("form-group") {
            button(classes = "btn btn-info btn-xs") {
                attributes += "data-toggle" to "collapse"
                attributes += "data-target" to "#${prefix}security-container"
                +"Security settings"
            }
            div("collapse security-container") {
                id = "${prefix}security-container"
                div("form-group row") {
                    label("col-xs-5 fejoa-text") {
                        attributes += "for" to "${prefix}kdf-algo"
                        +"KDF Algorithm"
                    }
                    div("col-xs-7") {
                        select("form-control") {
                            id = "${prefix}kdf-algo"
                            option {
                                value = "PBKDF2"
                                +"PBKDF2"
                            }
                        }
                    }
                }
                div("form-group row") {
                    label("col-xs-5 fejoa-text") {
                        attributes += "for" to "${prefix}n-iterations"
                        +"KDF Iterations"
                    }

                    div("col-xs-7") {
                        input(classes = "form-control") {
                            id = "${prefix}n-iterations"
                            type = InputType.number
                            value = "1000000"
                            min = "100"
                            max = "1000000000"
                        }
                    }
                }

                div("form-group row") {
                    div("col-xs-5") {
                        label("fejoa-text") {
                            +"Benchmark"
                        }
                    }
                    div("col-xs-7") {
                        button(classes = "btn") {
                            id = "${prefix}benchmark-button"
                            +"Start"
                        }
                        button(classes = "btn") {
                            id = "${prefix}benchmark-cancel-button"
                            +"Cancel"
                        }
                    }
                }
                div("form-group row") {
                    div("col-xs-12") {
                        span("col-form-label") {
                            id = "${prefix}benchmark-result"
                        }
                    }
                }
            }
        }
    }
}
