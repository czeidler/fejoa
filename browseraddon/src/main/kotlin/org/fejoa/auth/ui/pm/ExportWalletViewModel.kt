package org.fejoa.auth.ui.pm

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.auth.passwordmanager.PMAccountManager
import org.fejoa.auth.passwordmanager.toJson
import org.fejoa.jsbindings.chrome
import org.fejoa.support.PathUtils
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.Window
import org.w3c.dom.url.URL
import org.w3c.files.Blob
import org.w3c.files.BlobPropertyBag
import kotlin.js.json


class ExportWalletModel(val accountManager: PMAccountManager) {
    val listeners: MutableList<() -> Unit> = ArrayList()

    val onGoingExportsAccounts: MutableList<String> = ArrayList()

    private val accountListener = object : PMAccountManager.Listener {
        override fun onAccountListUpdated() {

        }

        override fun onAccountSelected(account: String?) {
            notifyListeners()
        }
    }

    private fun notifyListeners() {
        listeners.forEach { it.invoke() }
    }

    fun isSelectedAccountExporting(): Boolean {
        val selectedAccount = accountManager.getSelectedAccount() ?: return false
        return onGoingExportsAccounts.contains(selectedAccount.name)
    }

    fun exportSelectedAccount() {
        val selectedAccount = accountManager.getSelectedAccount() ?: return
        if (onGoingExportsAccounts.contains(selectedAccount.name))
            return
        val wallet = selectedAccount.account?.getDefaultWallet() ?: return
        onGoingExportsAccounts += selectedAccount.name
        notifyListeners()
        GlobalScope.launch {
            val walletJson = wallet.toJson()
            val url = URL.createObjectURL(Blob(arrayOf(walletJson), BlobPropertyBag("octet/stream")))
            chrome.downloads.download(json("url" to  url,
                    "filename" to "wallet_${PathUtils.toPath(selectedAccount.name)}.json"))

            onGoingExportsAccounts -= selectedAccount.name
            notifyListeners()
        }
    }

    init {
        accountManager.accountListeners += accountListener
    }
}

class ExportWalletViewModel(val window: Window,  val model: ExportWalletModel) {
    val document = window.document

    val exportButton = document.getElementById(pmExportWalletButtonId).unsafeCast<HTMLButtonElement>()

    private val modelListener = {
        update()
    }

    init {
        model.listeners += modelListener
        window.addEventListener("unload", {
            model.listeners.remove(modelListener)
        })

        exportButton.onclick = {
            model.exportSelectedAccount()
        }
        update()
    }

    private fun update() {
        exportButton.disabled = model.isSelectedAccountExporting()
    }
}