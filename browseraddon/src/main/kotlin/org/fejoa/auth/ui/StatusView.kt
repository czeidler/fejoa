package org.fejoa.auth.ui

import kotlinx.html.div
import kotlinx.html.dom.create
import kotlinx.html.hidden
import kotlinx.html.id
import kotlinx.html.js.div
import org.w3c.dom.HTMLDivElement
import kotlin.browser.document


object StatusView {
    const val statusViewId = "status-message"
    const val errorViewId = "error-message"

    fun build(): HTMLDivElement {
        return document.create.div {
            // status view
            div("alert alert-info") {
                id = statusViewId
                attributes["role"] = "alert"
                hidden = true
            }
            // error view
            div("alert alert-danger") {
                id = errorViewId
                attributes["role"] = "alert"
                hidden = true
            }
        }
    }
}
