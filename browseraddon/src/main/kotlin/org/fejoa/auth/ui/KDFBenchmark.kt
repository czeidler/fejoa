package org.fejoa.auth.ui

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.auth.KDFWorkerClient
import org.fejoa.crypto.*
import org.fejoa.support.Future
import org.fejoa.support.await
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLSpanElement
import org.w3c.dom.Window
import kotlin.js.Date


class KDFBenchmark {
    class Result(val time: Double? = null, val nIterations: Int = 0, val future: Future<SecretKey>? = null)

    var result = Result()
        private set

    val listeners = ArrayList<() -> Unit>()

    private fun updateResult(newResult: Result) {
        result = newResult
        listeners.forEach { it.invoke() }
    }

    fun benchmark(kdfAlgo: CryptoSettings.KDF_ALGO, nIterations: Int) {
        val salt = CryptoHelper.crypto.generateSalt16()
        val params = when (kdfAlgo) {
            CryptoSettings.KDF_ALGO.PBKDF2_SHA256 -> {
                val kdf = CryptoSettings.default.kdf
                kdf.algo = CryptoSettings.KDF_ALGO.PBKDF2_SHA256
                kdf.params.iterations = nIterations
                BaseKeyParams(kdf, salt)
            }
        }
        GlobalScope.launch {
            val startTime = Date().getTime()
            val worker = KDFWorkerClient()
            val result = worker.deriveKey("password", params.getSalt(), params.kdf)
            updateResult(Result(future = result))
            try {
                result.await()
                updateResult(Result(time = Date().getTime() - startTime, nIterations = nIterations))
            } catch (e: dynamic) {
                // canceled
                updateResult(Result())
            }
        }
    }
}

class KDFBenchmarkViewModel(val window: Window, val benchmark: KDFBenchmark, val kdfConfig: KDFConfigViewModel,
                            uiElementPrefix: String = "") {
    val document = window.document

    val resultView = document.getElementById("${uiElementPrefix}benchmark-result").unsafeCast<HTMLSpanElement>()
    val benchmarkButton = document.getElementById("${uiElementPrefix}benchmark-button").unsafeCast<HTMLButtonElement>()
    val cancelButton = document.getElementById("${uiElementPrefix}benchmark-cancel-button").unsafeCast<HTMLButtonElement>()

    init {
        benchmark.listeners.add(this::onResultUpdated)

        benchmarkButton.addEventListener("click", {
            val algo = kdfConfig.getKdfAlgo()
            val nIterations = kdfConfig.getNIterations()

            benchmark.benchmark(algo, nIterations)
        })

        cancelButton.addEventListener("click", {
            benchmark.result.future?.cancel()
        })

        onResultUpdated()
    }

    fun onResultUpdated() {
        val result = benchmark.result
        if (result.future != null) {
            // running
            resultView.innerText = "calculating..."
            benchmarkButton.disabled = true
            cancelButton.disabled = false
        } else {
            benchmarkButton.disabled = false
            cancelButton.disabled = true

            if (result.time != null)
                resultView.innerText = "${result.time} ms for ${result.nIterations} iterations"
            else
                resultView.innerText = "-"
        }
    }
}
