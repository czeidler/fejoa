package org.fejoa.auth.passwordmanager

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fejoa.*
import org.fejoa.auth.ui.pm.SyncModel
import org.fejoa.crypto.CryptoSettings
import org.fejoa.crypto.generateSecretKeyData
import org.fejoa.network.GetServerConfigJob
import org.fejoa.network.ReturnType
import org.fejoa.network.UpdateServerConfigJob
import org.fejoa.storage.HashValue
import org.fejoa.support.await


class PMAccount(val client: Client, val name: String) {
    val userData = client.userData
    private val wallets = ArrayList<Wallet>()
    val walletUpdatedListeners = ArrayList<() -> Unit>()
    val syncModel = SyncModel(client)
    // Reload the wallets a pm branch is synced. This ensures a wallet is loaded when an account is retrieved.
    private val walletStorageListener = object : Logger {
        override fun onEvent(category: Logger.Category, event: Logger.Event, level: Logger.LogLevel) {
            if (category != Logger.Category.INFO || event.type != Logger.TYPE.SYNC_MANAGER)
                return
            val data = event.data as SyncEvent
            if (data.branchType != SyncEvent.BranchType.BRANCH || data.status != SyncEvent.Status.SYNCED)
                return
            if (data.context == BRANCH_CONTEXT) GlobalScope.launch {
                reloadWallets()
            }
        }
    }

    companion object {
        const val BRANCH_CONTEXT = "org.fejoa.auth.pm"
    }

    init {
        client.logger.children.add(walletStorageListener)

        GlobalScope.launch {
            reloadWallets()
        }
    }

    fun dispose() {
        client.logger.children.remove(walletStorageListener)
        clearWallets()
    }

    private fun clearWallets() {
        wallets.clear()
    }

    private suspend fun reloadWallets() {
        clearWallets()

        client.withLooper {
            val branches = userData.getBranches(BRANCH_CONTEXT, false)
            branches.forEach {
                val storageDir = userData.getStorageDir(it)

                val wallet = Wallet(storageDir)
                wallets += wallet
            }
            notifyWalletUpdated()
        }
    }

    suspend fun close() {
        client.close()
    }

    private fun notifyWalletUpdated() {
        walletUpdatedListeners.forEach { it.invoke() }
    }

    suspend fun createWallet(): Wallet {
        val branchName = UserData.generateBranchName()

        val keyData = CryptoSettings.default.symmetric.generateSecretKeyData()
        val branch = userData.createStorageDir(BRANCH_CONTEXT, branchName.toHex(), "PM Wallet",
                keyData)
        val storageDir = userData.getStorageDir(branch)
        val wallet = Wallet(storageDir)
        addWallet(wallet)
        return wallet
    }

    // TODO: store a default wallet?
    fun getDefaultWallet(): Wallet? {
        return wallets.firstOrNull()
    }

    fun getWallet(id: String): Wallet? {
        return wallets.firstOrNull { it.getId() == id }
    }

    fun getWallets(): List<Wallet> {
        return wallets
    }

    private fun addWallet(wallet: Wallet) {
        wallets.add(wallet)
        notifyWalletUpdated()
    }

    private suspend fun collectSyncBranches(userData: UserData): List<Branch> {
        val list: MutableList<Branch> = ArrayList()

        val userDataBranches = userData.getBranches(UserData.USER_DATA_CONTEXT, true)
        list += userDataBranches
        list += userData.getBranches(PMAccount.BRANCH_CONTEXT, false)
        return list
    }

    suspend fun sync(remote: Remote) {
        client.withLooper {
            val branches = collectSyncBranches(userData)
            branches.forEach {
                syncModel.startSync(remote, it)
            }
        }
    }

    suspend fun addRemote(user: String, url: String) {
        val remote = Remote.create(user, url)

        // check if account exists user data
        val retrieveResult = client.connectionAuthManager.send(GetServerConfigJob(user), remote,
                LoginAuthInfo()).await()
        if (retrieveResult.code != ReturnType.OK)
            throw Exception(retrieveResult.message)
        if (retrieveResult.storageConfig != null)
            throw Exception("Remote already has a storage, delete existing storage first.")
        // upload new config
        val serverConfig = client.withLooper {
            userData.getServerConfig("", "")
        } ?: throw Exception("Failed to read user data config")
        val updateResult = client.connectionAuthManager.send(
                UpdateServerConfigJob(user, null, serverConfig), remote, LoginAuthInfo()).await()
        if (updateResult.code != ReturnType.OK)
            throw Exception(updateResult.message)

        client.withLooper {
            userData.remotes.get(HashValue.fromHex(remote.id)).write(remote)

            val syncBranches = collectSyncBranches(userData)
            syncBranches.forEach {
                it.updateRemote(remote, LoginAuthInfo())
            }

            userData.commit()

            restartSyncer()
        }
    }
}