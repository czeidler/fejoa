package org.fejoa.auth.passwordmanager


/**
 * Manages submitted password that are not stored in a wallet yet.
 *
 * For example, passwords that are submitted in web page but not confirmed to be stored in the wallet.
 */
class PendingPasswordManager {
    class Entry(val tabId: Int, val origin: String, val user: String, val password: String)

    private val entries: MutableMap<String, MutableList<Entry>> = HashMap()

    fun add(tabId: Int, origin: String, user: String, password: String) {
        ensureEntries(origin) += Entry(tabId, origin, user, password)
    }

    fun clear(origin: String) {
        entries.remove(origin)
    }

    fun getEntries(origin: String): List<Entry> {
        return entries[origin] ?: emptyList()
    }

    private fun ensureEntries(origin: String): MutableList<Entry> {
        return entries[origin] ?: ArrayList<Entry>().also {
            entries[origin] = it
        }
    }
}