package org.fejoa.auth.passwordmanager

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.fejoa.jsbindings.chrome
import org.fejoa.withLooper
import org.w3c.dom.url.URL
import kotlin.js.Json
import kotlin.js.json


class PMBackground(val accountManager: PMAccountManager) {
    val pendingPasswordManager = PendingPasswordManager()

    private val accountListener = object : PMAccountManager.Listener {
        override fun onAccountListUpdated() {
            chrome.tabs.query(json()) { result ->
                for (tab in result) {
                    val tabUrl = tab.url ?: continue
                    val origin = URL(tabUrl).origin

                    pendingPasswordManager.getEntries(origin).filter { it.tabId == tab.id }.forEach {
                        chrome.tabs.sendMessage(tab.id!!, json("method" to "accountListUpdated"))
                    }
                }
            }
        }

        override fun onAccountSelected(account: String?) {

        }
    }

    init {
        accountManager.accountListeners += accountListener

        chrome.runtime.onMessage.addListener { message, sender, senderResponse ->
            if (message == null)
                return@addListener false
            if (sender.url == null)
                return@addListener false
            val tab = sender.tab ?: return@addListener false
            val tabUrl = tab.url ?: return@addListener false
            val origin = URL(tabUrl).origin
            val requestName = message["method"] ?: return@addListener false
            if (requestName == "getCredentials") {
                val userName = message["user"].unsafeCast<String>()
                getCredentials(origin, userName, senderResponse)
                // the reply is async because we have to look up the password in the database
                return@addListener true
            } else if (requestName == "newCredentials") {
                val user = message["user"]?.unsafeCast<String>() ?: return@addListener false
                val password = message["password"]?.unsafeCast<String>() ?: return@addListener false
                if (user == "" || password == "")
                    return@addListener false

                pendingPasswordManager.add(tab.id!!, origin, user, password)
                senderResponse(json("method" to "showStorePWNotification", "show" to true))
            } else if (requestName == "getCredentialsInfo") {
                val entries = pendingPasswordManager.getEntries(origin).filter {
                    it.tabId == tab.id
                }
                val hasPendingCredentials = entries.isNotEmpty()
                if (!hasPendingCredentials) {
                    senderResponse(json("hasPendingCredentials" to false))
                    return@addListener false
                }
                val openAccounts = accountManager.getAccounts().filter {
                    it.type == PMAccountManager.AccountEntry.Type.OPEN
                }.map { it.name }.toTypedArray()

                senderResponse(json("hasPendingCredentials" to true,
                        "openAccounts" to openAccounts))

            } else if (requestName == "ignoreCredentials") {
                ignoreCredentials(origin, tab.id!!)
                notifyPendingCredentials(tab.id!!, false)
            } else if (requestName == "storeCredentials") {
                val account = message["account"]?.unsafeCast<String>() ?: return@addListener false
                storeCredentials(origin, tab.id!!, account)
                notifyPendingCredentials(tab.id!!, false)
            }
            return@addListener false
        }
    }

    private fun notifyPendingCredentials(tabId: Int, hasPendingCredentials: Boolean) {
        chrome.tabs.sendMessage(tabId, json("method" to "updatePendingCredentials",
                "hasPendingCredentials" to hasPendingCredentials))
    }

    private fun getCredentials(origin: String, userName: String, senderResponse: (arg: Json?) -> Unit) = GlobalScope.async {
        val defaultAccountEntry = accountManager.getDefaultAccount() ?: return@async
        val originCredentials = accountManager.getAccounts().filter {
            it.type == PMAccountManager.AccountEntry.Type.OPEN
        }.sortedWith(object: Comparator<PMAccountManager.AccountEntry> {
            override fun compare(a: PMAccountManager.AccountEntry, b: PMAccountManager.AccountEntry): Int {
                if (a == defaultAccountEntry)
                    return -1
                else
                    return 0
            }
        }).mapNotNull { it.account }.flatMap { account ->
            account.getWallets().map { it to account }
        }.mapNotNull {
            it.second.client.withLooper {
                it.first.getPassword(origin, userName)
            }
        }

        val userCredentials = originCredentials.firstOrNull { it.username == userName } ?: return@async
        val user = userCredentials.username
        val password = userCredentials.password
        senderResponse(json("user" to user, "password" to password))
    }

    fun ignoreCredentials(origin: String, tabId: Int) {
        pendingPasswordManager.clear(origin)
    }

    fun storeCredentials(origin: String, tabId: Int, account: String) {
        val accountEntry = accountManager.getAccounts().firstOrNull { it.name == account }
                ?: return
        val account = accountEntry.account?: return
        val wallet = account.getDefaultWallet() ?: return
        val entry = pendingPasswordManager.getEntries(origin).firstOrNull {
            it.tabId == tabId
        } ?: return
        pendingPasswordManager.clear(origin)
        if (entry.origin != origin)
            throw Exception("PMBackground: Origin mismatch")

        GlobalScope.launch {
            account.client.withLooper {
                wallet.putPassword(origin, entry.user, entry.password)
                wallet.commit()
            }
        }
    }

    fun fillRegisterPassword(password: String) {
        chrome.tabs.query(json("active" to true)) { tabArray ->
            val tab = tabArray.firstOrNull() ?: return@query
            if (tab.id == null)
                return@query
            chrome.tabs.sendMessage(tab.id!!, json("method" to "fillRegisterPassword", "password" to password))
        }
    }

}
