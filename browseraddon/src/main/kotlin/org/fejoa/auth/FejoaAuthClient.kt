package org.fejoa.auth

import kotlinx.coroutines.Dispatchers
import org.fejoa.AccountIO
import org.fejoa.ConnectionAuthManager
import org.fejoa.FejoaContext
import org.fejoa.auth.passwordmanager.PMAccountManager
import org.fejoa.auth.passwordmanager.PMBackground
import org.fejoa.auth.ui.AuthManager
import org.fejoa.auth.ui.PasswordRequestManager
import org.fejoa.auth.ui.pm.ExportWalletModel
import org.fejoa.crypto.BaseKeyCache
import org.fejoa.crypto.BaseKeyParams
import org.fejoa.crypto.SecretKey
import org.fejoa.support.Future


class WebWorkerStrategy : BaseKeyCache.DeriveStrategy {
    override fun derive(baseKeyParams: BaseKeyParams, password: String): Future<SecretKey> {
        val kdfWorker = KDFWorkerClient()
        return kdfWorker.deriveKey(password, baseKeyParams.getSalt(), baseKeyParams.kdf)
    }
}

class FejoaAuthClient {
    val context = FejoaContext(AccountIO.Type.CLIENT, "fejoa-auth", "noUser", Dispatchers.Default)
    val connectionAuthManager = ConnectionAuthManager(context.baseKeyCache)
    val authManager = AuthManager(context, connectionAuthManager)
    val accountManager = PMAccountManager(context, connectionAuthManager)
    val exportModel = ExportWalletModel(accountManager)
    val pmBackground = PMBackground(accountManager)
    val pwRequestManager = PasswordRequestManager()

    init {
        context.setBaseKeyCacheStrategy(WebWorkerStrategy())
    }

    fun dispose() {
        accountManager.dispose()
    }
}