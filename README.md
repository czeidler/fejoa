# Fejoa: The Portable Privacy Preserving Cloud

Please visit [https://czeidler.gitlab.io/fejoapage/](https://czeidler.gitlab.io/fejoapage/) for more information.

For build instructions please visit [link](https://czeidler.gitlab.io/fejoapage/getstarted.html).